﻿namespace Infrastructure.Security
{
    public class Rol
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }

    }
}
