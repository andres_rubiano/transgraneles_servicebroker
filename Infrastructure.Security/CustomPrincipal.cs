﻿using System.Collections.Generic;
using System.Security.Principal;

namespace Infrastructure.Security
{
    public class CustomPrincipal : IPrincipal
    {
        private readonly CustomIdentity _identity;

        public List<Rol> Roles { get; set; }
        public CustomIdentity User { get { return _identity; } }

        public CustomPrincipal(CustomIdentity identity, List<Rol> roles)
        {
            _identity = identity;
            Roles = roles;
        }


        public CustomPrincipal(string usrCode)
        {
           
        }

        public bool IsInRole(string role)
        {
            return (Roles.Find(p => p.Code == role) != null);

        }

        public IIdentity Identity
        {
            get { return _identity; }
        }
    }
}
