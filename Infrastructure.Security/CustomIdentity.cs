﻿using System.Security.Principal;

namespace Infrastructure.Security
{
    public class CustomIdentity : IIdentity
    {
        public string Name
        {
            get
            {
                return FirstName + LastName;
            }
        }

        public string AuthenticationType
        {
            get { return "Custom"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public bool IsActivated { get; set; }

        public int UserId { get; set; }
        public string Code { get; set; }
        public string UcrCode { get; set; }
        public string UloCode { get; set; }
        public string Company { get; set; }
        public string Subsidiary { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
