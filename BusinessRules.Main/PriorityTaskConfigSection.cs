﻿using System.Configuration;

namespace BusinessRules.Main
{
    class PriorityTaskConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("PriorityTaskConfiguration")]
        public PriorityTaskConfigCollection PriorityTaskConfiguration
        {
            get { return ((PriorityTaskConfigCollection)(base["PriorityTaskConfiguration"])); }
        }
    }

    public class PriorityTaskConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("OperationName", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string OperationName
        {
            get { return ((string)(base["OperationName"])); }
            set { base["OperationName"] = value; }
        }

        [ConfigurationProperty("OperationCode", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string OperationCode
        {
            get { return ((string)(base["OperationCode"])); }
            set { base["OperationCode"] = value; }
        }

        [ConfigurationProperty("Priority", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string Priority
        {
            get { return ((string)(base["Priority"])); }
            set { base["Priority"] = value; }
        }
        
    }

    [ConfigurationCollection(typeof(PriorityTaskConfigElement))]
    internal class PriorityTaskConfigCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new PriorityTaskConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PriorityTaskConfigElement)(element)).OperationName;
        }

        public PriorityTaskConfigElement this[int idx]
        {
            get { return (PriorityTaskConfigElement)BaseGet(idx); }
        }

        public PriorityTaskConfigElement this[string OperationName]
        {
            get { return (PriorityTaskConfigElement)BaseGet(OperationName); }
        }
    }
}
