﻿using Infrastructure.Data.Main.EF;

namespace BusinessRules.Main
{
    public static class VoyageMonitorUtils
    {
        public class CancelCargoData
        {
            public string ViajeNumero { get; set; }
            public string RemesaNumero { get; set; }
        }

        public enum VoyageIntegrationState
        {
            SinIntegrar = 1,
            EnProceso = 2,
            Error = 3,
            Integrado = 4
        }

        public static class Constants
        {
            public const string InitialIntegration = "Integración Inicial";
            public const string CancelIntegration = "Cancelación de Integración";
            public const string CancelCargo = "Anulación de remesa";
            public const string UpdateCargo = "Actualización de remesa";
        }

        public static VoyageMonitor AddOrUpdateVoyageMonitor(string voyageNumber, AsTransEntities db, VoyageMonitor currentVoyageMonitor)
        {
            if (currentVoyageMonitor is null)
            {
                currentVoyageMonitor = new VoyageMonitor
                {
                    VomVoyageNumber = voyageNumber,
                    VomIntegrationState = (int)VoyageIntegrationState.EnProceso
                };

                db.VoyageMonitor.Add(currentVoyageMonitor);
            }

            db.SaveChanges();

            return currentVoyageMonitor;
        }

        public static VoyageMonitorQueue AddOrUpdateVoyageMonitorQueue(string voyageNumber, AsTransEntities db, VoyageMonitorQueue currentVoyageQueue, string message, string processName)
        {
            if (currentVoyageQueue is null)
            {
                currentVoyageQueue = new VoyageMonitorQueue
                {
                    VmqVoyageNumber = voyageNumber,
                    VmqCreationDate = System.DateTime.Now
                };

                db.VoyageMonitorQueue.Add(currentVoyageQueue);
            }

            currentVoyageQueue.VmqMNProcessName = processName;
            currentVoyageQueue.VmqMNErrorMessage = message;
            currentVoyageQueue.VmqMNProcessDate = System.DateTime.Now;
            currentVoyageQueue.VmqModificationDate = System.DateTime.Now;

            db.SaveChanges();

            return currentVoyageQueue;
        }

        public static void AddVoyageMonitorHistory(AsTransEntities db, string voyageNumber, string request, string message, string processName)
        {
            db.VoyageMonitorHisto.Add(new VoyageMonitorHisto
            {
                VmhProcessName = processName,
                VmhVoyageNumber = voyageNumber,
                VmhCreationDate = System.DateTime.Now,
                VmhProcessDate = System.DateTime.Now,
                VmhMessage = message,
                VmhObjectSend = request
            });

            db.SaveChanges();
        }
    }
}
