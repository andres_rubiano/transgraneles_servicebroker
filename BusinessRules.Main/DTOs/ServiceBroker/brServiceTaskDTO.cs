﻿using System;
using Common;

namespace BusinessRules.Main.DTOs.ServiceBroker
{
    public class brServiceTaskDTO
    {
        public int StkId { get; set; }
        public int StkType { get; set; }
        public string StkService_SrvCode { get; set; }
        public string StkReference { get; set; }
        public string StkConditionService_SrvCode { get; set; }
        public string StkConditionReference { get; set; }
        public bool StkSuccessed { get; set; }
        public bool StkError { get; set; }
        public string StkMessage { get; set; }
        public int StkState { get; set; }
        public int StkAttempt { get; set; }
        public int StkRequestID { get; set; }
        public int StkResponseID { get; set; }
        public DateTime StkProcessDate { get; set; }
        public DateTime? StkProcessedDate { get; set; }
        public DateTime StkCreationDate { get; set; }
        public string StkCreation_UsrCode { get; set; }
        public string StkObjectJson { get; set; }

        public void EstablecerEnError(string mensaje)
        {
            StkError = true;
            StkProcessDate = DateTime.Now;
            StkMessage = mensaje.Truncate(1000);
        }

        public void EstablecerEnOk(string mensaje)
        {
            StkSuccessed = true;
            StkProcessDate = DateTime.Now;
            StkMessage = mensaje.Truncate(1000);
        }

        public bool EstaEnError()
        {
            return StkError;
        }
    }
}
