﻿using System;

namespace BusinessRules.Main.DTOs.ServiceBroker
{
    public class InterfaceServiceBrokerDTO
    {
        public string IsbInterface { get; set; }
        public string IsbReference { get; set; }
        public bool IsbSuccessed { get; set; }
        public bool IsbError { get; set; }
        public string IsbMessage { get; set; }
        public string IsbState { get; set; }
        public DateTime IsbProcessedDate { get; set; }
        public int IsbRequestID { get; set; }
        public int IsbResponseID { get; set; }
        public string IsbUser_UsrCode { get; set; }
        public DateTime IsbDate { get; set; }
        public int IsbAttempt { get; set; }

    }
}
