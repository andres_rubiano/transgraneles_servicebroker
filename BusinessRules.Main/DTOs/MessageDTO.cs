﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Common.Constants;

namespace BusinessRules.Main.DTOs
{
    public class MessageDTO
    {
        [XmlIgnore]
        [DataMember]
        public MessagesConstants.Types TypeEnum { set; get; }

        private int type;
        [DataMember]
        public int Type
        {
            get { return (int)TypeEnum; }
            set { type = value; }
        }

        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string TransactionNumber { get; set; }
        [DataMember]
        public string ErrorCode { get; set; }
    }
}
