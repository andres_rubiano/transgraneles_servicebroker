﻿namespace BusinessRules.Main.DTOs.BRProviderService
{
    public class ExtCustomerDTO : ExtThirdPartyDTO
    {
        public bool UcrTemporal { get; set; }
    }
}
