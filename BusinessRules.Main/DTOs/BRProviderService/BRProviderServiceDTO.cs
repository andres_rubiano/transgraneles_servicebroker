﻿namespace BusinessRules.Main.DTOs.BRProviderService
{
   public class BrProviderServiceDTO
    {
        public int SprId { get; set; }
        public string SprCode { get; set; }
        public string SprName { get; set; }
        public bool SprAuthenticationRequired { get; set; }
        public string SprUser { get; set; }
        public string SprPassword { get; set; }
        public string SprDomain { get; set; }
    }
   public class BrProviderServicePsswDTO
   {
       public string SprPassword { get; set; }
   }
}
