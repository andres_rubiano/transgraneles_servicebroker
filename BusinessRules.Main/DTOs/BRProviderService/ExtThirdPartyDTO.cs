﻿using System;
using System.Xml.Serialization;

namespace BusinessRules.Main.DTOs.BRProviderService
{
    [Serializable]
    public class ExtThirdPartyDTO
    {
        [XmlElement(IsNullable = true)]
        public string Code { get; set; } //Código del proveedor o del cliente, según aplique
        public string ExternalErpCode { get; set; } //Este atributo es utilizado para asociar un cliente con un proveedor y viceversa
        [XmlElement(IsNullable = true)]
        public string Status { get; set; } //Estado actual del tercero
        [XmlElement(IsNullable = true)]
        public string UtyCode { get; set; } //Código que identifica el tipo de tercero a registrar        
        public string IdentificationType { get; set; } //Código que indica el tipo de identificación del tercero
        [XmlElement(IsNullable = true)]
        public string UitName { get; set; } // Descripcion tipo Identificacion
        [XmlElement(IsNullable = true)]
        public string Identificacion { get; set; }  // identificacion
        [XmlElement(IsNullable = true)]
        public string Name { get; set; } // Razon Social        
        public string FirstName { get; set; } // Nombre        
        public string LastName { get; set; } // Apellido
        [XmlElement(IsNullable = true)]
        public string Address { get; set; } // Direccion
        [XmlElement(IsNullable = true)]
        public string CityName { get; set; } // ciudad
        [XmlElement(IsNullable = false)]
        public string CountyCode { get; set; } // Codigo Condado
        [XmlElement(IsNullable = false)]
        public string CountyName { get; set; } // Descripcion Condado
        [XmlElement(IsNullable = true)]
        public string StateCode { get; set; }
        [XmlElement(IsNullable = true)]
        public string StateName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string ZipCode { get; set; }
        public string DistrictName { get; set; } //Texto con el nombre del barrio del cliente  
        public string Contact { get; set; }  // UcrContact        
        public string Email { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
    }
}
