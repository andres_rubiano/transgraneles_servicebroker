﻿using System.Configuration;

namespace BusinessRules.Main
{
    public class VoyageMonitorConfig : ConfigurationSection
    {
        private VoyageMonitorConfig() { }

        public static VoyageMonitorConfig Constants()
        {
            if (ConfigurationManager.GetSection("VoyageMonitor") is VoyageMonitorConfig configuration)
                return configuration;

            return new VoyageMonitorConfig();
        }

        [ConfigurationProperty("SuccessCode", IsRequired = false)]
        public string SuccessCode
        {
            get { return (string)this["SuccessCode"]; }
        }

        [ConfigurationProperty("User", IsRequired = false)]
        public string User
        {
            get { return (string)this["User"]; }
        }

        [ConfigurationProperty("Password", IsRequired = false)]
        public string Password
        {
            get { return (string)this["Password"]; }
        }

        [ConfigurationProperty("CancelCargoDelete", IsRequired = false)]
        public string CancelCargoDelete
        {
            get { return (string)this["CancelCargoDelete"]; }
        }
    }
}
