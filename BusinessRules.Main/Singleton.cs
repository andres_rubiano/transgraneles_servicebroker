﻿namespace BusinessRules.Main
{
    public class Singleton
    {
        private static Singleton instance;

        private Singleton() { }

        public static string Request { get; set; }

        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Singleton();
                }
                return instance;
            }
        }
    }
}
