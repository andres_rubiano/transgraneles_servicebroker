﻿using BusinessRules.Main.DTOs.BRProviderService;
using BusinessRules.Main.DTOs.ServiceBroker;

namespace BusinessRules.Main.Interfaces.ServiceBroker
{
    /// <summary>
    /// Interface que define el contrato para toda ejecución de lógica de negocio contra las tareas del servicebroker
    /// </summary>
    public interface ITaskProcessor
    {
        /// <summary>
        /// Simplexity S.A
        /// Method: ExecuteTask
        /// Author: Cesar Fernando Estupiñan Date: 24/05/2013
        /// Objective:En la implementación de este método se debe realizar la ejecución de toda la lógica de negocio definida para la tarea (integraciones, impresiones, etc.)
        /// </summary>
        /// <param name="task">The task.</param>
        /// <returns>brServiceTaskDTO</returns>
        brServiceTaskDTO ExecuteTask(brServiceTaskDTO task);
    }
}
