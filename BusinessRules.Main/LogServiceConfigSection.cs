﻿using System.Configuration;

namespace BusinessRules.Main
{
    class LogServiceConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("LogServiceConfiguration")]
        public LogServiceConfigCollection LogServiceConfiguration
        {
            get { return ((LogServiceConfigCollection)(base["LogServiceConfiguration"])); }
        }
    }

    internal class LogServiceConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("OperationName", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string OperationName
        {
            get { return ((string)(base["OperationName"])); }
            set { base["OperationName"] = value; }
        }


        [ConfigurationProperty("OperationCode", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string OperationCode
        {
            get { return ((string)(base["OperationCode"])); }
            set { base["OperationCode"] = value; }
        }

        [ConfigurationProperty("SoapReference", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string SoapReference
        {
            get { return ((string)(base["SoapReference"])); }
            set { base["SoapReference"] = value; }
        }

        [ConfigurationProperty("AssociateToServiceBroker", DefaultValue = false, IsKey = false, IsRequired = false)]
        public bool AssociateToServiceBroker
        {
            get { return ((bool)(base["AssociateToServiceBroker"])); }
            set { base["AssociateToServiceBroker"] = value; }
        }
    }

    [ConfigurationCollection(typeof(LogServiceConfigElement))]
    internal class LogServiceConfigCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new LogServiceConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((LogServiceConfigElement)(element)).OperationName;
        }

        public LogServiceConfigElement this[int idx]
        {
            get { return (LogServiceConfigElement)BaseGet(idx); }
        }

        public LogServiceConfigElement this[string OperationName]
        {
            get { return (LogServiceConfigElement)BaseGet(OperationName); }
        }
    }
}
