﻿using System.Configuration;

namespace BusinessRules.Main
{
    public class AsTransConfig : ConfigurationSection
    {
        public static AsTransConfig Constants()
        {
            AsTransConfig configuration = ConfigurationManager.GetSection("AsTrans") as AsTransConfig;

            if (configuration != null)
                return configuration;

            return new AsTransConfig();
        }

        [ConfigurationProperty("Down022_UbicacionArchivos", IsRequired = false)]
        public string Down022_UbicacionArchivos
        {
            get { return (string)this["Down022_UbicacionArchivos"]; }
        }

        [ConfigurationProperty("Down022_NombreArchivo", IsRequired = false)]
        public string Down022_NombreArchivo
        {
            get { return (string)this["Down022_NombreArchivo"]; }
        }

        [ConfigurationProperty("Down022_DirectorioVirtual", IsRequired = false)]
        public string Down022_DirectorioVirtual
        {
            get { return (string)this["Down022_DirectorioVirtual"]; }
        }


        [ConfigurationProperty("Qrc001_UbicacionArchivos", IsRequired = false)]
        public string Qrc001_UbicacionArchivos
        {
            get { return (string)this["Qrc001_UbicacionArchivos"]; }
        }

        [ConfigurationProperty("Qrc001_DirectorioVirtual", IsRequired = false)]
        public string Qrc001_DirectorioVirtual
        {
            get { return (string)this["Qrc001_DirectorioVirtual"]; }
        }

    }
}
