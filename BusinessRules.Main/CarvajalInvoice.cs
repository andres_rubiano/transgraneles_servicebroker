﻿using System.Configuration;

namespace BusinessRules.Main
{
    public class CarvajalInvoice : ConfigurationSection
    {
        //private static CarvajalInvoice constants = ConfigurationManager.GetSection("CarvajalInvoice") as CarvajalInvoice;

        public static CarvajalInvoice Constants()
        {

            CarvajalInvoice configuration = ConfigurationManager.GetSection("CarvajalInvoice") as CarvajalInvoice;

            if (configuration != null)
                return configuration;

            return new CarvajalInvoice();
        }


        [ConfigurationProperty("Enc_4", IsRequired = false)]
        public string Enc_4
        {
            get { return (string)this["Enc_4"]; }
        }

        [ConfigurationProperty("Enc_5_Factura", IsRequired = false)]
        public string Enc_5_Factura
        {
            get { return (string)this["Enc_5_Factura"]; }
        }

        [ConfigurationProperty("Enc_5_NotaCredito", IsRequired = false)]
        public string Enc_5_NotaCredito
        {
            get { return (string)this["Enc_5_NotaCredito"]; }
        }

        [ConfigurationProperty("Enc_5_NotaDebito", IsRequired = false)]
        public string Enc_5_NotaDebito
        {
            get { return (string)this["Enc_5_NotaDebito"]; }
        }

        [ConfigurationProperty("Enc_20", IsRequired = false)]
        public string Enc_20
        {
            get { return (string)this["Enc_20"]; }
        }

        [ConfigurationProperty("Enc_21_Factura", IsRequired = false)]
        public string Enc_21_Factura
        {
            get { return (string)this["Enc_21_Factura"]; }
        }

        [ConfigurationProperty("Enc_21_NotaCredito20", IsRequired = false)]
        public string Enc_21_NotaCredito20
        {
            get { return (string)this["Enc_21_NotaCredito20"]; }
        }

        [ConfigurationProperty("Enc_21_NotaCredito21", IsRequired = false)]
        public string Enc_21_NotaCredito21
        {
            get { return (string)this["Enc_21_NotaCredito21"]; }
        }

        [ConfigurationProperty("Enc_21_NotaDebito20", IsRequired = false)]
        public string Enc_21_NotaDebito20
        {
            get { return (string)this["Enc_21_NotaDebito20"]; }
        }

        [ConfigurationProperty("Enc_21_NotaDebito21", IsRequired = false)]
        public string Enc_21_NotaDebito21
        {
            get { return (string)this["Enc_21_NotaDebito21"]; }
        }

        [ConfigurationProperty("Emi_4", IsRequired = false)]
        public string Emi_4
        {
            get { return (string)this["Emi_4"]; }
        }

        [ConfigurationProperty("Tac_1", IsRequired = false)]
        public string Tac_1
        {
            get { return (string)this["Tac_1"]; }
        }

        [ConfigurationProperty("Gte_1", IsRequired = false)]
        public string Gte_1
        {
            get { return (string)this["Gte_1"]; }
        }

        [ConfigurationProperty("Gte_2", IsRequired = false)]
        public string Gte_2
        {
            get { return (string)this["Gte_2"]; }
        }

        [ConfigurationProperty("Gta_1", IsRequired = false)]
        public string Gta_1
        {
            get { return (string)this["Gta_1"]; }
        }

        [ConfigurationProperty("Gta_2", IsRequired = false)]
        public string Gta_2
        {
            get { return (string)this["Gta_2"]; }
        }

        [ConfigurationProperty("Imp_1_reteFuente", IsRequired = false)]
        public string Imp_1_reteFuente
        {
            get { return (string)this["Imp_1_reteFuente"]; }
        }

        [ConfigurationProperty("Imp_1_reteIca", IsRequired = false)]
        public string Imp_1_reteIca
        {
            get { return (string)this["Imp_1_reteIca"]; }
        }

        [ConfigurationProperty("Imp_1_iva", IsRequired = false)]
        public string Imp_1_iva
        {
            get { return (string)this["Imp_1_iva"]; }
        }

        [ConfigurationProperty("Dsc_1", IsRequired = false)]
        public string Dsc_1
        {
            get { return (string)this["Dsc_1"]; }
        }

        [ConfigurationProperty("Dsc_2", IsRequired = false)]
        public string Dsc_2
        {
            get { return (string)this["Dsc_2"]; }
        }

        [ConfigurationProperty("Dsc_5", IsRequired = false)]
        public string Dsc_5
        {
            get { return (string)this["Dsc_5"]; }
        }

        [ConfigurationProperty("Dsc_6", IsRequired = false)]
        public string Dsc_6
        {
            get { return (string)this["Dsc_6"]; }
        }

        [ConfigurationProperty("Not_1_Pos_1", IsRequired = false)]
        public string Not_1_Pos_1
        {
            get { return (string)this["Not_1_Pos_1"]; }
        }

        [ConfigurationProperty("Not_1_Pos_3", IsRequired = false)]
        public string Not_1_Pos_3
        {
            get { return (string)this["Not_1_Pos_3"]; }
        }

        [ConfigurationProperty("Not_2_Pos_1", IsRequired = false)]
        public string Not_2_Pos_1
        {
            get { return (string)this["Not_2_Pos_1"]; }
        }

        [ConfigurationProperty("Not_4_Pos_1", IsRequired = false)]
        public string Not_4_Pos_1
        {
            get { return (string)this["Not_4_Pos_1"]; }
        }

        [ConfigurationProperty("Not_9_Pos_1", IsRequired = false)]
        public string Not_9_Pos_1
        {
            get { return (string)this["Not_9_Pos_1"]; }
        }

        [ConfigurationProperty("Not_9_Pos_2", IsRequired = false)]
        public string Not_9_Pos_2
        {
            get { return (string)this["Not_9_Pos_2"]; }
        }

        [ConfigurationProperty("Not_9_Pos_3", IsRequired = false)]
        public string Not_9_Pos_3
        {
            get { return (string)this["Not_9_Pos_3"]; }
        }

        [ConfigurationProperty("Not_12_Pos_1", IsRequired = false)]
        public string Not_12_Pos_1
        {
            get { return (string)this["Not_12_Pos_1"]; }
        }

        [ConfigurationProperty("Cts_1", IsRequired = false)]
        public string Cts_1
        {
            get { return (string)this["Cts_1"]; }
        }

        [ConfigurationProperty("Iae_2", IsRequired = false)]
        public string Iae_2
        {
            get { return (string)this["Iae_2"]; }
        }

        [ConfigurationProperty("Ref_1", IsRequired = false)]
        public string Ref_1
        {
            get { return (string)this["Ref_1"]; }
        }

        [ConfigurationProperty("Ref_5", IsRequired = false)]
        public string Ref_5
        {
            get { return (string)this["Ref_5"]; }
        }

        [ConfigurationProperty("Mep_1", IsRequired = false)]
        public string Mep_1
        {
            get { return (string)this["Mep_1"]; }
        }

        [ConfigurationProperty("Divisa", IsRequired = false)]
        public string Divisa
        {
            get { return (string)this["Divisa"]; }
        }

        [ConfigurationProperty("PaisCodigo", IsRequired = false)]
        public string PaisCodigo
        {
            get { return (string)this["PaisCodigo"]; }
        }

        [ConfigurationProperty("PaisNombre", IsRequired = false)]
        public string PaisNombre
        {
            get { return (string)this["PaisNombre"]; }
        }

        [ConfigurationProperty("UbicacionArchivos", IsRequired = false)]
        public string UbicacionArchivos
        {
            get { return (string)this["UbicacionArchivos"]; }
        }

        [ConfigurationProperty("UrlArchivos", IsRequired = false)]
        public string UrlArchivos
        {
            get { return (string)this["UrlArchivos"]; }
        }

        [ConfigurationProperty("EmailPruebaEntregaFactura", IsRequired = false)]
        public string EmailPruebaEntregaFactura
        {
            get { return (string)this["EmailPruebaEntregaFactura"]; }
        }

        [ConfigurationProperty("ExtensionArchivos", IsRequired = false)]
        public string ExtensionArchivos
        {
            get { return (string)this["ExtensionArchivos"]; }
        }

        [ConfigurationProperty("UsernameService", IsRequired = false)]
        public string UsernameService
        {
            get { return (string)this["UsernameService"]; }
        }

        [ConfigurationProperty("PasswordService", IsRequired = false)]
        public string PasswordService
        {
            get { return (string)this["PasswordService"]; }
        }

        [ConfigurationProperty("CompanyId", IsRequired = false)]
        public string CompanyId
        {
            get { return (string)this["CompanyId"]; }
        }

        [ConfigurationProperty("AccountId", IsRequired = false)]
        public string AccountId
        {
            get { return (string)this["AccountId"]; }
        }

        [ConfigurationProperty("MaximoCaracteresNotTag", IsRequired = false)]
        public string MaximoCaracteresNotTag
        {
            get { return (string)this["MaximoCaracteresNotTag"]; }
        }

        [ConfigurationProperty("FechaInicioVersion21", IsRequired = false)]
        public string FechaInicioVersion21
        {
            get { return (string)this["FechaInicioVersion21"]; }
        }

        [ConfigurationProperty("Ila_5", IsRequired = false)]
        public string Ila_5
        {
            get { return (string)this["Ila_5"]; }
        }

        [ConfigurationProperty("Cde_4", IsRequired = false)]
        public string Cde_4
        {
            get { return (string)this["Cde_4"]; }
        }

        [ConfigurationProperty("Cdn_2", IsRequired = false)]
        public string Cdn_2
        {
            get { return (string)this["Cdn_2"]; }
        }

        [ConfigurationProperty("Cdn_2_MinCaracteres", IsRequired = false)]
        public int Cdn_2_MinCaracteres
        {
            get { return (int)this["Cdn_2_MinCaracteres"]; }
        }

        [ConfigurationProperty("Iei_2", IsRequired = false)]
        public string Iei_2
        {
            get { return (string)this["Iei_2"]; }
        }

        [ConfigurationProperty("Cdn_1_NotaDebito", IsRequired = false)]
        public string Cdn_1_NotaDebito
        {
            get { return (string)this["Cdn_1_NotaDebito"]; }
        }

        [ConfigurationProperty("Cdn_1_NotaCredito", IsRequired = false)]
        public string Cdn_1_NotaCredito
        {
            get { return (string)this["Cdn_1_NotaCredito"]; }
        }

        [ConfigurationProperty("Cdn_1_NotaCreditoAnulaFactura", IsRequired = false)]
        public string Cdn_1_NotaCreditoAnulaFactura
        {
            get { return (string)this["Cdn_1_NotaCreditoAnulaFactura"]; }
        }

        [ConfigurationProperty("Ext_1", IsRequired = false)]
        public string Ext_1
        {
            get { return (string)this["Ext_1"]; }
        }

        [ConfigurationProperty("MaximoDeExt", IsRequired = false)]
        public int MaximoDeExt
        {
            get { return (int)this["MaximoDeExt"]; }
        }

        [ConfigurationProperty("MaximoDeEgc", IsRequired = false)]
        public int MaximoDeEgc
        {
            get { return (int)this["MaximoDeEgc"]; }
        }

        [ConfigurationProperty("Fac105MinutosReprogramar", IsRequired = false)]
        public int Fac105MinutosReprogramar
        {
            get { return (int)this["Fac105MinutosReprogramar"]; }
        }

        [ConfigurationProperty("Fac106MinutosReprogramar", IsRequired = false)]
        public int Fac106MinutosReprogramar
        {
            get { return (int)this["Fac106MinutosReprogramar"]; }
        }

        [ConfigurationProperty("Fac106HorasValidaAceptacionTacita", IsRequired = false)]
        public int Fac106HorasValidaAceptacionTacita
        {
            get { return (int)this["Fac106HorasValidaAceptacionTacita"]; }
        }

        [ConfigurationProperty("Fac107MinutosReprogramar", IsRequired = false)]
        public int Fac107MinutosReprogramar
        {
            get { return (int)this["Fac107MinutosReprogramar"]; }
        }

        [ConfigurationProperty("Fac107SenderPartySchemeAgencyID", IsRequired = false)]
        public string Fac107SenderPartySchemeAgencyID
        {
            get { return (string)this["Fac107SenderPartySchemeAgencyID"]; }
        }

        [ConfigurationProperty("Fac107ReceiverPartySchemeAgencyID", IsRequired = false)]
        public string Fac107ReceiverPartySchemeAgencyID
        {
            get { return (string)this["Fac107ReceiverPartySchemeAgencyID"]; }
        }

        [ConfigurationProperty("Fac107schemeAgencyName", IsRequired = false)]
        public string Fac107schemeAgencyName
        {
            get { return (string)this["Fac107schemeAgencyName"]; }
        }

        [ConfigurationProperty("Fac107ResponseCode", IsRequired = false)]
        public string Fac107ResponseCode
        {
            get { return (string)this["Fac107ResponseCode"]; }
        }

        [ConfigurationProperty("Fac107ResponseDescription", IsRequired = false)]
        public string Fac107ResponseDescription
        {
            get { return (string)this["Fac107ResponseDescription"]; }
        }

        [ConfigurationProperty("Fac107DocumentTypeCode", IsRequired = false)]
        public string Fac107DocumentTypeCode
        {
            get { return (string)this["Fac107DocumentTypeCode"]; }
        }

        [ConfigurationProperty("Fac108MinutosReprogramar", IsRequired = false)]
        public int Fac108MinutosReprogramar
        {
            get { return (int)this["Fac108MinutosReprogramar"]; }
        }

        [ConfigurationProperty("Fac502MinutosReprogramar", IsRequired = false)]
        public int Fac502MinutosReprogramar
        {
            get { return (int)this["Fac502MinutosReprogramar"]; }
        }

        [ConfigurationProperty("Fac105MinutosReprogramarCuandoLegalAceptado", IsRequired = false)]
        public int Fac105MinutosReprogramarCuandoLegalAceptado
        {
            get { return (int)this["Fac105MinutosReprogramarCuandoLegalAceptado"]; }
        }

        [ConfigurationProperty("AccountIdAceptacionTacita", IsRequired = false)]
        public string AccountIdAceptacionTacita
        {
            get { return (string)this["AccountIdAceptacionTacita"]; }
        }

        [ConfigurationProperty("Fac107ReceiverPartyCompanyID", IsRequired = false)]
        public string Fac107ReceiverPartyCompanyID
        {
            get { return (string)this["Fac107ReceiverPartyCompanyID"]; }
        }

        [ConfigurationProperty("Fac107RegistrationName", IsRequired = false)]
        public string Fac107RegistrationName
        {
            get { return (string)this["Fac107RegistrationName"]; }
        }

        [ConfigurationProperty("Fac107Note", IsRequired = false)]
        public string Fac107Note
        {
            get { return (string)this["Fac107Note"]; }
        }
    }
}
