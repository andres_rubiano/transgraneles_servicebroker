﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using Common.Constants;
using Infrastructure.Data.Main.EF;

namespace BusinessRules.Main.Factory
{
    public static class InvoiceFactory
    {
        public static StringBuilder GetInvoiceForFile(GetInvoiceHeaderToCarvajal_Result invoiceHeader, List<GetInvoiceDetailToCarvajal_Result> invoiceDetail, bool isCancelInvoice)
        {
            var data = new StringBuilder();
            var constantes = CarvajalInvoice.Constants();

            // datos del encabezado
            data.AppendLine(string.Format("<{0}>", InvoiceCarvajal.TypeInvoice[invoiceHeader.Enc_1_TipoDocumento]));
            data.AppendLine("  <ENC>");
            data.AppendLine("    <ENC_1>" + invoiceHeader.Enc_1_TipoDocumento + "</ENC_1>");
            data.AppendLine("    <ENC_2>" + invoiceHeader.Enc_2_IdentificacionEmisor + "</ENC_2>");
            data.AppendLine("    <ENC_3>" + invoiceHeader.Enc_3_IdentificacionAdquiriente + "</ENC_3>");
            data.AppendLine("    <ENC_4>" + constantes.Enc_4 + "</ENC_4>");

            // 1057 TGR-CC-210329 Cambios FE Anexo 1.8:
            switch (invoiceHeader.Enc_1_TipoDocumento)
            {
                case InvoiceCarvajal.Factura:
                    data.AppendLine("    <ENC_5>" + constantes.Enc_5_Factura + "</ENC_5>");
                    break;
                case InvoiceCarvajal.NotaDebito:
                    data.AppendLine("    <ENC_5>" + constantes.Enc_5_NotaDebito + "</ENC_5>");
                    break;
                case InvoiceCarvajal.NotaCredito:
                    data.AppendLine("    <ENC_5>" + constantes.Enc_5_NotaCredito + "</ENC_5>");
                    break;
            }



            data.AppendLine("    <ENC_6>" + invoiceHeader.Enc_6_NumeroDocumento + "</ENC_6>");
            data.AppendLine("    <ENC_7>" + invoiceHeader.Enc_7_FechaFactura + "</ENC_7>");
            data.AppendLine("    <ENC_8>" + invoiceHeader.Enc_8_HoraFactura + "</ENC_8>");
            data.AppendLine("    <ENC_9>" + invoiceHeader.Enc_9_TipoFactura + "</ENC_9>");
            data.AppendLine("    <ENC_10>" + constantes.Divisa + "</ENC_10>");
            data.AppendLine("    <ENC_15>" + invoiceDetail.Count() + "</ENC_15>");

            if (invoiceHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
            {
                data.AppendLine("    <ENC_16>" + invoiceHeader.Enc_16_FechaVencimiento + "</ENC_16>");
                data.AppendLine("    <ENC_17>" + invoiceHeader.Enc_17_UrlArchivosAnexos + "</ENC_17>");
            }

            data.AppendLine("    <ENC_20>" + constantes.Enc_20 + "</ENC_20>");
            data.AppendLine("    <ENC_21>" + invoiceHeader.Enc_21_TipoOperacion + "</ENC_21>");
            data.AppendLine("  </ENC>");

            // Emisor:

            data.AppendLine("  <EMI>");
            data.AppendLine("    <EMI_1>" + invoiceHeader.Emi_1_TipoPersona + "</EMI_1>");
            data.AppendLine("    <EMI_2>" + invoiceHeader.Emi_2_Identificacion + "</EMI_2>");
            data.AppendLine("    <EMI_3>" + invoiceHeader.Emi_3_TipoIdentificacion + "</EMI_3>");
            data.AppendLine("    <EMI_4>" + constantes.Emi_4 + "</EMI_4>");
            data.AppendLine("    <EMI_6>" + invoiceHeader.Emi_6_RazonSocial + "</EMI_6>");
            data.AppendLine("    <EMI_7>" + invoiceHeader.Emi_7_NombreComercial + "</EMI_7>");
            data.AppendLine("    <EMI_10>" + invoiceHeader.Emi_10_Direccion + "</EMI_10>");
            data.AppendLine("    <EMI_11>" + invoiceHeader.Emi_11_CodigoDepartamento + "</EMI_11>");
            data.AppendLine("    <EMI_13>" + invoiceHeader.Emi_13_Ciudad + "</EMI_13>");
            data.AppendLine("    <EMI_14>" + invoiceHeader.Emi_14_CodigoPostal + "</EMI_14>");
            data.AppendLine("    <EMI_15>" + constantes.PaisCodigo + "</EMI_15>");
            data.AppendLine("    <EMI_19>" + invoiceHeader.Emi_19_NombreDepartamento + "</EMI_19>");
            data.AppendLine("    <EMI_21>" + constantes.PaisNombre + "</EMI_21>");
            data.AppendLine("    <EMI_22>" + invoiceHeader.Emi_22_DigitoVerificacion + "</EMI_22>");
            data.AppendLine("    <EMI_23>" + invoiceHeader.Emi_23_CodigoMunicipio + "</EMI_23>");
            data.AppendLine("    <EMI_24>" + invoiceHeader.Emi_6_RazonSocial + "</EMI_24>");
            data.AppendLine("    <TAC>");
            data.AppendLine("      <TAC_1>" + constantes.Tac_1 + "</TAC_1>");
            data.AppendLine("    </TAC>");
            data.AppendLine("    <DFE>");
            data.AppendLine("      <DFE_1>" + invoiceHeader.Emi_23_CodigoMunicipio + "</DFE_1>");
            data.AppendLine("      <DFE_2>" + invoiceHeader.Emi_11_CodigoDepartamento + "</DFE_2>");
            data.AppendLine("      <DFE_3>" + constantes.PaisCodigo + "</DFE_3>");
            data.AppendLine("      <DFE_4>" + invoiceHeader.Emi_14_CodigoPostal + "</DFE_4>");
            data.AppendLine("      <DFE_5>" + constantes.PaisNombre + "</DFE_5>");
            data.AppendLine("      <DFE_6>" + invoiceHeader.Emi_19_NombreDepartamento + "</DFE_6>");
            data.AppendLine("      <DFE_7>" + invoiceHeader.Emi_13_Ciudad + "</DFE_7>");

            var dfeOcho = string.Format("{0}, {1}, {2}, {3}", constantes.PaisNombre,
                invoiceHeader.Emi_19_NombreDepartamento, invoiceHeader.Emi_13_Ciudad, invoiceHeader.Emi_10_Direccion);

            data.AppendLine("      <DFE_8>" + dfeOcho.Truncate(300) + "</DFE_8>");

            data.AppendLine("    </DFE>");

            data.AppendLine("    <ICC>");
            data.AppendLine("      <ICC_9>" + invoiceHeader.Drf_4_Prefijo + "</ICC_9>");
            data.AppendLine("    </ICC>");


            data.AppendLine("    <CDE>");
            data.AppendLine("      <CDE_1>" + invoiceHeader.Emi_Cde_1_TipoContacto + "</CDE_1>");
            data.AppendLine("      <CDE_2>" + invoiceHeader.Emi_Cde_2_NombreContacto + "</CDE_2>");
            data.AppendLine("      <CDE_3>" + invoiceHeader.Emi_Cde_3_TelefonoContacto + "</CDE_3>");
            data.AppendLine("      <CDE_4>" + constantes.Cde_4 + "</CDE_4>");
            data.AppendLine("    </CDE>");
            data.AppendLine("    <GTE>");
            data.AppendLine("      <GTE_1>" + constantes.Gte_1 + "</GTE_1>");
            data.AppendLine("      <GTE_2>" + constantes.Gte_2 + "</GTE_2>");
            data.AppendLine("    </GTE>");
            data.AppendLine("  </EMI>");

            // Adquiriente:

            data.AppendLine("  <ADQ>");
            data.AppendLine("    <ADQ_1>" + invoiceHeader.Adq_1_TipoPersona + "</ADQ_1>");
            data.AppendLine("    <ADQ_2>" + invoiceHeader.Adq_2_Identificacion + "</ADQ_2>");
            data.AppendLine("    <ADQ_3>" + invoiceHeader.Adq_3_TipoIdentificacion + "</ADQ_3>");
            data.AppendLine("    <ADQ_4>" + invoiceHeader.Adq_4_RegimenFiscal + "</ADQ_4>");
            data.AppendLine("    <ADQ_6>" + invoiceHeader.Adq_6_RazonSocial + "</ADQ_6>"); // el SP ya hace la validacion del tipo.   
            data.AppendLine("    <ADQ_7>" + invoiceHeader.Adq_7_NombreComercial + "</ADQ_7>");
            data.AppendLine("    <ADQ_8>" + invoiceHeader.Adq_8_NombreCompleto + "</ADQ_8>");
            data.AppendLine("    <ADQ_9>" + invoiceHeader.Adq_8_NombreCompleto + "</ADQ_9>");
            data.AppendLine("    <ADQ_10>" + invoiceHeader.Adq_10_Direccion + "</ADQ_10>");
            data.AppendLine("    <ADQ_11>" + invoiceHeader.Adq_11_CodigoDepartamento + "</ADQ_11>");
            data.AppendLine("    <ADQ_13>" + invoiceHeader.Adq_13_Ciudad + "</ADQ_13>");
            data.AppendLine("    <ADQ_14>" + invoiceHeader.Adq_14_CodigoPostal + "</ADQ_14>");
            data.AppendLine("    <ADQ_15>" + constantes.PaisCodigo + "</ADQ_15>");
            data.AppendLine("    <ADQ_19>" + invoiceHeader.Adq_19_NombreDepartamento + "</ADQ_19>");
            data.AppendLine("    <ADQ_21>" + constantes.PaisNombre + "</ADQ_21>");

            // 1057 TGR-CC-210329 Cambios FE Anexo 1.8
            if (invoiceHeader.Adq_3_TipoIdentificacion.Equals("31"))
                data.AppendLine("    <ADQ_22>" + invoiceHeader.Adq_22_DigitoVerificacion + "</ADQ_22>");

            data.AppendLine("    <ADQ_23>" + invoiceHeader.Adq_23_CodigoMunicipio + "</ADQ_23>");
            data.AppendLine("    <ADQ_24>" + invoiceHeader.Adq_24_Identificacion + "</ADQ_24>");
            data.AppendLine("    <ADQ_25>" + invoiceHeader.Adq_3_TipoIdentificacion + "</ADQ_25>");

            if (invoiceHeader.Adq_3_TipoIdentificacion.Equals(DocumentTyes.Nit))
                data.AppendLine("    <ADQ_26>" + invoiceHeader.Adq_22_DigitoVerificacion + "</ADQ_26>");

            data.AppendLine("    <TCR>");
            data.AppendLine("      <TCR_1>" + invoiceHeader.Tcr_1_ResponsabilidadesFiscales + "</TCR_1>");
            data.AppendLine("    </TCR>");
            data.AppendLine("    <ILA>");
            data.AppendLine("      <ILA_1>" + invoiceHeader.Adq_7_NombreComercial + "</ILA_1>");
            data.AppendLine("      <ILA_2>" + invoiceHeader.Adq_2_Identificacion + "</ILA_2>");
            data.AppendLine("      <ILA_3>" + invoiceHeader.Adq_3_TipoIdentificacion + "</ILA_3>");
            data.AppendLine("      <ILA_4>" + invoiceHeader.Adq_22_DigitoVerificacion + "</ILA_4>");
            data.AppendLine("      <ILA_5>" + constantes.Ila_5 + "</ILA_5>");
            data.AppendLine("    </ILA>");

            //if (invoiceHeader.Adq_4_RegimenFiscal.Equals(RegimenFiscal.ResponsableIva))
            if (!invoiceHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
            {
                data.AppendLine("    <DFA>");
                data.AppendLine("      <DFA_1>" + constantes.PaisCodigo + "</DFA_1>");
                data.AppendLine("      <DFA_2>" + invoiceHeader.Adq_11_CodigoDepartamento + "</DFA_2>");
                data.AppendLine("      <DFA_3>" + invoiceHeader.Adq_14_CodigoPostal + "</DFA_3>");
                data.AppendLine("      <DFA_4>" + invoiceHeader.Adq_23_CodigoMunicipio + "</DFA_4>");
                data.AppendLine("      <DFA_5>" + constantes.PaisNombre + "</DFA_5>");
                data.AppendLine("      <DFA_6>" + invoiceHeader.Adq_19_NombreDepartamento + "</DFA_6>");
                data.AppendLine("      <DFA_7>" + invoiceHeader.Adq_13_Ciudad + "</DFA_7>");
                data.AppendLine("      <DFA_8>" + invoiceHeader.Adq_10_Direccion + "</DFA_8>");
                data.AppendLine("    </DFA>");
            }

            data.AppendLine("    <ICR>");
            data.AppendLine("      <ICR_1>" + invoiceHeader.Icr_1_MatriculaMercantil + "</ICR_1>");
            data.AppendLine("    </ICR>");
            data.AppendLine("    <GTA>");
            data.AppendLine("      <GTA_1>" + constantes.Gta_1 + "</GTA_1>");
            data.AppendLine("      <GTA_2>" + constantes.Gta_2 + "</GTA_2>");
            data.AppendLine("    </GTA>");
            data.AppendLine("    <CDA>");
            data.AppendLine("      <CDA_1>" + invoiceHeader.Cda_1_TipoContacto + "</CDA_1>");
            data.AppendLine("      <CDA_2>" + invoiceHeader.Cda_2_NombreContacto + "</CDA_2>");
            data.AppendLine("      <CDA_3>" + invoiceHeader.Cda_3_TelefonoContacto + "</CDA_3>");

            // Para las pruebas se utiliza un correo definido por Tgr:

            if (string.IsNullOrEmpty(constantes.EmailPruebaEntregaFactura))
                data.AppendLine("      <CDA_4>" + invoiceHeader.Cda_4_EmailFactura + "</CDA_4>");
            else
                data.AppendLine("      <CDA_4>" + constantes.EmailPruebaEntregaFactura + "</CDA_4>");

            data.AppendLine("    </CDA>");
            data.AppendLine("  </ADQ>");

            #region CC-TGR-074-Control de Cambio FE Version 1.8
            // 2020Jun12_1138 Juan Camilo Cano solicita:
            // Como te comenté ayer al final del día, me dijo Viviana Valencia que el cambio del conjunto de tags “IAA” 
            // no debe ir porque después de validar con la DIAN resultó que lo aplazaron indefinidamente, 
            // por lo tanto como no lo han liberado puede frenar la facturación.

            //if (invoiceHeader.Adq_1_TipoPersona.Equals("2"))
            //{
            //    data.AppendLine("  <IAA>");
            //    data.AppendLine("    <IAA_1>" + invoiceHeader.Adq_2_Identificacion + "</IAA_1>");
            //    data.AppendLine("    <IAA_2>" + invoiceHeader.Adq_8_NombreCompleto + "</IAA_2>");
            //    data.AppendLine("    <IAA_3>" + invoiceHeader.Adq_11_CodigoDepartamento + "</IAA_3>");
            //    data.AppendLine("    <IAA_4>" + invoiceHeader.Adq_19_NombreDepartamento + "</IAA_4>");
            //    data.AppendLine("    <IAA_5>" + invoiceHeader.Adq_23_CodigoMunicipio + "</IAA_5>");
            //    data.AppendLine("    <IAA_6>" + invoiceHeader.Adq_13_Ciudad + "</IAA_6>");
            //    data.AppendLine("    <IAA_7>" + invoiceHeader.Adq_14_CodigoPostal + "</IAA_7>");
            //    data.AppendLine("    <IAA_8>" + invoiceHeader.Adq_10_Direccion + "</IAA_8>");
            //    data.AppendLine("  </IAA>");
            //}

            #endregion

            // Totales:

            data.AppendLine("  <TOT>");
            data.AppendLine("    <TOT_1>" + invoiceHeader.Tot_1_ImporteBruto + "</TOT_1>");
            data.AppendLine("    <TOT_2>" + constantes.Divisa + "</TOT_2>");
            data.AppendLine("    <TOT_3>" + invoiceHeader.Tot_3_BaseImponible + "</TOT_3>");
            data.AppendLine("    <TOT_4>" + constantes.Divisa + "</TOT_4>");
            data.AppendLine("    <TOT_5>" + invoiceHeader.Tot_5_TotalFactura + "</TOT_5>");
            data.AppendLine("    <TOT_6>" + constantes.Divisa + "</TOT_6>");
            data.AppendLine("    <TOT_7>" + invoiceHeader.Tot_7_TotalFactura + "</TOT_7>");
            data.AppendLine("    <TOT_8>" + constantes.Divisa + "</TOT_8>");
            data.AppendLine("    <TOT_9>" + invoiceHeader.Tot_9_TotalDescuentos + "</TOT_9>");
            data.AppendLine("    <TOT_10>" + constantes.Divisa + "</TOT_10>");
            data.AppendLine("    <TOT_11>0</TOT_11>"); // no existen los cargos positivos
            data.AppendLine("    <TOT_12>" + constantes.Divisa + "</TOT_12>");
            data.AppendLine("  </TOT>");

            // Un Tim para cada impuesto:

            // 2021Jul23_1644 Tgr solicita que el ica se reporte para todos los documentos, siempre que sea calculado o mayor a cero:

            if (invoiceHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
            {
                //Retenciones solo para facturas:
                //inactivacion por solictud de Tgr:
                //reactivacion: 27/09/2021 14:30 --> 1077-TGR-CC-210824 Ajuste Eventos Aceptación Tacita 
                if (invoiceHeader.Tim_2_iva > 0)
                {
                    var tim4 = (double)((invoiceHeader.Imp_2_reteFuente_BaseImponible * (invoiceHeader.Imp_6_reteFuente_PorcentajeImpuesto / 100)) -
                            invoiceHeader.Imp_4_reteFuente_ImporteImpuesto);
                    data.AppendLine("  <TIM>");
                    data.AppendLine("    <TIM_1>" + invoiceHeader.Tim_1_reteFuente + "</TIM_1>");
                    data.AppendLine("    <TIM_2>" + invoiceHeader.Tim_2_reteFuente + "</TIM_2>");
                    data.AppendLine("    <TIM_3>" + constantes.Divisa + "</TIM_3>");
                    data.AppendLine("    <TIM_4>" + Math.Round(tim4, 6) + "</TIM_4>");
                    data.AppendLine("    <TIM_5>" + constantes.Divisa + "</TIM_5>");
                    data.AppendLine("    <IMP>");
                    data.AppendLine("      <IMP_1>" + constantes.Imp_1_reteFuente + "</IMP_1>");
                    data.AppendLine("      <IMP_2>" + invoiceHeader.Imp_2_reteFuente_BaseImponible + "</IMP_2>");
                    data.AppendLine("      <IMP_3>" + constantes.Divisa + "</IMP_3>");
                    data.AppendLine("      <IMP_4>" + invoiceHeader.Imp_4_reteFuente_ImporteImpuesto + "</IMP_4>");
                    data.AppendLine("      <IMP_5>" + constantes.Divisa + "</IMP_5>");
                    data.AppendLine("      <IMP_6>" + String.Format("{0:0.00}",invoiceHeader.Imp_6_reteFuente_PorcentajeImpuesto) + "</IMP_6>");
                    data.AppendLine("    </IMP>");
                    data.AppendLine("  </TIM>");
                }


                if (invoiceHeader.Tim_2_reteIca > 0)
                {
                    var tim4 = (double)((invoiceHeader.Imp_2_reteIca_BaseImponible * (invoiceHeader.Imp_6_reteIca_PorcentajeImpuesto / 100)) -
                                            invoiceHeader.Imp_4_reteIca_ImporteImpuesto);
                    data.AppendLine("  <TIM>");
                    data.AppendLine("    <TIM_1>" + invoiceHeader.Tim_1_reteIca + "</TIM_1>");
                    data.AppendLine("    <TIM_2>" + invoiceHeader.Tim_2_reteIca + "</TIM_2>");
                    data.AppendLine("    <TIM_3>" + constantes.Divisa + "</TIM_3>");
                    data.AppendLine("    <TIM_4>" + Math.Round(tim4, 6) + "</TIM_4>");
                    data.AppendLine("    <TIM_5>" + constantes.Divisa + "</TIM_5>");
                    data.AppendLine("    <IMP>");
                    data.AppendLine("      <IMP_1>" + constantes.Imp_1_reteIca + "</IMP_1>");
                    data.AppendLine("      <IMP_2>" + invoiceHeader.Imp_2_reteIca_BaseImponible + "</IMP_2>");
                    data.AppendLine("      <IMP_3>" + constantes.Divisa + "</IMP_3>");
                    data.AppendLine("      <IMP_4>" + invoiceHeader.Imp_4_reteIca_ImporteImpuesto + "</IMP_4>");
                    data.AppendLine("      <IMP_5>" + constantes.Divisa + "</IMP_5>");
                    data.AppendLine("      <IMP_6>" + String.Format("{0:0.00}", invoiceHeader.Imp_6_reteIca_PorcentajeImpuesto) + "</IMP_6>");
                    data.AppendLine("    </IMP>");
                    data.AppendLine("  </TIM>");
                }

            }

            if (invoiceHeader.Tim_2_iva > 0)
            {
                var tim4 = (double)((invoiceHeader.Imp_2_iva_BaseImponible * (invoiceHeader.Imp_6_iva_PorcentajeImpuesto / 100)) -
                        invoiceHeader.Imp_4_iva_ImporteImpuesto);

                var tim6 = (double)invoiceHeader.Imp_6_iva_PorcentajeImpuesto;

                data.AppendLine("  <TIM>");
                data.AppendLine("    <TIM_1>" + invoiceHeader.Tim_1_iva + "</TIM_1>");
                data.AppendLine("    <TIM_2>" + invoiceHeader.Tim_2_iva + "</TIM_2>");
                data.AppendLine("    <TIM_3>" + constantes.Divisa + "</TIM_3>");
                data.AppendLine("    <TIM_4>" + Math.Round(tim4, 6) + "</TIM_4>");
                data.AppendLine("    <TIM_5>" + constantes.Divisa + "</TIM_5>");
                data.AppendLine("    <IMP>");
                data.AppendLine("      <IMP_1>" + constantes.Imp_1_iva + "</IMP_1>");
                data.AppendLine("      <IMP_2>" + invoiceHeader.Imp_2_iva_BaseImponible + "</IMP_2>");
                data.AppendLine("      <IMP_3>" + constantes.Divisa + "</IMP_3>");
                data.AppendLine("      <IMP_4>" + invoiceHeader.Imp_4_iva_ImporteImpuesto + "</IMP_4>");
                data.AppendLine("      <IMP_5>" + constantes.Divisa + "</IMP_5>");
                data.AppendLine("      <IMP_6>" + String.Format("{0:0.00}", Math.Round(tim6, 2)) + "</IMP_6>");
                data.AppendLine("    </IMP>");
                data.AppendLine("  </TIM>");
            }

            data.AppendLine("  <DSC>");
            data.AppendLine("    <DSC_1>" + constantes.Dsc_1 + "</DSC_1>");
            data.AppendLine("    <DSC_2>" + constantes.Dsc_2 + "</DSC_2>");
            data.AppendLine("    <DSC_3>" + invoiceHeader.Dsc_3_ValorDescuento + "</DSC_3>");
            data.AppendLine("    <DSC_4>" + constantes.Divisa + "</DSC_4>");
            data.AppendLine("    <DSC_5>" + constantes.Dsc_5 + "</DSC_5>");
            data.AppendLine("    <DSC_6>" + constantes.Dsc_6 + "</DSC_6>");
            data.AppendLine("    <DSC_7>" + invoiceHeader.Tot_1_ImporteBruto + "</DSC_7>");
            data.AppendLine("    <DSC_8>" + constantes.Divisa + "</DSC_8>");
            data.AppendLine("    <DSC_10>1</DSC_10>");
            data.AppendLine("  </DSC>");

            // 1057 TGR-CC-210329 Cambios FE Anexo 1.8
            // solo para notas:

            if (!invoiceHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
            {
                var nota = string.IsNullOrEmpty(invoiceHeader.Nota_3_Pos_1)
                    ? CarvajalInvoice.Constants().Cdn_2
                    : invoiceHeader.Nota_3_Pos_1;

                if (nota.Length < CarvajalInvoice.Constants().Cdn_2_MinCaracteres)
                    nota =
                        string.Format("{0}                    ", nota)
                            .Truncate(CarvajalInvoice.Constants().Cdn_2_MinCaracteres);

                data.AppendLine("  <CDN>");

                switch (invoiceHeader.Enc_1_TipoDocumento)
                {
                    case InvoiceCarvajal.NotaDebito:
                        data.AppendLine("    <CDN_1>" + constantes.Cdn_1_NotaDebito + "</CDN_1>");
                        break;
                    case InvoiceCarvajal.NotaCredito:
                        if (isCancelInvoice)
                            data.AppendLine("    <CDN_1>" + constantes.Cdn_1_NotaCreditoAnulaFactura + "</CDN_1>");
                        else
                            data.AppendLine("    <CDN_1>" + constantes.Cdn_1_NotaCredito + "</CDN_1>");
                        break;
                }

                data.AppendLine("    <CDN_2>" + nota + "</CDN_2>");
                //data.AppendLine("    <CDN_3>" + invoiceHeader.Nrf_1 + "</CDN_3>"); solo para validar
                data.AppendLine("  </CDN>");
            }


            string[] datoResolucion = invoiceHeader.Drf_Resolucion.Split('|');

            if (invoiceHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
            {
                data.AppendLine("  <DRF>");
                data.AppendLine("    <DRF_1>" + datoResolucion[1] + "</DRF_1>");
                data.AppendLine("    <DRF_2>" + datoResolucion[2] + "</DRF_2>");
                data.AppendLine("    <DRF_3>" + datoResolucion[3] + "</DRF_3>");
                data.AppendLine("    <DRF_4>" + invoiceHeader.Drf_4_Prefijo + "</DRF_4>");
                data.AppendLine("    <DRF_5>" + invoiceHeader.Drf_5_NumeracionRangoMinimo + "</DRF_5>");
                data.AppendLine("    <DRF_6>" + invoiceHeader.Drf_6_NumeracionRangoMaximo + "</DRF_6>");
                data.AppendLine("  </DRF>");
            }
            else
            {
                data.AppendLine("  <DRF>");
                data.AppendLine("    <DRF_1></DRF_1>");
                data.AppendLine("    <DRF_2></DRF_2>");
                data.AppendLine("    <DRF_3></DRF_3>");
                data.AppendLine("    <DRF_4>" + invoiceHeader.Drf_4_Prefijo + "</DRF_4>");
                data.AppendLine("    <DRF_5>" + invoiceHeader.Drf_5_NumeracionRangoMinimo + "</DRF_5>");
                data.AppendLine("    <DRF_6>" + invoiceHeader.Drf_6_NumeracionRangoMaximo + "</DRF_6>");
                data.AppendLine("  </DRF>");
            }

            // Notas:

            if (invoiceHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
            {
                // los note maximo de 500 caracteres, se truncan:

                var t = int.Parse(CarvajalInvoice.Constants().MaximoCaracteresNotTag);
                var notaUno =
                    ("1.=" + constantes.Not_1_Pos_1 + "|" + invoiceHeader.Nota_1_Pos_2 + "|" + constantes.Not_1_Pos_3)
                        .Truncate(t);
                var notaDos = ("2.=" + constantes.Not_2_Pos_1 + "|" + datoResolucion[0]).Truncate(t);
                var notaTres = ("3.=" + invoiceHeader.Nota_3_Pos_1).Truncate(t);
                var notaCuatro = ("4.=" + constantes.Not_4_Pos_1).Truncate(t);
                var notaSeis = ("6.=" + invoiceHeader.Nota_6_Pos_1 + "|" + invoiceHeader.Nota_6_Pos_2).Truncate(t);
                var notaNueve =
                    ("9.=" + constantes.Not_9_Pos_1 + "|" + constantes.Not_9_Pos_2 + "|" + constantes.Not_9_Pos_3)
                        .Truncate(t);
                var notaOnce = ("11.=||||" + invoiceHeader.Nota_11_Pos_5).Truncate(t);
                var notaDoce = ("12.=" + CarvajalInvoice.Constants().Not_12_Pos_1 +
                                invoiceHeader.Enc_17_UrlArchivosAnexos);

                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + notaUno + "</NOT_1>");
                data.AppendLine("  </NOT>");
                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + notaDos + "</NOT_1>");
                data.AppendLine("  </NOT>");
                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + notaTres + "</NOT_1>");
                data.AppendLine("  </NOT>");
                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + notaCuatro + "</NOT_1>");
                data.AppendLine("  </NOT>");
                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + notaSeis + "</NOT_1>");
                data.AppendLine("  </NOT>");
                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + notaNueve + "</NOT_1>");
                data.AppendLine("  </NOT>");
                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + notaOnce + "</NOT_1>");
                data.AppendLine("  </NOT>");
                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + notaDoce + "</NOT_1>");
                data.AppendLine("  </NOT>");
            }
            else
            {
                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + "1.=" + constantes.Not_1_Pos_1 + "|" + invoiceHeader.Nota_1_Pos_2 + "</NOT_1>");
                data.AppendLine("  </NOT>");
                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + "3.=" + invoiceHeader.Nota_3_Pos_1 + "</NOT_1>");
                data.AppendLine("  </NOT>");
                data.AppendLine("  <NOT>");
                data.AppendLine("    <NOT_1>" + "11.=||||" + invoiceHeader.Nota_11_Pos_5 + "</NOT_1>");
                data.AppendLine("  </NOT>");
            }

            data.AppendLine("  <ORC>");
            data.AppendLine("    <ORC_1>" + invoiceDetail.FirstOrDefault().Orc_1 + "</ORC_1>");
            data.AppendLine("  </ORC>");


            // 2019Oct18 Andres Rubiano Moreno: REF solo se debe enviar si la factura referencia es version 2.1, las facturas version 2.1
            //      tienen CUFE, se valida si hay cufe para enviar, si no hay es pq pertenece a la version 2.0 o que aun no tiene CUfe...
            // !string.IsNullOrEmpty(invoiceHeader.Ref_4_FacturaReferenciaCufe)

            //Maritza Nieto Giraldo
            //Nov 6, 2019, 1:41 PM
            //Buenas tardes
            //Solicitamos incluir nuevamente en el XML de las notas los tag que referencian la factura: REF_1, REF_2, REF_3 , REF_4 y REF_5 
            //(cómo te lo habíamos definido inicialmente). Por favor nos avisas cuando tengas el cambio.

            if (!invoiceHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
            {
                data.AppendLine("  <REF>");
                data.AppendLine("    <REF_1>" + constantes.Ref_1 + "</REF_1>");
                data.AppendLine("    <REF_2>" + invoiceHeader.Ref_2_FacturaReferencia + "</REF_2>");
                data.AppendLine("    <REF_3>" + invoiceHeader.Ref_3_FacturaReferenciaFecha + "</REF_3>");
                data.AppendLine("    <REF_4>" + invoiceHeader.Ref_4_FacturaReferenciaCufe + "</REF_4>");
                data.AppendLine("    <REF_5>" + constantes.Ref_5 + "</REF_5>");
                data.AppendLine("  </REF>");
            }

            data.AppendLine("  <MEP>");
            data.AppendLine("    <MEP_1>" + constantes.Mep_1 + "</MEP_1>");
            data.AppendLine("    <MEP_2>" + invoiceHeader.Mep_2_MetodoDePago + "</MEP_2>");

            if (invoiceHeader.Mep_2_MetodoDePago == "2") // 2 Mmetodo de pago a credito
                data.AppendLine("    <MEP_3>" + invoiceHeader.Mep_3_FechaDePago + "</MEP_3>");

            data.AppendLine("  </MEP>");

            data.AppendLine("  <CTS>");
            data.AppendLine("    <CTS_1>" + constantes.Cts_1 + "</CTS_1>");
            data.AppendLine("  </CTS>");

            // 1057 TGR-CC-210329 Cambios FE Anexo 1.8
            // Ajuste por correo: Re: IMPLEMENTACIÓN FECO: Estándar Factura Electrónica actualizado Anexo 1.8 18-Jun-2021

            #region no se debe enviar DCN:

            //if (!invoiceHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
            //{
            //    var nota = string.IsNullOrEmpty(invoiceHeader.Nota_3_Pos_1)
            //        ? CarvajalInvoice.Constants().Cdn_2
            //        : invoiceHeader.Nota_3_Pos_1;

            //    if (nota.Length < CarvajalInvoice.Constants().Cdn_2_MinCaracteres)
            //        nota =
            //            string.Format("{0}                    ", nota)
            //                .Truncate(CarvajalInvoice.Constants().Cdn_2_MinCaracteres);

            //    data.AppendLine("  <DCN>");
            //    data.AppendLine("    <DCN_1>" + nota + "</DCN_1>");
            //    data.AppendLine("  </DCN>");
            //}

            #endregion

            //if (!String.IsNullOrEmpty(invoiceHeader.Ovt_1))
            //{
            //    data.AppendLine("  <OVT>");
            //    data.AppendLine("    <OVT_1>" + invoiceHeader.Ovt_1 + "</OVT_1>");
            //    data.AppendLine("    <OVT_2>" + invoiceHeader.Ovt_2 + "</OVT_2>");
            //    data.AppendLine("    <OVT_3>" + constantes.Divisa + "</OVT_3>");
            //    data.AppendLine("  </OVT>");
            //}


            // detalle de invoice:

            foreach (var detalle in invoiceDetail)
            {
                data.AppendLine("  <ITE>");
                data.AppendLine("    <ITE_1>" + detalle.Ite_1 + "</ITE_1>");
                data.AppendLine("    <ITE_3>" + String.Format("{0:0.00}", detalle.Ite_3) + "</ITE_3>");
                data.AppendLine("    <ITE_4>" + detalle.Ite_4 + "</ITE_4>");
                data.AppendLine("    <ITE_5>" + detalle.Ite_5 + "</ITE_5>");
                data.AppendLine("    <ITE_6>" + constantes.Divisa + "</ITE_6>");
                data.AppendLine("    <ITE_7>" + detalle.Ite_7 + "</ITE_7>");
                data.AppendLine("    <ITE_8>" + constantes.Divisa + "</ITE_8>");
                data.AppendLine("    <ITE_11>" + detalle.Ite_11 + "</ITE_11>");
                data.AppendLine("    <ITE_18>" + detalle.Ite_18 + "</ITE_18>");
                data.AppendLine("    <ITE_21>" + detalle.Ite_21 + "</ITE_21>");
                data.AppendLine("    <ITE_22>" + constantes.Divisa + "</ITE_22>");
                data.AppendLine("    <ITE_27>" + detalle.Ite_3 + "</ITE_27>");
                data.AppendLine("    <ITE_28>" + detalle.Ite_4 + "</ITE_28>");
                data.AppendLine("    <IRF>");
                data.AppendLine("      <IRF_1>" + detalle.Irf_1_item_Remesa + "</IRF_1>");
                data.AppendLine("      <IRF_2>" + detalle.Irf_2_item_Remesa + "</IRF_2>");
                data.AppendLine("    </IRF>");
                data.AppendLine("    <IRF>");
                data.AppendLine("      <IRF_1>" + detalle.Irf_1_item_Placa + "</IRF_1>");
                data.AppendLine("      <IRF_2>" + detalle.Irf_2_item_Placa + "</IRF_2>");
                data.AppendLine("    </IRF>");
                // 2018Dic18 Solicitan que en ORC solo se envie el primer numero de referencia, todas las referencias se van a IRF con clave "ON":
                data.AppendLine("    <IRF>");
                data.AppendLine("      <IRF_1>" + detalle.Irf_1_item_Referencia + "</IRF_1>");
                data.AppendLine("      <IRF_2>" + detalle.Irf_2_item_Referencia + "</IRF_2>");
                data.AppendLine("    </IRF>");

                data.AppendLine("    <IAE>");
                data.AppendLine("      <IAE_1>" + detalle.Iae_1_CodigoProducto + "</IAE_1>");
                data.AppendLine("      <IAE_2>" + constantes.Iae_2 + "</IAE_2>");
                data.AppendLine("      <IAE_4>Estándar de adopción del contribuyente</IAE_4>");
                data.AppendLine("    </IAE>");

                // Si hay retencion ya conocemos la tasa de la retención y esta aplica para cada item, asi que se puede calcular.

                if (invoiceHeader.Tim_2_reteFuente > 0)
                {
                    var iim2 = (double)((detalle.tasaReteFuente / 100) * detalle.Ite_5);
                    var tii4 = iim2 - Math.Round(iim2, 0);

                    data.AppendLine("    <TII>");
                    //data.AppendLine("      <TII_1>" + String.Format("{0:0.00}", invoiceHeader.Tim_2_reteFuente) + "</TII_1>");
                    data.AppendLine("      <TII_1>" + (detalle.tasaReteFuente / 100) * detalle.Ite_5 + "</TII_1>");
                    data.AppendLine("      <TII_2>" + constantes.Divisa + "</TII_2>");
                    data.AppendLine("      <TII_3>true</TII_3>");
                    data.AppendLine("      <TII_4>" + Math.Round(tii4, 6) + "</TII_4>");
                    data.AppendLine("      <TII_5>" + constantes.Divisa + "</TII_5>");
                    data.AppendLine("      <IIM>");
                    data.AppendLine("        <IIM_1>" + constantes.Imp_1_reteFuente + "</IIM_1>");
                    data.AppendLine("        <IIM_2>" + Math.Round(iim2, 0) + "</IIM_2>");
                    data.AppendLine("        <IIM_3>" + constantes.Divisa + "</IIM_3>");
                    data.AppendLine("        <IIM_4>" + detalle.Ite_5 + "</IIM_4>");
                    data.AppendLine("        <IIM_5>" + constantes.Divisa + "</IIM_5>");
                    data.AppendLine("        <IIM_6>" + String.Format("{0:0.00}", detalle.tasaReteFuente) + "</IIM_6>");
                    data.AppendLine("      </IIM>");
                    data.AppendLine("    </TII>");
                }

                // Si hay iva, se valida si el item aplica IVA, hay iva para el item cuando el parametro TASARIVA tiene un valor para la actividad de venta del articulo del detalle
                // (parametro TASARIVA indica reteiva pero por alguna razon lo usaron para el IVA)
                // El invoiceDetail tiene le valor de la tasa:

                if (invoiceHeader.Tim_2_iva > 0 && detalle.tasaIva > 0)
                {
                    var iim2 = (double)((detalle.tasaIva / 100) * detalle.Ite_5);
                    var tii4 = iim2 - Math.Round(iim2, 0);
                    var tii6 = (double)(detalle.tasaIva);

                    data.AppendLine("    <TII>");
                    //data.AppendLine("      <TII_1>" + String.Format("{0:0.00}", invoiceHeader.Tim_2_iva) + "</TII_1>");
                    data.AppendLine("      <TII_1>" + invoiceHeader.Tim_2_iva + "</TII_1>");
                    data.AppendLine("      <TII_2>" + constantes.Divisa + "</TII_2>");
                    data.AppendLine("      <TII_3>false</TII_3>");
                    data.AppendLine("      <TII_4>" + tii4 + "</TII_4>");
                    data.AppendLine("      <TII_5>" + constantes.Divisa + "</TII_5>");
                    data.AppendLine("      <IIM>");
                    data.AppendLine("        <IIM_1>" + constantes.Imp_1_iva + "</IIM_1>");
                    data.AppendLine("        <IIM_2>" + Math.Round(iim2, 0) + "</IIM_2>");
                    data.AppendLine("        <IIM_3>" + constantes.Divisa + "</IIM_3>");
                    data.AppendLine("        <IIM_4>" + detalle.Ite_5 + "</IIM_4>");
                    data.AppendLine("        <IIM_5>" + constantes.Divisa + "</IIM_5>");
                    data.AppendLine("        <IIM_6>" + String.Format("{0:0.00}", Math.Round(tii6, 2)) + "</IIM_6>");
                    data.AppendLine("      </IIM>");
                    data.AppendLine("    </TII>");
                }

                //1057 TGR-CC-210329 Cambios FE Anexo 1.8:
                // 1069-TGR-CC-210610 FE Monitor - Aceptacion Tacita

                #region Etiquetas IEI solo si viene numero de remesa:

                if (!string.IsNullOrEmpty(detalle.Irf_2_item_Remesa) && !detalle.Irf_2_item_Remesa.Equals("-"))
                {
                    if (!string.IsNullOrEmpty(detalle.remesaIdRndc))
                    {
                        data.AppendLine("    <IEI>");
                        data.AppendLine("      <IEI_1>01</IEI_1>");
                        data.AppendLine("      <IEI_2>" + detalle.remesaIdRndc + "</IEI_2>");
                        data.AppendLine("      <IEI_3>" + detalle.Ite_3 + "</IEI_3>");
                        data.AppendLine("      <IEI_4>KGM</IEI_4>");
                        data.AppendLine("    </IEI>");
                    }

                    data.AppendLine("    <IEI>");
                    data.AppendLine("      <IEI_1>02</IEI_1>");
                    data.AppendLine("      <IEI_2>" + detalle.Irf_2_item_Remesa + "</IEI_2>");
                    data.AppendLine("      <IEI_3>" + detalle.Ite_3 + "</IEI_3>");
                    data.AppendLine("      <IEI_4>KGM</IEI_4>");
                    data.AppendLine("    </IEI>");

                    data.AppendLine("    <IEI>");
                    data.AppendLine("      <IEI_1>03</IEI_1>");
                    data.AppendLine("      <IEI_2>" + detalle.Ite_21 + "</IEI_2>");
                    data.AppendLine("      <IEI_3>" + detalle.Ite_3 + "</IEI_3>");
                    data.AppendLine("      <IEI_4>KGM</IEI_4>");
                    data.AppendLine("    </IEI>");
                }

                #endregion

                data.AppendLine("    <IDI>");
                data.AppendLine("      <IDI_1>" + invoiceHeader.Idi_1 + "</IDI_1>");
                data.AppendLine("    </IDI>");

                data.AppendLine("  </ITE>");
            }

            if (!invoiceHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
            {
                data.AppendLine("  <NRF>");
                data.AppendLine("    <NRF_1>" + invoiceHeader.Nrf_1 + "</NRF_1>");
                data.AppendLine("  </NRF>");
            }

            // 1057 TGR-CC-210329 Cambios FE Anexo 1.8
            // Extensiones:

            #region Ya no aplican las extensiones, migran a IEI:
            // 1069-TGR-CC-210610 FE Monitor - Aceptacion Tacita

            //var remesas =
            //    invoiceDetail.Where(d => !d.Irf_2_item_Remesa.Equals("-") && !string.IsNullOrEmpty(d.Irf_1_item_Remesa));
            //int contadorEgc = 0;
            //int contadorExt = 0;
            //int cantidadRemesas = remesas.Count();

            //#region Version 2 para manejo de ext y egc

            //if (cantidadRemesas > 0)
            //{
            //    data.AppendLine("  <EXT>");
            //    data.AppendLine("    <EXT_1>" + constantes.Ext_1 + "</EXT_1>");

            //    foreach (var detail in remesas)
            //    {
            //        if (contadorExt > CarvajalInvoice.Constants().MaximoDeExt)
            //            continue;

            //        #region Registro EGC maximo 20 repeticiones

            //        data.AppendLine("    <EGC>");
            //        data.AppendLine("      <EGC_1>" + detail.Irf_2_item_Remesa + "</EGC_1>");

            //        if (!string.IsNullOrEmpty(detail.remesaIdRndc))
            //        {
            //            data.AppendLine("      <ECA>");
            //            data.AppendLine("        <ECA_1>01</ECA_1>");
            //            data.AppendLine("        <ECA_2>" + detail.remesaIdRndc + "</ECA_2>");
            //            data.AppendLine("      </ECA>");
            //        }

            //        data.AppendLine("      <ECA>");
            //        data.AppendLine("        <ECA_1>02</ECA_1>");
            //        data.AppendLine("        <ECA_2>" + detail.Irf_2_item_Remesa + "</ECA_2>");
            //        data.AppendLine("      </ECA>");

            //        data.AppendLine("      <ECA>");
            //        data.AppendLine("        <ECA_1>03</ECA_1>");
            //        data.AppendLine("        <ECA_2>" + detail.Ite_21 + "</ECA_2>");
            //        data.AppendLine("      </ECA>");

            //        data.AppendLine("      <ECA>");
            //        data.AppendLine("        <ECA_1>Cantidad transportada</ECA_1>");
            //        data.AppendLine("        <ECA_2>" + detail.Ite_3 + "</ECA_2>");
            //        data.AppendLine("      </ECA>");

            //        data.AppendLine("      <ECA>");
            //        data.AppendLine("        <ECA_1>Unidad de medida</ECA_1>");
            //        data.AppendLine("        <ECA_2>" + detail.Ite_4 + "</ECA_2>");
            //        data.AppendLine("      </ECA>");

            //        data.AppendLine("      <ECA>");
            //        data.AppendLine("        <ECA_1>Tipo servicio</ECA_1>");
            //        data.AppendLine("        <ECA_2>1</ECA_2>");
            //        data.AppendLine("      </ECA>");

            //        data.AppendLine("      <ECA>");
            //        data.AppendLine("        <ECA_1>Orden de compra</ECA_1>");
            //        data.AppendLine("        <ECA_2>" + detail.Irf_2_item_Referencia + "</ECA_2>");
            //        data.AppendLine("      </ECA>");

            //        #endregion

            //        data.AppendLine("    </EGC>");
            //        contadorEgc = contadorEgc + 1;

            //        if (contadorEgc == CarvajalInvoice.Constants().MaximoDeEgc && cantidadRemesas > CarvajalInvoice.Constants().MaximoDeEgc)
            //        {
            //            // nuevo ext:
            //            // cierra el actual:
            //            data.AppendLine("  </EXT>");
            //            // abre el nuevo
            //            data.AppendLine("  <EXT>");
            //            data.AppendLine("    <EXT_1>" + constantes.Ext_1 + "</EXT_1>");
            //            contadorEgc = 0;
            //            contadorExt = contadorExt + 1;
            //        }
            //    }
            //    data.AppendLine("  </EXT>");
            //}

            //#endregion

            #endregion

            data.AppendLine(string.Format("</{0}>", InvoiceCarvajal.TypeInvoice[invoiceHeader.Enc_1_TipoDocumento]));
            return data;
        }
    }
}
