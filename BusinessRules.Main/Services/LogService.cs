﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using BusinessRules.Main.DTOs;
using BusinessRules.Main.Resources;
using Common;
using Common.Constants;
using Infrastructure.Data.Main.EF;

namespace BusinessRules.Main.Services
{
    public static class LogService
    {
        /// <summary>
        /// Metodo que se encarga se guardar traza de las llamadas a los servicios
        /// </summary>
        /// <param name="operationName">Nombre del mètodo</param>
        /// <param name="request">Request en formato SOAP</param>
        /// <param name="response">Response en formato SOAP</param>
        public static void CreateSoapCallLog(string operationName, string request, string response) //Nota: Este metodo se dejo de usar para el log de servicios web ya que se implemento el IClientMessageInspector 
        {
            //Extraer los datos de referencia del mensaje si así esta configurado
            var configuration = GetConfiguration(operationName);

            using (var entity = new AsTransLogEntities())
            {
                var soapCallLog = new SoapCallLog
                {
                    OperationName = operationName,
                    Request = request,
                    Response = response,
                    CallDateTime = DateTime.Now,
                    OperationCode = configuration != null ? configuration.OperationCode : null,
                    Reference = configuration != null ? GetSoapReference(request, configuration) : null
                };

                entity.SoapCallLogs.Add(soapCallLog);
                entity.SaveChanges();
            }
        }

        public static void CreateMessageLog(List<MessageDTO> messagesDto, string ucrCode, string interfaz,string evesource=null)
        {
            foreach (var messageDTO in messagesDto)
            {
                if (messageDTO.TypeEnum != MessagesConstants.Types.Information)
                {
                    Utility.RegistrarEventoNegocio(
                        EventTypesCodeConstants.Facturacion,
                        interfaz,
                        messageDTO.Message,
                        ucrCode,
                        messageDTO.ErrorCode,
                        null,
                        null,
                        evesource
                    );
                }
            }
        }

        public static void CreateMessageLog(MessageDTO messageDto, string ucrCode, string interfaz, string evesource = null)
        {
            var listMessage = new List<MessageDTO>();
            listMessage.Add(messageDto);
            CreateMessageLog(listMessage, ucrCode, interfaz,evesource);
        }

        /// <summary>
        /// Simplexity S.A
        /// Method: GetConfiguration
        /// Author: Cesar Fernando Estupiñan 
        /// Date: 16/11/2013
        /// Objective: Método utilizado para traer la configuración correspondiente desde el .config
        /// </summary>
        /// <param name="operationName">Name of the operation.</param>
        /// <returns>LogServiceConfigElement</returns>
        private static LogServiceConfigElement GetConfiguration(string operationName)
        {
            var configSection = (LogServiceConfigSection)ConfigurationManager.GetSection(Configurations.LogServiceConfigurationSection);
            var logServiceConfigurations = new List<LogServiceConfigElement>();
            LogServiceConfigElement configuration = null;

            if (configSection != null)
                logServiceConfigurations.AddRange(configSection.LogServiceConfiguration.Cast<LogServiceConfigElement>());

            if (logServiceConfigurations.Any())
                configuration = logServiceConfigurations.FirstOrDefault(p => p.OperationName.Equals(operationName));

            return configuration;
        }


        /// <summary>
        /// Simplexity S.A
        /// Method: GetSoapReference
        /// Author: Cesar Fernando Estupiñan 
        /// Date: 16/11/2013
        /// Objective: Método que obtiene el valor de referencia soap
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="configuration">The configuration.</param>
        /// <returns>String</returns>
        private static string GetSoapReference(string request, LogServiceConfigElement configuration)
        {
            string transformRequest = Utility.RemoveXMLNamespace(request);
            XmlReader xmlReader = XmlReader.Create(new StringReader(transformRequest));
            XElement document = XElement.Load(xmlReader);

            string soapreference = (from doc in document.Descendants(configuration.SoapReference)
                                    select (string)doc).FirstOrDefault();
            return soapreference;
        }
    }
}
