﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Transactions;
using BusinessRules.Main.DTOs.BRProviderService;
using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using Common;
using Common.Constants;
using Infrastructure.Data.Main.EF;

namespace BusinessRules.Main.Services.ServiceBroker
{
    public class ServicesBroker
    {
        /// <summary>
        /// Asigna datos y crea un nuevo objeto completo de tipo brServiceTaskDTO
        /// </summary>
        /// <param name="brServiceTask">Objeto con datos nuevos para una inserción en la db</param>
        /// <param name="db">Objeto de entidad de base de datos</param>
        public static void CreateServiceTask(brServiceTaskDTO brServiceTask, AsTransEntities db)
        {
            var serviceTastData = new brServiceTask
            {
                StkType = (short)brServiceTask.StkType,
                StkService_SrvCode = brServiceTask.StkService_SrvCode,
                StkReference = brServiceTask.StkReference,
                StkConditionService_SrvCode = brServiceTask.StkConditionService_SrvCode,
                StkConditionReference = brServiceTask.StkConditionReference,
                StkState = (int)ServiceBrokerConstants.ServiceTaskState.Scheduled,
                StkAttempt = (short)brServiceTask.StkAttempt,
                StkProcessDate = brServiceTask.StkProcessDate,
                StkProcessedDate = brServiceTask.StkProcessedDate,
                StkCreationDate = DateTime.Now,
                StkCreation_UsrCode = brServiceTask.StkCreation_UsrCode,
                stkObjectJson = brServiceTask.StkObjectJson
            };

            //db.AddObject("brServiceTask", serviceTastData);
            db.brServiceTask.Add(serviceTastData);

        }
        /// <summary>
        /// Actualiza datos y modifica datos traídos de la tarea, crea objeto con datos modificados de tipo brServiceTaskDTO
        /// </summary>
        /// <param name="brServiceTask">Objeto con datos modificados para una actualización en la db</param>
        /// <param name="db">Objeto de entidad de base de datos</param>
        public static void UpdateServiceTask(brServiceTaskDTO brServiceTask, AsTransEntities db)
        {
            var update = new brServiceTask();
            update = db.brServiceTask.FirstOrDefault(a => a.StkId == brServiceTask.StkId);
            update.StkSuccessed = brServiceTask.StkSuccessed;
            update.StkError = brServiceTask.StkError;
            update.StkMessage = brServiceTask.StkMessage.Truncate(1000);
            update.StkState = (short)brServiceTask.StkState;
            update.StkProcessedDate = DateTime.Now;
            update.StkRequestID = brServiceTask.StkRequestID;
            update.StkResponseID = brServiceTask.StkResponseID;
            update.stkObjectJson = brServiceTask.StkObjectJson;
        }
        /// <summary>
        /// Obtiene cinco tareas que están actualmente pendientes para proceso
        /// </summary>
        /// <returns>Un objeto de tipo lista con cinco tareas de tipo brServiceTaskDTO</returns>
        public static List<brServiceTaskDTO> GetServiceTask(int top)
        {
            if (top == 0)
                top = 10;

            using (var entity = new AsTransEntities())
            {
                var pallet =
                    (from serviceTask in entity.brServiceTask
                     where serviceTask.StkProcessDate <= DateTime.Now && serviceTask.StkState == (int)ServiceBrokerConstants.ServiceTaskState.Scheduled
                     select new brServiceTaskDTO()
                     {
                         StkId = serviceTask.StkId,
                         StkType = serviceTask.StkType,
                         StkService_SrvCode = serviceTask.StkService_SrvCode,
                         StkReference = serviceTask.StkReference,
                         StkConditionService_SrvCode = serviceTask.StkConditionService_SrvCode,
                         StkConditionReference = serviceTask.StkConditionReference,
                         StkState = serviceTask.StkState,
                         StkAttempt = serviceTask.StkAttempt == null ? 0 : (int)serviceTask.StkAttempt,
                         StkProcessDate = serviceTask.StkProcessDate,
                         StkCreation_UsrCode = serviceTask.StkCreation_UsrCode,
                         StkObjectJson = serviceTask.stkObjectJson
                     }
                     ).OrderBy(var => var.StkProcessDate).Take(top);
                return pallet.ToList();
            }
        }

        /// <summary>
        /// Obtiene cinco tareas que están actualmente pendientes para proceso
        /// </summary>
        /// <returns>Un objeto de tipo lista con cinco tareas de tipo brServiceTaskDTO</returns>
        public static List<brServiceTaskDTO> GetServiceTaskByCodes(int top, IEnumerable<string> configTasks)
        {
            if (top == 0)
                top = 10;

            using (var entity = new AsTransEntities())
            {
                var pallet =
                    (from serviceTask in entity.brServiceTask
                     where serviceTask.StkProcessDate <= DateTime.Now && serviceTask.StkState == (int)ServiceBrokerConstants.ServiceTaskState.Scheduled &&
                     configTasks.Contains(serviceTask.StkService_SrvCode)
                     select new brServiceTaskDTO()
                     {
                         StkId = serviceTask.StkId,
                         StkType = serviceTask.StkType,
                         StkService_SrvCode = serviceTask.StkService_SrvCode,
                         StkReference = serviceTask.StkReference,
                         StkConditionService_SrvCode = serviceTask.StkConditionService_SrvCode,
                         StkConditionReference = serviceTask.StkConditionReference,
                         StkState = serviceTask.StkState,
                         StkAttempt = serviceTask.StkAttempt == null ? 0 : (int)serviceTask.StkAttempt,
                         StkProcessDate = serviceTask.StkProcessDate,
                         StkCreation_UsrCode = serviceTask.StkCreation_UsrCode,
                         StkObjectJson = serviceTask.stkObjectJson
                     }
                     ).OrderBy(var => var.StkProcessDate).Take(top);
                return pallet.ToList();
            }
        }
        /// <summary>
        /// Modifica a estado Processing(2) las tareas traídas en consulta previa
        /// </summary>
        /// <param name="serviceTask">Objeto con lista de datos previamente consultados</param>
        public static void StateTask(List<brServiceTaskDTO> serviceTask)
        {
            using (var db = new AsTransEntities())
            {
                foreach (brServiceTaskDTO brServiceTask in serviceTask)
                {
                    brServiceTask.StkState = (int)ServiceBrokerConstants.ServiceTaskState.Processing;
                    UpdateServiceTask(brServiceTask, db);
                }
                using (var transaction = new TransactionScope())
                {
                    db.SaveChanges();
                    transaction.Complete();
                }
            }
        }
        /// <summary>
        /// Valida datos de precondiciones para posterior proceso en la tarea
        /// </summary>
        /// <param name="serviceTask">Objeto con datos de tarea para validacion</param>
        /// <param name="listDataService">Objeto con datos de precondición para validación</param>
        /// <returns>Estado de precondicion</returns>
        public static int ValidateServiceTaskPrecondition(brServiceTaskDTO serviceTask, List<brServiceDTO> listDataService)
        {
            using (var entity = new AsTransEntities())
            {
                if (serviceTask.StkType == (int)ServiceBrokerConstants.ServiceTaskTypes.PreconditionRetry || serviceTask.StkType == (int)ServiceBrokerConstants.ServiceTaskTypes.PosconditionRetry)
                {
                    return (int)ServiceBrokerConstants.TaskPreconditionState.Valid;
                }
                var preCondition =
                    (from VAR in listDataService where VAR.SrvCode == serviceTask.StkService_SrvCode && VAR.SrvCondition select VAR).Count();
                if (preCondition <= 0)
                {
                    return (int)ServiceBrokerConstants.TaskPreconditionState.Valid;
                }
                var task = (from VAR in entity.brServiceTask
                            where
                                VAR.StkService_SrvCode == serviceTask.StkConditionService_SrvCode &&
                                VAR.StkReference == serviceTask.StkConditionReference
                            orderby VAR.StkCreationDate ascending
                            select VAR).FirstOrDefault();
                if (task != null)
                {
                    if (task.StkState == (int)ServiceBrokerConstants.ServiceTaskState.Processed && task.StkSuccessed == true)
                    {
                        return (int)ServiceBrokerConstants.TaskPreconditionState.Valid;
                    }
                    if (task.StkState == (int)ServiceBrokerConstants.ServiceTaskState.Processed && (task.StkSuccessed == false || task.StkError == true))
                    {
                        return (int)ServiceBrokerConstants.TaskPreconditionState.Error;
                    }
                    if (task.StkState == (int)ServiceBrokerConstants.ServiceTaskState.Scheduled)
                    {
                        return (int)ServiceBrokerConstants.TaskPreconditionState.Scheduled;
                    }
                    if (task.StkState == (int)ServiceBrokerConstants.ServiceTaskState.Processing)
                    {
                        return (int)ServiceBrokerConstants.TaskPreconditionState.Processing;
                    }
                }
                else
                {
                    return (int)ServiceBrokerConstants.TaskPreconditionState.Valid;
                }
            }
            return (int)ServiceBrokerConstants.TaskPreconditionState.Error;
        }
        /// <summary>
        /// Proceso que se ejecuta cuando está en estado de Error este genera una nueva tarea y modifica la tarea padre
        /// </summary>
        /// <param name="brServiceTask">Objeto con datos de tarea para modificación y creación de nuevos datos</param>
        public static void ErrorTask(brServiceTaskDTO brServiceTask)
        {
            //PENDIENTE de mensaje
            var createTaskError = new brServiceTaskDTO
            {
                StkType = (int)ServiceBrokerConstants.ServiceTaskTypes.PreconditionRetry,
                StkState = (int)ServiceBrokerConstants.ServiceTaskState.Processed,
                StkService_SrvCode = brServiceTask.StkConditionService_SrvCode,
                StkReference = brServiceTask.StkConditionReference,
                StkConditionService_SrvCode = brServiceTask.StkService_SrvCode,
                StkConditionReference = brServiceTask.StkReference,
                StkProcessDate = DateTime.Now,
                StkCreation_UsrCode = ServiceBrokerConstants.Datos.user,
                StkObjectJson = brServiceTask.StkObjectJson
            };
            var updateTaskError = new brServiceTaskDTO();
            updateTaskError = brServiceTask;
            updateTaskError.StkSuccessed = false;
            updateTaskError.StkError = false;
            updateTaskError.StkMessage = "ErrorTask";
            updateTaskError.StkState = (int)ServiceBrokerConstants.ServiceTaskState.Processed;
            updateTaskError.StkProcessedDate = DateTime.Now;

            using (var db = new AsTransEntities())
            {
                CreateServiceTask(createTaskError, db);
                UpdateServiceTask(updateTaskError, db);
                using (var transaction = new TransactionScope())
                {
                    db.SaveChanges();
                    transaction.Complete();
                }
            }
        }
        /// <summary>
        /// Proceso que se ejecuta cuando está en estado de “Scheduled” este modifica la tarea con nuevos datos
        /// </summary>
        /// <param name="brServiceTask">Objeto con datos de tarea para modificación</param>
        public static void TaskLocked(brServiceTaskDTO brServiceTask)
        {
            //PENDIENTE de mensaje
            var updateTaskError = new brServiceTaskDTO();
            updateTaskError = brServiceTask;
            updateTaskError.StkSuccessed = false;
            updateTaskError.StkError = false;
            updateTaskError.StkMessage = ServiceBrokerConstants.Mensajes.bloqueo;
            updateTaskError.StkState = (int)ServiceBrokerConstants.ServiceTaskState.Locked;
            updateTaskError.StkProcessedDate = DateTime.Now;
            using (var db = new AsTransEntities())
            {
                UpdateServiceTask(updateTaskError, db);
                using (var transaction = new TransactionScope())
                {
                    db.SaveChanges();
                    transaction.Complete();
                }
            }
        }
        /// <summary>
        /// Procesa tarea si se en cuenta en archivo de recursos
        /// </summary>
        /// <param name="brServiceTask">Objeto con datos de tarea para modificación</param>
        public static void ExecuteTask(brServiceTaskDTO brServiceTask, BrProviderServiceDTO providerService)
        {
            ITaskProcessor taskProcessor =
                 (ITaskProcessor)Spring.GetObject(brServiceTask.StkService_SrvCode, SpringContext.ServiceBrokerContext);

            brServiceTask = taskProcessor.ExecuteTask(brServiceTask);
            brServiceTask.StkState = (int)ServiceBrokerConstants.ServiceTaskState.Processed;
            using (var db = new AsTransEntities())
            {
                UpdateServiceTask(brServiceTask, db);
                using (var transaction = new TransactionScope())
                {
                    db.SaveChanges();
                    transaction.Complete();
                }
            }

        }
        /// <summary>
        /// Valida los tipos de la tarea procesada previamente
        /// </summary>
        /// <param name="brServiceTask">Objeto con datos de tarea para validar los tipos</param>
        /// <param name="listDataService">Objeto con datos para validación de tipos</param>
        public static void ProcessResponses(brServiceTaskDTO brServiceTask, List<brServiceDTO> listDataService)
        {
            switch (brServiceTask.StkType)
            {
                case (int)ServiceBrokerConstants.ServiceTaskTypes.PreconditionRetry:
                    if (brServiceTask.StkSuccessed) processPreRetry(brServiceTask, listDataService);
                    break;
                case (int)ServiceBrokerConstants.ServiceTaskTypes.AutomaticRetry:
                case (int)ServiceBrokerConstants.ServiceTaskTypes.New:
                    var preCondition =
                    (from VAR in listDataService where VAR.SrvCode == brServiceTask.StkService_SrvCode select VAR).FirstOrDefault();
                    if (preCondition.SrvAutomaticRetry && preCondition.SrvRetries > brServiceTask.StkAttempt)
                    {
                        if (!brServiceTask.StkSuccessed) ProcessRetry(brServiceTask, listDataService);
                    }
                    break;
            }
        }
        /// <summary>
        /// Crea una nueva tarea a partir de un objeto padre
        /// </summary>
        /// <param name="brServiceTask">Objeto padre para creación de nueva tarea</param>
        /// <param name="listDataService">Lista de datos para verificación de reintento y cantidad de reintentos</param>
        public static void processPreRetry(brServiceTaskDTO brServiceTask, List<brServiceDTO> listDataService)
        {
            var retry =
                    (from VAR in listDataService where VAR.SrvCode == brServiceTask.StkService_SrvCode && VAR.SrvConditionRetry select VAR).Count();
            if (retry <= 0)
            {
                //PENDIENTE DATOS DE RETORNO
                return;
            }
            var createTask = new brServiceTaskDTO
            {
                StkType = (int)ServiceBrokerConstants.ServiceTaskTypes.PosconditionRetry,
                StkState = (int)ServiceBrokerConstants.ServiceTaskState.Scheduled,
                StkService_SrvCode = brServiceTask.StkConditionService_SrvCode,
                StkReference = brServiceTask.StkConditionReference,
                StkProcessDate = DateTime.Now,
                StkCreationDate = DateTime.Now,
                StkCreation_UsrCode = ServiceBrokerConstants.Datos.user,
                StkObjectJson = brServiceTask.StkObjectJson
            };
            using (var db = new AsTransEntities())
            {
                CreateServiceTask(createTask, db);
                using (var transaction = new TransactionScope())
                {
                    db.SaveChanges();
                    transaction.Complete();
                }
            }
        }
        /// <summary>
        /// Genera una nueva tarea a partir de una tarea padre con datos modificados
        /// </summary>
        /// <param name="brServiceTask">Objeto padre para creación de nueva tarea</param>
        /// <param name="listDataService">Lista de datos para modificación de datos</param>
        public static void ProcessRetry(brServiceTaskDTO brServiceTask, List<brServiceDTO> listDataService)
        {
            var retry =
                    (from VAR in listDataService
                     where VAR.SrvCode == brServiceTask.StkService_SrvCode
                     select VAR).FirstOrDefault();
            var createTask = new brServiceTaskDTO
            {
                StkType = (int)ServiceBrokerConstants.ServiceTaskTypes.AutomaticRetry,
                StkState = (int)ServiceBrokerConstants.ServiceTaskState.Scheduled,
                StkService_SrvCode = brServiceTask.StkService_SrvCode,
                StkReference = brServiceTask.StkReference,
                StkConditionService_SrvCode = brServiceTask.StkConditionService_SrvCode,
                StkConditionReference = brServiceTask.StkConditionReference,
                StkAttempt = brServiceTask.StkAttempt + 1,
                StkProcessDate = DateTime.Now.AddSeconds(retry.SrvRetryTime),
                StkCreation_UsrCode = ServiceBrokerConstants.Datos.user,
                StkObjectJson = brServiceTask.StkObjectJson
            };
            using (var db = new AsTransEntities())
            {
                CreateServiceTask(createTask, db);
                using (var transaction = new TransactionScope())
                {
                    db.SaveChanges();
                    transaction.Complete();
                }
            }
        }


        public void VerifyRetryTask()
        {

        }
        public void GetService()
        {

        }
        public void GetServices()
        {

        }
        public void GetServiceProvider()
        {

        }
        public void GetServiceProviders()
        {

        }
        /// <summary>
        /// Generación de objeto de tipo brServiceDTO para mantener datos para el proceso
        /// </summary>
        /// <returns>Una lista con objetos de tipo brServiceDTO</returns>
        public static List<brServiceDTO> brServiceData()
        {
            try
            {
                using (var entity = new AsTransEntities())
                {
                    var query = (from serviceBr in entity.brService
                                 select new brServiceDTO()
                                 {
                                     SrvCode = serviceBr.SrvCode,
                                     SrvName = serviceBr.SrvName,
                                     SrvState = serviceBr.SrvState,
                                     SrvServiceProvider_SprCode = serviceBr.SrvServiceProvider_SprCode,
                                     SrvRetries = (int)serviceBr.SrvRetries,
                                     SrvCondition = (bool)serviceBr.SrvCondition,
                                     SrvConditionRetry = (bool)serviceBr.SrvConditionRetry,
                                     SrvRetryTime = (int)serviceBr.SrvRetryTime,
                                     SrvAutomaticRetry = (bool)serviceBr.SrvAutomaticRetry,
                                 });
                    return query.ToList();
                }
            }
            catch (Exception ex)
            {
                var demo = ex.Message;
                return new List<brServiceDTO>();
            }
        }

        public static IEnumerable<string> GetConfigurationTasksByPriority(string priority)
        {
            var configSection = (PriorityTaskConfigSection)ConfigurationManager.GetSection(BusinessRules.Main.Resources.Configurations.PriorityTaskConfigurationSection);
            var priorityTaskConfigurations = new List<PriorityTaskConfigElement>();
            
            if (configSection != null)
                priorityTaskConfigurations.AddRange(configSection.PriorityTaskConfiguration.Cast<PriorityTaskConfigElement>());

            // IEnumerable<PriorityTaskConfigElement> query = from c in priorityTaskConfigurations where c.Priority == priority select c;
            IEnumerable<string> query = from c in priorityTaskConfigurations where c.Priority == priority select c.OperationCode;



            return query;
        }
    }
}
