﻿using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using BusinessRules.Main.Resources;
using Common;
using Infrastructure.Data.Main.EF;
using Infrastructure.Interoperability.LoadShipmentMonitorService;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using static BusinessRules.Main.VoyageMonitorUtils;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    // VOY-103 - Integrar Anulacion de remesa a Monitor
    public class VoyageMonitorVOY103 : ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            try
            {
                string voyageNumber = task.StkReference;

                using (var db = new AsTransEntities())
                {

                    // Obtiene los datos del viaje
                    var voyage = db
                        .GetVoyageToMonitor(task.StkReference)
                        .FirstOrDefault();

                    if (voyage is null)
                    {
                        task.EstablecerEnError(string.Format(Messages.errVoyageNotFound, task.StkReference));
                        return task;
                    }

                    var cancelCargoData = JsonConvert.DeserializeObject<CancelCargoData>(task.StkObjectJson);

                    if(cancelCargoData is null)
                    {
                        task.EstablecerEnError(Messages.errCancelCargoDataNotFound);
                        return task;
                    }

                    if(string.IsNullOrEmpty(cancelCargoData.RemesaNumero) || string.IsNullOrEmpty(cancelCargoData.ViajeNumero))
                    {
                        task.EstablecerEnError(Messages.errCancelCargoDataNotValid);
                        return task;
                    }

                    var currentVoyageMonitor = db.VoyageMonitor
                        .FirstOrDefault(x => x.VomVoyageNumber.Equals(voyageNumber));

                    // Validar si el viaje esta como integrado a Monitor
                    if (currentVoyageMonitor != null && currentVoyageMonitor.VomIntegrationState != (int)VoyageIntegrationState.Integrado)
                    {
                        task.EstablecerEnError(string.Format(Messages.errVoyageIsNotIntegrated, voyageNumber));
                        return task;
                    }

                    var currentVoyageQueue = db.VoyageMonitorQueue
                        .FirstOrDefault(x => x.VmqVoyageNumber.Equals(voyageNumber));

                    currentVoyageQueue = AddOrUpdateVoyageMonitorQueue(voyageNumber, db, currentVoyageQueue, string.Empty, Constants.CancelIntegration);

                    // Se instancia el servicio Monitor
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var service = new LoadShipmentServiceClient();

                    var request = new cancelCargos { idManifest = voyageNumber, delete = VoyageMonitorConfig.Constants().CancelCargoDelete, idCargo = cancelCargoData.RemesaNumero };

                    // Guarda el xml enviado
                    var serializedRequest = Utility.SerializeObject(request);
                    task.StkRequestID = Utility.RegistrarRequest(serializedRequest);

                    var response = service.cancelCargo(GetToken(), request);

                    if (response is null)
                    {
                        task.EstablecerEnError(Messages.errServiceDidNotRespond);

                        currentVoyageMonitor.VomCancellationState = (int)VoyageIntegrationState.Error;

                        UpdateVoyageMonitorData(voyageNumber, db, currentVoyageQueue, serializedRequest,$"{Messages.errServiceDidNotRespond} [{cancelCargoData.RemesaNumero}]");

                        return task;
                    }

                    // Guarda el xml de respuesta
                    var serializedResponse = Utility.SerializeObject(response);
                    task.StkResponseID = Utility.RegistrarResponse(serializedResponse);

                    if (response.code != VoyageMonitorConfig.Constants().SuccessCode)
                    {
                        task.EstablecerEnError(string.Format(Messages.errMonitorError, response.code, response.description, cancelCargoData.RemesaNumero));

                        currentVoyageMonitor.VomCancellationState = (int)VoyageIntegrationState.Error;

                        UpdateVoyageMonitorData(voyageNumber, db, currentVoyageQueue, serializedRequest, $"{response.description} [{cancelCargoData.RemesaNumero}]");

                        return task;
                    }

                    UpdateVoyageMonitorData(voyageNumber, db, currentVoyageQueue, serializedRequest, $"{response.description} [{cancelCargoData.RemesaNumero}]");

                    task.EstablecerEnOk("AsTrans: Tarea finalizada correctamente.");
                }
            }
            catch (System.Exception ex)
            {
                _ = ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out _);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }

            return task;
        }

        private authToken GetToken()
        {
            var user = VoyageMonitorConfig.Constants().User;
            var password = VoyageMonitorConfig.Constants().Password;

            return new authToken
            {
                authUser = user,
                authPassword = password
            };
        }

        private static void UpdateVoyageMonitorData(string voyageNumber, AsTransEntities db, VoyageMonitorQueue currentVoyageQueue, string serializedRequest, string message)
        {
            AddOrUpdateVoyageMonitorQueue(voyageNumber, db, currentVoyageQueue, message, Constants.CancelCargo);

            AddVoyageMonitorHistory(db, voyageNumber, serializedRequest, message, Constants.CancelCargo);
        }
    }
}
