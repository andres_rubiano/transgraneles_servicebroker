﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Transactions;
using Common;
using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using BusinessRules.Main.Resources;
using Infrastructure.Data.Main.EF;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    public class TaskDownloadDOWN022 : ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            try
            {
                List<GetInformationForTransferToBancolombia_Result> informationTransfer;
                var dowId = int.Parse(task.StkReference);
                DOWNLOADS download;

                using (var db = new AsTransEntities())
                {
                    var informationTransferResult = db.GetInformationForTransferToBancolombia(int.Parse(task.StkReference));
                    var demo = informationTransferResult.ToList();
                    informationTransfer = demo;
                    download = db.DOWNLOADS.FirstOrDefault(d => d.DOWID == dowId);
                }

                // Validaciones:

                // Download:
                // Debe existir:
                if (download == null)
                {
                    task.EstablecerEnError(string.Format(Messages.errorDownloadNotFound, task.StkReference));
                    return task;
                }

                // Debe estar "Esperando por el broker":

                if (!download.DOWSTATUS.Equals("W"))
                {
                    task.EstablecerEnError(string.Format(Messages.errorDownloadStatusNotFound, task.StkReference));
                    return task;
                }

                // Si ya hay registros marcados como exportados no se continua:

                string sql = "Select Count(*) "
                             + "From   ATRANSACT "
                             + "       Join dbo.ATRANSCASH On ATHATRID = ATRID "
                             + "Where  Coalesce(ATRTRADOWID, 0) <> 0 "
                             + "       And ATHID In (Select items "
                             + "                     From   dbo.Split((Select DOFVALUE1 "
                             + "                                       From   DOWFILTERS "
                             + "                                       Where  DOFDOWID = " + dowId + "), N','))";

                int exportedRegisters;

                using (var db = new AsTransEntities())
                {
                    exportedRegisters = db.Database.ExecuteSqlCommand(sql);
                }

                if (exportedRegisters > 0)
                    task.EstablecerEnError(string.Format(Messages.errorExportedRegisters));

                // Debe existir informacion para procesar:

                if (!informationTransfer.Any() && !task.EstaEnError())
                    task.EstablecerEnError(string.Format(Messages.errorDownloadInformationNotFound));

                // Si no hay errores se crea el archivo:

                string fileName = AsTransConfig.Constants().Down022_NombreArchivo +
                                  Utility.ObtenerFechaActualSerializadaConMilisegundos() + ".TXT";

                var texto = new StringBuilder();

                foreach (var resultLine in informationTransfer)
                {
                    texto.AppendLine(resultLine.Information);
                }

                using (var db = new AsTransEntities())
                {
                    using (var transaction = new TransactionScope())
                    {
                        // Actualizacion en BD, de acuerdo a la respuesta:
                        // Si hubo errores en el proceso que llegaron hasta aca, se actualiza el download:

                        var downloadUpdate = db.DOWNLOADS.FirstOrDefault(d => d.DOWID == dowId);

                        if (task.EstaEnError())
                        {
                            downloadUpdate.DOWSTATUS = "E";
                            downloadUpdate.DOWSTATUSNAME = "Terminado con Errores";
                            downloadUpdate.DOWMEMO = task.StkMessage.Truncate(500);
                        }
                        else
                        {
                            Utility.CreateFile(texto.ToString(), AsTransConfig.Constants().Down022_UbicacionArchivos + fileName);

                            // Actualizar el download:

                            downloadUpdate.DOWSTATUS = "O";
                            downloadUpdate.DOWSTATUSNAME = "Terminado OK";
                            downloadUpdate.DOWFILE = fileName;
                            //downloadUpdate.DOWFILEZIP = AsTransConfig.Constants().Down022_UbicacionArchivos;
                            downloadUpdate.DOWVIRTUALDIRECTORY = AsTransConfig.Constants().Down022_DirectorioVirtual;

                            // Obtener los ATRID:

                            string sqlAtr = "Select Distinct(ATRID) "
                                            + "From   ATRANSACT "
                                            + "       Join dbo.ATRANSCASH On ATHATRID = ATRID "
                                            + "Where  ATHID In (Select items "
                                            + "                 From   dbo.Split((Select DOFVALUE1 "
                                            + "                                   From   DOWFILTERS "
                                            + "                                   Where  DOFDOWID = " + dowId + "), N','))";
                            var atransactIds = db.Database.SqlQuery<int>(sqlAtr).ToList();

                            // Actualizar ATRANSACT's:

                            var transacciones = atransactIds.Select(atransactId => db.ATRANSACT.FirstOrDefault(a => a.ATRID == atransactId)).ToList();

                            foreach (var transaccion in transacciones)
                            {
                                transaccion.ATRTRADOWID = dowId;
                                transaccion.ATRDATE = DateTime.Today;
                            }

                            // Registrar log por cada ATRANSACT:

                            foreach (var atransactId in atransactIds)
                            {
                                // Log file:

                                var r = new System.Data.Entity.Core.Objects.ObjectParameter("r", typeof(int));
                                db.SEQVAL("LOG_FILE_TRANSFER", r);

                                var log = new LOG_FILE_TRANSFER();

                                log.LTFID = int.Parse(r.Value.ToString());
                                log.LTFATRID = atransactId;
                                log.LTFATRTRADOWID = dowId;
                                log.LTFACTION = 1;
                                log.LTFMEMO = "Batch generado.";
                                log.LTFTYPE = "TRADOW";
                                log.LTFDESC = "Transferencia Bancaria";
                                log.LTFDATE = DateTime.Now;
                                log.LTFQUIC = downloadUpdate.DOWQUIC;

                                db.LOG_FILE_TRANSFER.Add(log);

                            }

                            // Actualizar Tarea:
                            task.EstablecerEnOk("Terminado OK");
                        }

                        db.SaveChanges();
                        transaction.Complete();
                    }
                }

            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);
                //task.StkError = true;
                //task.StkMessage = ex.Message;
                //task.StkProcessDate = DateTime.Now;

                task.EstablecerEnError(ex.Message);

                // En este caso se debe actualziar el download y dejarlo en error:
                using (var db = new AsTransEntities())
                {
                    using (var transaction = new TransactionScope())
                    {
                        var id = int.Parse(task.StkReference);

                        var downloadUpdate = db.DOWNLOADS.FirstOrDefault(d => d.DOWID == id);
                        if (downloadUpdate != null)
                        {
                            downloadUpdate.DOWSTATUS = "E";
                            downloadUpdate.DOWSTATUSNAME = "Terminado con errores.";
                            downloadUpdate.DOWMEMO = ex.Message.Truncate(500);
                        }
                        db.SaveChanges();
                        transaction.Complete();
                    }
                }

            }
            return task;
        }
    }
}
