﻿using System;
using BusinessRules.Main.DTOs;
using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using Common;
using Common.Constants;
using Infrastructure.Interoperability.ServiceTest;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    public class TaskTest : ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            var demo = new ServiceTestClient();
            //var inspector = new InspectorBehavior();
            //demo.Endpoint.Behaviors.Add(inspector);

            try
            {
                int dato = 75;
                var origen = Utility.SerializeObject(dato);
                task.StkRequestID = Utility.RegistrarRequest(origen);

                var aja = demo.GetData(dato);
                aja = Utility.SerializeObject(aja);
                task.StkResponseID = Utility.RegistrarResponse(aja);

                //inspector.request;

                //Utility.RegistrarEventoNegocio("FACT", "Descripcion del evento _200", "ErrorDescription_200: " + aja, "USerCode_20", "_20_" + dato.ToString(CultureInfo.InvariantCulture));
                //task.StkSuccessed = true;
                //task.StkProcessDate = DateTime.Now;
                //task.StkMessage = "Servicio test consultado!!!";
                task.EstablecerEnOk("Servicio test consultado!!!");
            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);
                //task.StkError = true;
                //task.StkMessage = ex.Message;
                //task.StkProcessDate = DateTime.Now;
                task.EstablecerEnError(ex.Message);
            }

            LogService.CreateMessageLog(new MessageDTO() { TypeEnum = MessagesConstants.Types.Information, Message = "Llamada Interfaz" }, UserCredentialsTC.Auto, AstransIntegrationsTypes.Drivers);

            return task;
        }
    }
}
