﻿using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using BusinessRules.Main.Resources;
using Common;
using Infrastructure.Data.Main.EF;
using Infrastructure.Interoperability.LoadShipmentMonitorService;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using static BusinessRules.Main.VoyageMonitorUtils;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    // VOY-101 - Integrar viaje en transito a Monitor
    public class VoyageMonitorVOY101 : ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            try
            {
                string voyageNumber = task.StkReference;

                using (var db = new AsTransEntities())
                {
                    // Obtiene los datos del viaje
                    var voyage = db
                        .GetVoyageToMonitor(task.StkReference)
                        .FirstOrDefault();

                    #region Validaciones
                    if (voyage is null)
                    {
                        task.EstablecerEnError(string.Format(Messages.errVoyageNotFound, task.StkReference));
                        return task;
                    }

                    // En tránsito = 5
                    if (voyage.AsTrans_VoyEcoNiv != 5)
                    {
                        task.EstablecerEnError(string.Format(Messages.errVoyageLevel, task.StkReference));
                        return task;
                    }

                    // PArametro VoyToMonitor permite?
                    if (voyage.AsTrans_Parameter_VoyToMonitor_Value != 1)
                    {
                        task.EstablecerEnError(string.Format(Messages.errParameterVoyToMonitorNotAllowed));
                        return task;
                    }


                    var voyageOrders = db
                        .GetOrdplaFromVoyageToMonitor(task.StkReference)
                        ?.ToList();

                    if (voyageOrders is null)
                    {
                        task.EstablecerEnError(string.Format(Messages.errOrdersNotFound, task.StkReference));
                        return task;
                    }
                    #endregion

                    #region Inicialización de Monitor y Monitor Queue

                    var currentVoyageMonitor = db.VoyageMonitor
                        .FirstOrDefault(x => x.VomVoyageNumber.Equals(voyageNumber));

                    if (currentVoyageMonitor != null)
                    {
                        currentVoyageMonitor.VomIntegrationState = (int)VoyageIntegrationState.EnProceso;
                    }

                    currentVoyageMonitor = AddOrUpdateVoyageMonitor(voyageNumber, db, currentVoyageMonitor);

                    var currentVoyageQueue = db.VoyageMonitorQueue
                        .FirstOrDefault(x => x.VmqVoyageNumber.Equals(voyageNumber));

                    currentVoyageQueue = AddOrUpdateVoyageMonitorQueue(voyageNumber, db, currentVoyageQueue, string.Empty, Constants.InitialIntegration);

                    #endregion

                    // Mapping de las propiedades del viaje
                    var request = SetRequestInfo(voyage, voyageOrders);

                    // Guarda el xml enviado
                    var serializedRequest = Utility.SerializeObject(request);
                    task.StkRequestID = Utility.RegistrarRequest(serializedRequest);

                    // Se instancia el servicio Monitor
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var service = new LoadShipmentServiceClient();

                    var response = service.loadShipment(
                            GetToken(),
                            request.manifest,
                            request.routePlane,
                            request.vehicle,
                            request.driver,
                            request.gpsProvider,
                            request.gpsSecuntaryProviders,
                            request.loadingOrders
                        );

                    if (response is null)
                    {
                        task.EstablecerEnError(Messages.errServiceDidNotRespond);
                        
                        currentVoyageMonitor.VomIntegrationState = (int)VoyageIntegrationState.Error;

                        UpdateVoyageMonitorData(voyageNumber, db, currentVoyageMonitor, currentVoyageQueue, serializedRequest, Messages.errServiceDidNotRespond);

                        return task;
                    }

                    // Guarda el xml de respuesta
                    var serializedResponse = Utility.SerializeObject(response);
                    task.StkResponseID = Utility.RegistrarResponse(serializedResponse);

                    if (response.code != VoyageMonitorConfig.Constants().SuccessCode)
                    {
                        task.EstablecerEnError(string.Format(Messages.errMonitorError, response.code, response.description));

                        currentVoyageMonitor.VomIntegrationState = (int)VoyageIntegrationState.Error;

                        UpdateVoyageMonitorData(voyageNumber, db, currentVoyageMonitor, currentVoyageQueue, serializedRequest, response.description);

                        return task;
                    }

                    currentVoyageMonitor.VomIntegrationDate = System.DateTime.Now;
                    currentVoyageMonitor.VomIntegrationState = (int)VoyageIntegrationState.Integrado;

                    UpdateVoyageMonitorData(voyageNumber, db, currentVoyageMonitor, currentVoyageQueue, serializedRequest, response.description);

                    task.EstablecerEnOk("AsTrans: Tarea finalizada correctamente.");
                }
            }
            catch (System.Exception ex)
            {
                _ = ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out _);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }

            return task;
        }

        private static void UpdateVoyageMonitorData(string voyageNumber, AsTransEntities db, VoyageMonitor currentVoyageMonitor, VoyageMonitorQueue currentVoyageQueue, string serializedRequest, string message)
        {
            AddOrUpdateVoyageMonitor(voyageNumber, db, currentVoyageMonitor);

            AddOrUpdateVoyageMonitorQueue(voyageNumber, db, currentVoyageQueue, message, Constants.InitialIntegration);

            AddVoyageMonitorHistory(db, voyageNumber, serializedRequest, message, Constants.InitialIntegration);
        }

        private loadShipmentRequest SetRequestInfo(GetVoyageToMonitor_Result voyage, List<GetOrdplaFromVoyageToMonitor_Result> voyageOrders)
        {
            var extraPoits = voyage
                .routePlane_extraPoints
                ?.Split(new[] { "," }, System.StringSplitOptions.RemoveEmptyEntries)
                .Select(x => new point { idPoint = x })
                .ToArray() ?? new point[] { };

            return new loadShipmentRequest
            {
                driver = new driver
                {
                    id = voyage.driver_idDriver,
                    name = voyage.driver_name,
                    surname = voyage.driver_surname,
                    mobilNumber = voyage.driver_mobilNumber
                },
                gpsProvider = new gpsProvider
                {
                    id = voyage.gpsProvider_id,
                    idCompany = voyage.gpsProvider_idCompany,
                    user = voyage.gpsProvider_user,
                    password = voyage.gpsProvider_password
                },
                manifest = new manifestInfo
                {
                    idManifest = voyage.manifest_idManifest,
                    createmanifestdate = voyage.manifest_createmanifestdate,
                    departureTime = voyage.manifest_departureTime,
                    operationType = voyage.manifest_operationType,
                    generador = new generator
                    {
                        idGenerator = voyage.generador_idGenerator,
                        nameGenerator = voyage.generador_nameGenerator,
                    },
                    transporter = new transporter
                    {
                        idTransporter = voyage.transporter_idTransporter,
                        nameTransporter = voyage.transporter_nameTransporter
                    },

                },
                routePlane = new routePlane
                {
                    via = voyage.routePlane_via,
                    routeName = voyage.routePlane_routeName,
                    destination = new destination
                    {
                        idDestination = voyage.routePlane_destination_idDestination
                    },
                    origin = new origin
                    {
                        idOrigen = voyage.routePlane_origin_idOrigen
                    },
                    extraPoints = extraPoits
                },
                vehicle = new vehicle
                {
                    licensePlate = voyage.vehicle_licensePlate
                },
                loadingOrders = SetLoadingOrders(voyageOrders)
            };
        }

        private loadOrder[] SetLoadingOrders(List<GetOrdplaFromVoyageToMonitor_Result> voyageOrders)
        {
            return voyageOrders?
                .Select(x => new loadOrder
                {
                    idCargo = x.remittance_IdCargo,
                    appointment = x.remittance_appointment,
                    estimatedDeliveryDate = x.remittance_estimatedDeliveryDate,
                    generador = new generator
                    {
                        idGenerator = x.remittance_generador_idGenerator
                    },
                    receiver = new receiver
                    {
                        idReceiver = x.remittance_receiver_idReceiver,
                        nameReceiver = x.remittance_receiver_nameReceiver
                    },
                    sender = new sender
                    {
                        idSender = x.remittance_sender_idSender,
                        nameSender = x.remittance_sender_nameSender
                    },
                    origin = new origin
                    {
                        idOrigen = x.remittance_origin_idOrigen,
                        originAddress = x.remittance_origin_originAddress,
                        originLatidude = x.remittance_origin_Latitude,
                        originLongitude = x.remittance_origin_Longitude
                    },
                    destination = new destination
                    {
                        idDestination = x.remittance_destination_idDestination,
                        destinationAddress = x.remittance_destination_destinationAddress,
                        destinationLatidude = x.remittance_destination_Latitude,
                        destinationLongitude = x.remittance_destination_Longitude
                    },
                    merchandise = new merchandise
                    {
                        productName = x.remittance_merchandise_productName,
                        category = x.remittance_merchandise_category
                    }
                }).ToArray();
        }

        private authToken GetToken()
        {
            var user = VoyageMonitorConfig.Constants().User;
            var password = VoyageMonitorConfig.Constants().Password;

            return new authToken
            {
                authUser = user,
                authPassword = password
            };
        }
    }
}
