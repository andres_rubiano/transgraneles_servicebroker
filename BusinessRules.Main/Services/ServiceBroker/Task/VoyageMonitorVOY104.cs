﻿using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using BusinessRules.Main.Resources;
using Common;
using Infrastructure.Data.Main.EF;
using Infrastructure.Interoperability.LoadShipmentMonitorService;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using static BusinessRules.Main.VoyageMonitorUtils;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    // VOY-104 - Integrar Actualizacion de viajes a Monitor
    public class VoyageMonitorVOY104 : ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            try
            {
                string voyageNumber = task.StkReference;

                using (var db = new AsTransEntities())
                {
                    // Obtiene los datos del viaje
                    var voyage = db
                        .GetVoyageToMonitor(task.StkReference)
                        .FirstOrDefault();

                    #region Validaciones
                    if (voyage is null)
                    {
                        task.EstablecerEnError(string.Format(Messages.errVoyageNotFound, task.StkReference));
                        return task;
                    }

                    // En tránsito = 5
                    if (voyage.AsTrans_VoyEcoNiv != 5)
                    {
                        task.EstablecerEnError(string.Format(Messages.errVoyageLevel, task.StkReference));
                        return task;
                    }

                    // PArametro VoyToMonitor permite?
                    if (voyage.AsTrans_Parameter_VoyToMonitor_Value != 1)
                    {
                        task.EstablecerEnError(string.Format(Messages.errParameterVoyToMonitorNotAllowed));
                        return task;
                    }

                    var voyageOrders = db
                        .GetOrdplaFromVoyageToMonitor(task.StkReference)
                        ?.ToList();

                    if (voyageOrders is null)
                    {
                        task.EstablecerEnError(string.Format(Messages.errOrdersNotFound, task.StkReference));
                        return task;
                    }
                    #endregion

                    #region Inicialización de Monitor y Monitor Queue

                    var currentVoyageMonitor = db.VoyageMonitor
                        .FirstOrDefault(x => x.VomVoyageNumber.Equals(voyageNumber));

                    // Validar si el viaje esta como integrado a Monitor
                    if (currentVoyageMonitor != null && currentVoyageMonitor.VomIntegrationState != (int)VoyageIntegrationState.Integrado)
                    {
                        task.EstablecerEnError(string.Format(Messages.errVoyageIsNotIntegrated, voyageNumber));
                        return task;
                    }

                    var currentVoyageQueue = db.VoyageMonitorQueue
                        .FirstOrDefault(x => x.VmqVoyageNumber.Equals(voyageNumber));

                    currentVoyageQueue = AddOrUpdateVoyageMonitorQueue(voyageNumber, db, currentVoyageQueue, string.Empty, Constants.UpdateCargo);

                    #endregion

                    // Mapping de las propiedades del viaje
                    var request = SetRequestInfo(voyageOrders);

                    // Guarda el xml enviado
                    var serializedRequest = Utility.SerializeObject(request);
                    task.StkRequestID = Utility.RegistrarRequest(serializedRequest);

                    // Se instancia el servicio Monitor
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var service = new LoadShipmentServiceClient();

                    var response = service.upsertCargo(
                            GetToken(),
                            request.loadingOrders,
                            voyageNumber
                        );

                    if (response is null)
                    {
                        task.EstablecerEnError(Messages.errServiceDidNotRespond);

                        UpdateVoyageMonitorData(voyageNumber, db, currentVoyageQueue, serializedRequest, Messages.errServiceDidNotRespond);

                        return task;
                    }

                    // Guarda el xml de respuesta
                    var serializedResponse = Utility.SerializeObject(response);
                    task.StkResponseID = Utility.RegistrarResponse(serializedResponse);

                    if (response.code != VoyageMonitorConfig.Constants().SuccessCode)
                    {
                        task.EstablecerEnError(string.Format(Messages.errMonitorError, response.code, response.description));

                        UpdateVoyageMonitorData(voyageNumber, db, currentVoyageQueue, serializedRequest, response.description);

                        return task;
                    }

                    UpdateVoyageMonitorData(voyageNumber, db, currentVoyageQueue, serializedRequest, response.description);

                    task.EstablecerEnOk("AsTrans: Tarea finalizada correctamente.");
                }
            }
            catch (System.Exception ex)
            {
                _ = ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out _);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }

            return task;
        }

        private static void UpdateVoyageMonitorData(string voyageNumber, AsTransEntities db, VoyageMonitorQueue currentVoyageQueue, string serializedRequest, string message)
        {
            AddOrUpdateVoyageMonitorQueue(voyageNumber, db, currentVoyageQueue, message, Constants.UpdateCargo);

            AddVoyageMonitorHistory(db, voyageNumber, serializedRequest, message, Constants.UpdateCargo);
        }

        private upsertCargoRequest SetRequestInfo(List<GetOrdplaFromVoyageToMonitor_Result> voyageOrders)
        {
            return new upsertCargoRequest
            {
                loadingOrders = voyageOrders?
                .Select(x => new loadOrder
                {
                    idCargo = x.remittance_IdCargo,
                    appointment = x.remittance_appointment,
                    estimatedDeliveryDate = x.remittance_estimatedDeliveryDate,
                    generador = new generator
                    {
                        idGenerator = x.remittance_generador_idGenerator
                    },
                    receiver = new receiver
                    {
                        idReceiver = x.remittance_receiver_idReceiver,
                        nameReceiver = x.remittance_receiver_nameReceiver
                    },
                    sender = new sender
                    {
                        idSender = x.remittance_sender_idSender,
                        nameSender = x.remittance_sender_nameSender
                    },
                    origin = new origin
                    {
                        idOrigen = x.remittance_origin_idOrigen,
                        originAddress = x.remittance_origin_originAddress,
                        originLatidude = x.remittance_origin_Latitude,
                        originLongitude = x.remittance_origin_Longitude
                    },
                    destination = new destination
                    {
                        idDestination = x.remittance_destination_idDestination,
                        destinationAddress = x.remittance_destination_destinationAddress,
                        destinationLatidude = x.remittance_destination_Latitude,
                        destinationLongitude = x.remittance_destination_Longitude
                    },
                    merchandise = new merchandise
                    {
                        productName = x.remittance_merchandise_productName,
                        category = x.remittance_merchandise_category
                    }
                }).ToArray()
            };
        }

        private authToken GetToken()
        {
            var user = VoyageMonitorConfig.Constants().User;
            var password = VoyageMonitorConfig.Constants().Password;

            return new authToken
            {
                authUser = user,
                authPassword = password
            };
        }
    }
}
