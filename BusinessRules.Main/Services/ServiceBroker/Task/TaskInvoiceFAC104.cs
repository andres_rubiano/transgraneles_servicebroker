﻿using System;
using System.Collections.Generic;
//using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Transactions;
using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Factory;
using BusinessRules.Main.Interfaces.ServiceBroker;
using BusinessRules.Main.Resources;
using Common;
using Common.Constants;
using Infrastructure.Data.Main.EF;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Infrastructure.Interoperability.CenFinancieroService;
using System.Data.Entity.Core.Objects;
using Newtonsoft.Json;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    public class TaskInvoiceFAC104 : CarvajalBaseTask, ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            Constantes = CarvajalInvoice.Constants();

            try
            {
                var invoiceHeader = new List<GetInvoiceHeaderToCarvajal_Result>();
                var invoiceDetail = new List<GetInvoiceDetailToCarvajal_Result>();
                var infoReversionNote = new GetInvoiceNumberFromAnulationNote_Result();

                ObjectResult<GetInvoiceHeaderToCarvajal_Result> invoiceHeaderResult;
                ObjectResult<GetInvoiceDetailToCarvajal_Result> invoiceDetailResult;
                ObjectResult<GetInvoiceNumberFromAnulationNote_Result> infoReversionNoteResult;

                using (var db = new AsTransEntities())
                {
                    infoReversionNoteResult = db.GetInvoiceNumberFromAnulationNote(task.StkReference);
                    infoReversionNote = infoReversionNoteResult.ToList().FirstOrDefault();

                    if (infoReversionNote == null)
                    {
                        //task.StkError = true;
                        //task.StkProcessDate = DateTime.Now;
                        //task.StkMessage = string.Format(Messages.errorInvoiceNotFound, task.StkReference);

                        task.EstablecerEnError(string.Format(Messages.errorInvoiceNotFound, task.StkReference));
                        return task;
                    }

                    invoiceHeaderResult = db.GetInvoiceHeaderToCarvajal(infoReversionNote.Factura);
                    invoiceDetailResult = db.GetInvoiceDetailToCarvajal(infoReversionNote.Factura);
                    invoiceHeader = invoiceHeaderResult.ToList();
                    invoiceDetail = invoiceDetailResult.ToList();
                }

                // Validaciones:

                if (!invoiceHeader.Any())
                {
                    task.EstablecerEnError(string.Format(Messages.errorInvoiceNotFound, task.StkReference));
                    return task;
                }

                var reversionNote = invoiceHeader.FirstOrDefault();
                reversionNote.Enc_1_TipoDocumento = InvoiceCarvajal.AnulacionFactura;
                reversionNote.Enc_6_NumeroDocumento = Utility.FormatInvoiceNumberToCarvajal(task.StkReference);
                reversionNote.Enc_7_FechaFactura = infoReversionNote.FechaReversion;
                reversionNote.Enc_8_HoraFactura = infoReversionNote.HoraReversion;
                reversionNote.Enc_9_TipoFactura = "91";
                reversionNote.Ref_2_FacturaReferencia = Utility.FormatInvoiceNumberToCarvajal(infoReversionNote.Factura);
                reversionNote.Ref_3_FacturaReferenciaFecha = infoReversionNote.FechaReversion;
                reversionNote.Nrf_1 = Utility.FormatInvoiceNumberToCarvajal(infoReversionNote.Factura);
                reversionNote.Drf_4_Prefijo = task.StkReference.Split('-')[0];
                reversionNote.Nota_3_Pos_1 = infoReversionNote.Observaciones;
                //reversionNote.Nota_11_Pos_5 = reversionNote.Tot_5_TotalFactura;

                // Mapeo de objetos:
                var fechaVersion21 =
                    Utility.CastStringToValidDatetimeFormats(CarvajalInvoice.Constants().FechaInicioVersion21);

                if (Utility.CastStringToValidDatetimeFormats(infoReversionNote.FechaFactura) < fechaVersion21)
                    reversionNote.Enc_21_TipoOperacion = CarvajalInvoice.Constants().Enc_21_NotaCredito20;
                else
                    reversionNote.Enc_21_TipoOperacion = CarvajalInvoice.Constants().Enc_21_NotaCredito21;

                var anulationNote = InvoiceFactory.GetInvoiceForFile(reversionNote, invoiceDetail, true);

                // se hace el proceso generando archivo, registransdo en carvaajal y en BD:

                // Creacion de archivo
                string localPath;
                string urlFile;
                string fileName;
                Utility.CreateFile(anulationNote.ToString(), CarvajalInvoice.Constants().UbicacionArchivos, task.StkReference,
                    CarvajalInvoice.Constants().ExtensionArchivos, out localPath, out urlFile, out fileName);
                urlFile = CarvajalInvoice.Constants().UrlArchivos + urlFile;

                string transactionId;

                using (var db = new AsTransEntities())
                {
                    // Reportar a Carvajal
                    // autenticar:
                    var cenFinancieroClient = new invoiceServiceClient();

                    cenFinancieroClient.ChannelFactory.Endpoint.Behaviors.Remove<System.ServiceModel.Description.ClientCredentials>();
                    cenFinancieroClient.ChannelFactory.Endpoint.Behaviors.Add(new CustomCredentials());
                    cenFinancieroClient.ClientCredentials.UserName.UserName = CarvajalInvoice.Constants().UsernameService;
                    cenFinancieroClient.ClientCredentials.UserName.Password = Utility.EncryptSha256(CarvajalInvoice.Constants().PasswordService);

                    // Se debe consignar el contenido del archivo, codificado en base 64. 
                    byte[] fileContents;
                    fileContents = Encoding.UTF8.GetBytes(anulationNote.ToString());
                    var fileEncoded = Convert.ToBase64String(fileContents);

                    // enviar informacion:
                    var uploadRequest = new UploadRequest();
                    uploadRequest.fileName = fileName;
                    uploadRequest.fileData = fileEncoded;
                    uploadRequest.companyId = CarvajalInvoice.Constants().CompanyId;
                    uploadRequest.accountId = CarvajalInvoice.Constants().AccountId;

                    // request:
                    var origen = Utility.SerializeObject(uploadRequest);
                    task.StkRequestID = Utility.RegistrarRequest(origen);

                    UploadResponse response = cenFinancieroClient.Upload(uploadRequest);

                    // response:

                    var respuesta = Utility.SerializeObject(response);
                    task.StkResponseID = Utility.RegistrarResponse(respuesta);

                    //Si sucede un error es capturado por la excepción de aqui en adelante se asume correcta respuesta del servcio.

                    using (var transaction = new TransactionScope())
                    {
                        // Actualizacion en BD, de acuerdo a la respuesta:
                        var electronicInvoice = db.ElectronicInvoicing.FirstOrDefault(ei => ei.EinIvoNumber.Equals(task.StkReference));
                        var eInvoiceQueue = db.ElectronicInvoiceQueue.FirstOrDefault(e => e.EiqIvoNumber.Equals(task.StkReference));

                        if (electronicInvoice != null)
                        {
                            electronicInvoice.EinFileLocalPath = localPath;
                            electronicInvoice.EinFileUrl = urlFile;
                            electronicInvoice.EinState = (int)State.Integration.Integrado;
                            electronicInvoice.EinExternalId = response.transactionId;
                        }
                        else
                        {
                            electronicInvoice = new ElectronicInvoicing();
                            electronicInvoice.EinIvoNumber = task.StkReference;
                            electronicInvoice.EinFileLocalPath = localPath;
                            electronicInvoice.EinFileUrl = urlFile;
                            electronicInvoice.EinState = (int)State.Integration.Integrado;
                            electronicInvoice.EinExternalId = response.transactionId;
                            db.ElectronicInvoicing.Add(electronicInvoice);
                        }

                        transactionId = response.transactionId;

                        if (eInvoiceQueue == null)
                        {
                            ElectronicInvoiceQueue eInvoiceQueueNew = new ElectronicInvoiceQueue();

                            eInvoiceQueueNew.EiqIvoNumber = task.StkReference;
                            eInvoiceQueueNew.EiqCreationDate = DateTime.Now;
                            db.ElectronicInvoiceQueue.Add(eInvoiceQueueNew);
                        }

                        db.SaveChanges();


                        task.EstablecerEnOk(string.Empty);
                        transaction.Complete();
                    }
                }
                // Crear tarea FAC105: ConsultarEstadoDocumento:
                if (!string.IsNullOrEmpty(transactionId))
                {
                    string tipo = reversionNote.Enc_1_TipoDocumento;

                    if (reversionNote.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
                        tipo = "FV";

                    string objectJson = JsonConvert.SerializeObject(new { NumeroDocumento = task.StkReference, TransactionId = transactionId, TipoDocumentoFe = tipo });
                    Utility.CrearTarea("FAC105", task.StkReference, objectJson, DateTime.Now.AddMinutes(Constantes.Fac105MinutosReprogramar), task.StkCreation_UsrCode);
                }
            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }
            return task;
        }
    }
}
