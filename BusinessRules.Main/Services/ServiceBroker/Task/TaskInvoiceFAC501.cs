﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Xml;
using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using Common;
using Common.Constants;
using Infrastructure.Data.Main.EF;
using Infrastructure.Interoperability.CenFinancieroService;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    public class TaskInvoiceFAC501 : ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            try
            {
                using (var db = new AsTransEntities())
                {
                    // Validar el documento
                    var documento = db.INVOICE.FirstOrDefault(d => d.IVONUM.Equals(task.StkReference));

                    if (documento == null)
                    {
                        task.EstablecerEnError(string.Format("El documento {0} no existe.", task.StkReference));
                        return task;
                    }

                    if (documento.IVOEFENIV != 2 && documento.IVOEFENIV > 0)
                    {
                        task.EstablecerEnError(
                            string.Format("El documento {0}, debe tener estado de facturacion: Definitivo, Anulado o Reversado.",
                                task.StkReference));
                        return task;
                    }

                    if (!string.IsNullOrEmpty(documento.IVOFEDOCNUMBER))
                    {
                        task.EstablecerEnError(string.Format("El documento {0} ya tiene numero de documento de FE.",
                            task.StkReference));
                        return task;
                    }

                    // autenticar:
                    var cenFinancieroClient = new invoiceServiceClient();

                    cenFinancieroClient.ChannelFactory.Endpoint.Behaviors.Remove<System.ServiceModel.Description.ClientCredentials>();
                    cenFinancieroClient.ChannelFactory.Endpoint.Behaviors.Add(new CustomCredentials());
                    cenFinancieroClient.ClientCredentials.UserName.UserName = CarvajalInvoice.Constants().UsernameService;
                    cenFinancieroClient.ClientCredentials.UserName.Password =
                        Utility.EncryptSha256(CarvajalInvoice.Constants().PasswordService);

                    // enviar informacion:

                    var downLoadRequest = new DownloadRequest();
                    downLoadRequest.companyId = CarvajalInvoice.Constants().CompanyId;
                    downLoadRequest.accountId = CarvajalInvoice.Constants().AccountId;

                    var numeroDocumento = documento.IVONUM.Split('-');

                    int numeroInvoice = int.Parse(numeroDocumento[1]);
                    downLoadRequest.documentType = InvoiceCarvajal.HomologationTypeInvoice[documento.IVODTYCODE];
                    downLoadRequest.documentNumber = numeroDocumento[0] +
                                                     numeroInvoice.ToString(CultureInfo.InvariantCulture);
                    downLoadRequest.documentPrefix = numeroDocumento[0];
                    downLoadRequest.resourceType = "SIGNED_XML";

                    // request:
                    var origen = Utility.SerializeObject(downLoadRequest);
                    task.StkRequestID = Utility.RegistrarRequest(origen);

                    DownloadResponse response = cenFinancieroClient.Download(downLoadRequest);

                    // response:

                    var respuesta = Utility.SerializeObject(response);
                    task.StkResponseID = Utility.RegistrarResponse(respuesta);

                    //Si sucede un error es capturado por la excepción de aqui en adelante se asume correcta respuesta del servcio.

                    var encodedString = response.downloadData;

                    byte[] data = Convert.FromBase64String(encodedString);
                    string decodedString = Encoding.UTF8.GetString(data);

                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.DtdProcessing = DtdProcessing.Parse;
                    XmlReader reader = XmlReader.Create(new StringReader(decodedString), settings);

                    string numeroDian = string.Empty;

                    if (reader.ReadToDescendant("cbc:UUID"))
                    {
                        reader.Read(); //this moves reader to next node which is text 
                        numeroDian = reader.Value; //this might give value than 
                    }

                    string sql = "Update INVOICE "
                                 + "Set IVOFEDOCNUMBER = '" + numeroDian + "' "
                                 + "Where IVONUM = '" + task.StkReference + "' ";

                    using (var transaction = new TransactionScope())
                    {
                        // Actualizacion en BD, de acuerdo a la respuesta:
                        db.Database.ExecuteSqlCommand(sql);

                        task.EstablecerEnOk(string.Empty);

                        transaction.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }

            return task;
        }
    }
}
