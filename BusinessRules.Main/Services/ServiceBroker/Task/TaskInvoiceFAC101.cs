﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Transactions;
using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Factory;
using BusinessRules.Main.Interfaces.ServiceBroker;
using BusinessRules.Main.Resources;
using Common;
using Common.Constants;
using Infrastructure.Data.Main.EF;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Infrastructure.Interoperability.CenFinancieroService;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Data.Entity.Core.Objects;
using Newtonsoft.Json;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    public class TaskInvoiceFAC101 : CarvajalBaseTask, ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            Constantes = CarvajalInvoice.Constants();

            try
            {
                var invoiceHeader = new List<GetInvoiceHeaderToCarvajal_Result>();
                var invoiceDetail = new List<GetInvoiceDetailToCarvajal_Result>();

                ObjectResult<GetInvoiceHeaderToCarvajal_Result> invoiceHeaderResult;
                ObjectResult<GetInvoiceDetailToCarvajal_Result> invoiceDetailResult;

                using (var db = new AsTransEntities())
                {
                    invoiceHeaderResult = db.GetInvoiceHeaderToCarvajal(invoiceNumber: task.StkReference);
                    invoiceDetailResult = db.GetInvoiceDetailToCarvajal(task.StkReference);
                    invoiceHeader = invoiceHeaderResult.ToList();
                    invoiceDetail = invoiceDetailResult.ToList();
                }

                // Validaciones:

                if (!invoiceHeader.Any())
                {
                    task.EstablecerEnError(string.Format(Messages.errorInvoiceNotFound, task.StkReference));
                    return task;
                }

                // Mapeo de objetos:

                //2019Nov21 Andres Rubiano Moreno:

                var ivoHeader = invoiceHeader.FirstOrDefault();
                var constantes = CarvajalInvoice.Constants();

                var esVersion21 = Utility.CastStringToValidDatetimeFormats(ivoHeader.Ref_3_FacturaReferenciaFecha) >
                                  Utility.CastStringToValidDatetimeFormats(constantes.FechaInicioVersion21);
                var encabezado21 = string.Empty;

                if (ivoHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
                    encabezado21 = constantes.Enc_21_Factura;
                else
                {
                    if (ivoHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.NotaCredito))
                        encabezado21 = esVersion21 ? constantes.Enc_21_NotaCredito21 : constantes.Enc_21_NotaCredito20;
                    else
                    {
                        if (ivoHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.NotaDebito))
                            encabezado21 = esVersion21 ? constantes.Enc_21_NotaDebito21 : constantes.Enc_21_NotaDebito20;
                    }
                }

                ivoHeader.Enc_21_TipoOperacion = encabezado21;
                var invoice = InvoiceFactory.GetInvoiceForFile(ivoHeader, invoiceDetail, false);

                // se hace el proceso generando archivo, registransdo en carvaajal y en BD:

                // Creacion de archivo
                string localPath;
                string urlFile;
                string fileName;
                Utility.CreateFile(invoice.ToString(), CarvajalInvoice.Constants().UbicacionArchivos, task.StkReference,
                    CarvajalInvoice.Constants().ExtensionArchivos, out localPath, out urlFile, out fileName);
                urlFile = CarvajalInvoice.Constants().UrlArchivos + urlFile;

                string transactionId;

                using (var db = new AsTransEntities())
                {
                    // Reportar a Carvajal
                    // autenticar:
                    var cenFinancieroClient = new invoiceServiceClient();

                    cenFinancieroClient.ChannelFactory.Endpoint.Behaviors.Remove<ClientCredentials>();
                    cenFinancieroClient.ChannelFactory.Endpoint.Behaviors.Add(new CustomCredentials());
                    cenFinancieroClient.ClientCredentials.UserName.UserName = CarvajalInvoice.Constants().UsernameService;
                    cenFinancieroClient.ClientCredentials.UserName.Password = Utility.EncryptSha256(CarvajalInvoice.Constants().PasswordService);

                    // Se debe consignar el contenido del archivo, codificado en base 64. 
                    byte[] fileContents;
                    fileContents = Encoding.UTF8.GetBytes(invoice.ToString());
                    var fileEncoded = Convert.ToBase64String(fileContents);

                    // enviar informacion:
                    var uploadRequest = new UploadRequest();
                    uploadRequest.fileName = fileName;
                    uploadRequest.fileData = fileEncoded;
                    uploadRequest.companyId = CarvajalInvoice.Constants().CompanyId;
                    uploadRequest.accountId = CarvajalInvoice.Constants().AccountId;

                    // request:
                    var origen = Utility.SerializeObject(uploadRequest);
                    task.StkRequestID = Utility.RegistrarRequest(origen);

                    UploadResponse response = cenFinancieroClient.Upload(uploadRequest);

                    // response:

                    var respuesta = Utility.SerializeObject(response);
                    task.StkResponseID = Utility.RegistrarResponse(respuesta);

                    //Si sucede un error es capturado por la excepción de aqui en adelante se asume correcta respuesta del servcio.

                    using (var transaction = new TransactionScope())
                    {
                        // Actualizacion en BD, de acuerdo a la respuesta:
                        var electronicInvoice = db.ElectronicInvoicing.FirstOrDefault(ei => ei.EinIvoNumber.Equals(task.StkReference));
                        var eInvoiceQueue = db.ElectronicInvoiceQueue.FirstOrDefault(e => e.EiqIvoNumber.Equals(task.StkReference));

                        if (electronicInvoice != null)
                        {
                            electronicInvoice.EinFileLocalPath = localPath;
                            electronicInvoice.EinFileUrl = urlFile;
                            electronicInvoice.EinState = (int)State.Integration.Integrado;
                            electronicInvoice.EinExternalId = response.transactionId;
                        }
                        else
                        {
                            electronicInvoice = new ElectronicInvoicing();
                            electronicInvoice.EinIvoNumber = task.StkReference;
                            electronicInvoice.EinFileLocalPath = localPath;
                            electronicInvoice.EinFileUrl = urlFile;
                            electronicInvoice.EinState = (int)State.Integration.Integrado;
                            electronicInvoice.EinExternalId = response.transactionId;
                            db.ElectronicInvoicing.Add(electronicInvoice);
                        }

                        transactionId = response.transactionId;

                        if (eInvoiceQueue == null)
                        {
                            ElectronicInvoiceQueue eInvoiceQueueNew = new ElectronicInvoiceQueue();

                            eInvoiceQueueNew.EiqIvoNumber = task.StkReference;
                            eInvoiceQueueNew.EiqCreationDate = DateTime.Now;
                            db.ElectronicInvoiceQueue.Add(eInvoiceQueueNew);
                        }

                        db.SaveChanges();

                        task.EstablecerEnOk("AsTrans: Tarea finalizada correctamente.");

                        transaction.Complete();
                    }
                }
                // Crear tarea FAC105: ConsultarEstadoDocumento:
                if (!string.IsNullOrEmpty(transactionId))
                {
                    Random rnd = new Random();
                    int secondsAdd = rnd.Next(60, 400);

                    string tipo = ivoHeader.Enc_1_TipoDocumento;

                    if (ivoHeader.Enc_1_TipoDocumento.Equals(InvoiceCarvajal.Factura))
                        tipo = "FV";

                    string objectJson = JsonConvert.SerializeObject(new { NumeroDocumento = task.StkReference, TransactionId = transactionId, TipoDocumentoFe = tipo });
                    Utility.CrearTarea("FAC105", task.StkReference, objectJson, DateTime.Now.AddMinutes(Constantes.Fac105MinutosReprogramar).AddSeconds(secondsAdd), task.StkCreation_UsrCode);
                }
            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }
            return task;
        }
    }
}
