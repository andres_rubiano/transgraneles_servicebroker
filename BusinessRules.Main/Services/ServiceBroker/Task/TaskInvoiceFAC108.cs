﻿using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using Common;
using Common.Constants;
using Infrastructure.Data.Main.EF;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    /// <summary>
    /// Consultar Aceptación Tácita
    /// </summary>
    public class TaskInvoiceFAC108 : CarvajalBaseTask, ITaskProcessor
    {
        private string TransactionId { get; set; }
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            Constantes = CarvajalInvoice.Constants();
            try
            {
                dynamic objeto = JsonConvert.DeserializeObject(task.StkObjectJson);
                TransactionId = objeto.TransactionId;

                #region Consumir servicio "EventStatus"

                var requestId = 0;
                var response = MetodoEventStatus(TransactionId, out requestId);
                task.StkRequestID = requestId;

                var respuesta = Utility.SerializeObject(response);
                task.StkResponseID = Utility.RegistrarResponse(respuesta);

                #endregion

                using (var db = new AsTransEntities())
                {
                    var tacitaQueue = db.ElectronicInvoiceTaticlyQueue.FirstOrDefault(t => t.EtqIvoNumber.Equals(task.StkReference));
                    var eInvoice = db.ElectronicInvoicing.FirstOrDefault(e => e.EinIvoNumber.Equals(task.StkReference));
                    if (tacitaQueue == null)
                    {
                        tacitaQueue = new ElectronicInvoiceTaticlyQueue
                        {
                            EtqIvoNumber = task.StkReference,
                            EtqExternalId = TransactionId,
                            EtqCreationDate = DateTime.Now                            
                        };
                        db.ElectronicInvoiceTaticlyQueue.Add(tacitaQueue);
                    }

                    switch (response.eventStatus)
                    { 
                        case "DOCUMENT_ACCEPTED":
                            tacitaQueue.EtqTacitlyStatus = InvoiceCarvajal.DictionaryEventStatusAsTrans[response.eventStatus];
                            if (eInvoice != null)
                            {
                                eInvoice.EinTacitlyLegalStatus = InvoiceCarvajal.DictionaryEventStatusAsTrans[response.eventStatus];
                                eInvoice.EinTacitlyLegalStatusDate = DateTime.Now;
                            }

                            using (var transaction = new TransactionScope())
                            {
                                db.ElectronicInvoiceTaticlyQueue.Remove(tacitaQueue);

                                db.SaveChanges();
                                task.EstablecerEnOk("AsTrans: Tarea finalizada correctamente.");
                                transaction.Complete();
                            }
                        break;

                        case "FAIL":
                        case "DOCUMENT_REJECTED":
                        case "AVAIBILITY_EVENT_DETECTED":
                        case "RESPONSE_INTEGRITY_EVENT_DETECTED":
                        case "PROCESSING":

                            Utility.CrearTarea("FAC108", task.StkReference, task.StkObjectJson, DateTime.Now.AddMinutes(Constantes.Fac108MinutosReprogramar), task.StkCreation_UsrCode);
                            tacitaQueue.EtqTacitlyStatus = InvoiceCarvajal.DictionaryEventStatusAsTrans[response.eventStatus];
                            
                            if (eInvoice != null)
                            {
                                eInvoice.EinTacitlyLegalStatus = InvoiceCarvajal.DictionaryEventStatusAsTrans[response.eventStatus];
                                eInvoice.EinTacitlyLegalStatusDate = DateTime.Now;
                            }

                            using (var transaction = new TransactionScope())
                            {
                                db.SaveChanges();
                                var mensajeF = string.Format("AsTrans: Estado [{0}], respuesta: {1}.", response.eventStatus, response.governmentResponseDescriptions);
                                task.EstablecerEnOk(mensajeF);
                                transaction.Complete();
                            }
                        break;

                        default:
                            using (var transaction = new TransactionScope())
                            {
                                db.SaveChanges();
                                var mensaje = string.Format("AsTrans: Estado [{0}] no reconocido, respuesta: {1}", response.eventStatus, response.governmentResponseDescriptions);
                                task.EstablecerEnOk(mensaje);
                                transaction.Complete();
                            }
                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }
            return task;




        }
    }
}
