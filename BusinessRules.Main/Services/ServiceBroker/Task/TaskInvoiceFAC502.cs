﻿using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using Common;
using Common.Constants;
using Infrastructure.Data.Main.EF;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    public class TaskInvoiceFAC502 : CarvajalBaseTask, ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            Constantes = CarvajalInvoice.Constants();
            try
            {
                using (var db = new AsTransEntities())
                {
                    // registros a reportar:
                    var documentos = db.Database.SqlQuery<string>(AsTransQuery.ObtenerDocumentosSinCufeCude).ToList();
                    var minutes = 0.0;
                    foreach (var item in documentos)
                    {
                        minutes = minutes + 0.5;
                        Utility.CrearTarea("FAC501", item, null, DateTime.Now.AddMinutes(minutes), "Auto");
                    }
                }

                Utility.CrearTarea("FAC502", string.Empty, null, DateTime.Now.AddMinutes(Constantes.Fac502MinutosReprogramar), "Auto");
                task.EstablecerEnOk("AsTrans: Tarea reprogramada.");
            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }
            return task;
        }
    }
}
