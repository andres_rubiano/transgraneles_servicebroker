﻿using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using BusinessRules.Main.Resources;
using Common;
using Infrastructure.Data.Main.EF;
using Infrastructure.Interoperability.LoadShipmentMonitorService;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Linq;
using System.Net;
using static BusinessRules.Main.VoyageMonitorUtils;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    // VOY-102 - Integrar cancelacion de viaje a Monitor
    class VoyageMonitorVOY102 : ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            try
            {
                string voyageNumber = task.StkReference;

                using (var db = new AsTransEntities())
                {
                   
                    // Obtiene los datos del viaje
                    var voyage = db
                        .GetVoyageToMonitor(task.StkReference)
                        .FirstOrDefault();

                    if(voyage is null)
                    {
                        task.EstablecerEnError(string.Format(Messages.errVoyageNotFound, task.StkReference));
                        return task;
                    }

                    var currentVoyageMonitor = db.VoyageMonitor
                        .FirstOrDefault(x => x.VomVoyageNumber.Equals(voyageNumber));

                    // Validar si el viaje esta como integrado a Monitor
                    if(currentVoyageMonitor != null && currentVoyageMonitor.VomIntegrationState != (int)VoyageIntegrationState.Integrado)
                    {
                        task.EstablecerEnError(string.Format(Messages.errVoyageIsNotIntegrated, voyageNumber));
                        return task;
                    }

                    currentVoyageMonitor.VomCancellationState = (int)VoyageIntegrationState.EnProceso;
            
                    currentVoyageMonitor = AddOrUpdateVoyageMonitor(voyageNumber, db, currentVoyageMonitor);

                    var currentVoyageQueue = db.VoyageMonitorQueue
                        .FirstOrDefault(x => x.VmqVoyageNumber.Equals(voyageNumber));

                    currentVoyageQueue = AddOrUpdateVoyageMonitorQueue(voyageNumber, db, currentVoyageQueue, string.Empty, Constants.CancelIntegration);

                    // Se instancia el servicio Monitor
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    var service = new LoadShipmentServiceClient();

                    var request = new cancelShipments { idManifest = voyageNumber };

                    // Guarda el xml enviado
                    var serializedRequest = Utility.SerializeObject(request);
                    task.StkRequestID = Utility.RegistrarRequest(serializedRequest);

                    var response = service.cancelShipment(GetToken(), request);

                    if (response is null)
                    {
                        task.EstablecerEnError(Messages.errServiceDidNotRespond);

                        currentVoyageMonitor.VomCancellationState = (int)VoyageIntegrationState.Error;

                        UpdateVoyageMonitorData(voyageNumber, db, currentVoyageMonitor, currentVoyageQueue, serializedRequest, Messages.errServiceDidNotRespond);

                        return task;
                    }

                    // Guarda el xml de respuesta
                    var serializedResponse = Utility.SerializeObject(response);
                    task.StkResponseID = Utility.RegistrarResponse(serializedResponse);

                    if (response.code != VoyageMonitorConfig.Constants().SuccessCode)
                    {
                        task.EstablecerEnError(string.Format(Messages.errMonitorError, response.code, response.description));

                        currentVoyageMonitor.VomCancellationState = (int)VoyageIntegrationState.Error;

                        UpdateVoyageMonitorData(voyageNumber, db, currentVoyageMonitor, currentVoyageQueue, serializedRequest, response.description);

                        return task;
                    }

                    currentVoyageMonitor.VomCancellationDate = DateTime.Now;
                    currentVoyageMonitor.VomCancellationState = (int)VoyageIntegrationState.Integrado;

                    UpdateVoyageMonitorData(voyageNumber, db, currentVoyageMonitor, currentVoyageQueue, serializedRequest, response.description);

                    task.EstablecerEnOk("AsTrans: Tarea finalizada correctamente.");
                }
            }
            catch (System.Exception ex)
            {
                _ = ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out _);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }

            return task;
        }

        private static void UpdateVoyageMonitorData(string voyageNumber, AsTransEntities db, VoyageMonitor currentVoyageMonitor, VoyageMonitorQueue currentVoyageQueue, string serializedRequest, string message)
        {
            AddOrUpdateVoyageMonitor(voyageNumber, db, currentVoyageMonitor);

            AddOrUpdateVoyageMonitorQueue(voyageNumber, db, currentVoyageQueue, message, Constants.CancelIntegration);

            AddVoyageMonitorHistory(db, voyageNumber, serializedRequest, message, Constants.CancelIntegration);
        }

        private authToken GetToken()
        {
            var user = VoyageMonitorConfig.Constants().User;
            var password = VoyageMonitorConfig.Constants().Password;

            return new authToken
            {
                authUser = user,
                authPassword = password
            };
        }
    }
}
