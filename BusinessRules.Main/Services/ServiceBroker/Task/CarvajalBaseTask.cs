﻿using Common;
using Common.Constants;
using Infrastructure.Data.Main.EF;
using Infrastructure.Interoperability.CenFinancieroService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
 
namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    public class CarvajalBaseTask
    {
        protected CarvajalInvoice Constantes { get; set; }

        public invoiceServiceClient CenFinancieroClient { get; set; }

        public CarvajalBaseTask()
        {
            CenFinancieroClient = new invoiceServiceClient();
        }

        public UploadResponse MetodoUpload(string fileEncoded, string fileName, string accountId, out int requestId)
        {
            Autenticar();

            var uploadRequest = new UploadRequest();
            uploadRequest.fileName = fileName;
            uploadRequest.fileData = fileEncoded;
            uploadRequest.companyId = CarvajalInvoice.Constants().CompanyId;
            uploadRequest.accountId = accountId;

            // request:
            var origen = Utility.SerializeObject(uploadRequest);
            requestId = Utility.RegistrarRequest(origen);

            UploadResponse response = CenFinancieroClient.Upload(uploadRequest);

            return response;
        }

        public DownloadResponse MetodoDownload(string documentoNumero, string documentoTipoFe, string resourceType, out int requestId)
        {
            Autenticar();

            var downLoadRequest = new DownloadRequest();
            downLoadRequest.companyId = CarvajalInvoice.Constants().CompanyId;
            downLoadRequest.accountId = CarvajalInvoice.Constants().AccountId;

            var numeroDocumento = documentoNumero.Split('-');

            int numeroInvoice = int.Parse(numeroDocumento[1]);
            downLoadRequest.documentType = documentoTipoFe;
            downLoadRequest.documentNumber = numeroDocumento[0] +
                                             numeroInvoice.ToString(CultureInfo.InvariantCulture);
            downLoadRequest.documentPrefix = numeroDocumento[0];
            downLoadRequest.resourceType = resourceType;

            // request:
            var origen = Utility.SerializeObject(downLoadRequest);
            requestId = Utility.RegistrarRequest(origen);

            DownloadResponse response = CenFinancieroClient.Download(downLoadRequest);

            return response;
        }

        public DocumentStatusResponse MetodoDocumentStatus(string transactionId, out int requestId)
        {
            Autenticar();

            var documentRequest = new DocumentStatusRequest();

            documentRequest.companyId = CarvajalInvoice.Constants().CompanyId;
            documentRequest.accountId = CarvajalInvoice.Constants().AccountId;
            documentRequest.transactionId = transactionId;

            // request:
            var origen = Utility.SerializeObject(documentRequest);
            requestId = Utility.RegistrarRequest(origen);

            DocumentStatusResponse response = CenFinancieroClient.DocumentStatus(documentRequest);

            return response;

        }

        public EventStatusResponse MetodoEventStatus(string transactionId, out int requestId)
        {
            Autenticar();

            var documentRequest = new EventStatusRequest
            {
                companyId = CarvajalInvoice.Constants().CompanyId,
                accountId = CarvajalInvoice.Constants().AccountIdAceptacionTacita,
                transactionId = transactionId
            };

            // request:
            var origen = Utility.SerializeObject(documentRequest);
            requestId = Utility.RegistrarRequest(origen);

            EventStatusResponse response = CenFinancieroClient.EventStatus(documentRequest);

            return response;

        }

        private void Autenticar()
        {
            // autenticar:
            CenFinancieroClient.ChannelFactory.Endpoint.Behaviors.Remove<System.ServiceModel.Description.ClientCredentials>();
            CenFinancieroClient.ChannelFactory.Endpoint.Behaviors.Add(new CustomCredentials());

            if (string.IsNullOrEmpty(CenFinancieroClient.ClientCredentials.UserName.UserName))
                CenFinancieroClient.ClientCredentials.UserName.UserName = CarvajalInvoice.Constants().UsernameService;
            if (string.IsNullOrEmpty(CenFinancieroClient.ClientCredentials.UserName.Password))
                CenFinancieroClient.ClientCredentials.UserName.Password = Utility.EncryptSha256(CarvajalInvoice.Constants().PasswordService);
        }

        protected void ActualizaMonitor(ElectronicInvoicing eInvoice, ElectronicInvoiceQueue eInvoiceQueue, DocumentStatusResponse response, AsTransEntities db)
        {
            using (var transaction = new TransactionScope())
            {
                eInvoice.EinLegalStatus = InvoiceCarvajal.DictionaryLegalStatusAsTrans[response.legalStatus];

                if (string.IsNullOrEmpty(response.businessStatus))
                    eInvoice.EinBusinessStatus = null;
                else
                    eInvoice.EinBusinessStatus = InvoiceCarvajal.DictionaryBusinessStatusAsTrans[response.businessStatus];

                if (!string.IsNullOrEmpty(response.businessStatus))
                    eInvoice.EinBusinessStatus = InvoiceCarvajal.DictionaryBusinessStatusAsTrans[response.businessStatus];

                if (eInvoiceQueue == null)
                {
                    ElectronicInvoiceQueue eInvoiceQueueNew = new ElectronicInvoiceQueue();

                    eInvoiceQueueNew.EiqIvoNumber = eInvoice.EinIvoNumber;
                    eInvoiceQueueNew.EiqFEErrorMessage = response.errorMessage;
                    eInvoiceQueueNew.EiqFEProcessDate = response.processDate;
                    eInvoiceQueueNew.EiqFEProcessName = response.processName;
                    eInvoiceQueueNew.EiqFEProcessStatus = response.processStatus;
                    eInvoiceQueueNew.EiqCreationDate = DateTime.Now;
                    db.ElectronicInvoiceQueue.Add(eInvoiceQueueNew);
                }
                else
                {
                    eInvoiceQueue.EiqFEErrorMessage = response.errorMessage;
                    eInvoiceQueue.EiqFEProcessDate = response.processDate;
                    eInvoiceQueue.EiqFEProcessName = response.processName;
                    eInvoiceQueue.EiqFEProcessStatus = response.processStatus;
                    eInvoiceQueue.EiqModificationDate = DateTime.Now;

                    var historico = new ElectronicInvoiceHisto();

                    historico.EihIvoNumber = eInvoice.EinIvoNumber;
                    historico.EihFEProcessName = response.processName;
                    historico.EihFEProcessStatus = response.processStatus;
                    historico.EihFEProcessDate = response.processDate;
                    historico.EihFEErrorMessage = response.errorMessage;
                    historico.EihCreationDate = DateTime.Now;
                    historico.EihExternalId = eInvoice.EinExternalId;
                    historico.EihLegalStatus = string.IsNullOrEmpty(response.legalStatus) ? "Sin información." : InvoiceCarvajal.DictionaryLegalStatusAsTransLib[response.legalStatus];
                    historico.EihBusinessStatus = string.IsNullOrEmpty(response.businessStatus) ? "Sin información." : InvoiceCarvajal.DictionaryBusinessStatusAsTransLib[response.businessStatus];
                    historico.EihMreLib = db.Database.SqlQuery<string>(string.Format(AsTransQuery.ObtenerMreLibFromInvoice, eInvoice.EinIvoNumber)).FirstOrDefault();

                    db.ElectronicInvoiceHisto.Add(historico);
                }

                db.SaveChanges();
                transaction.Complete();
            }
        }

        protected void FinalizaMonitoreo(ElectronicInvoicing eInvoice, ElectronicInvoiceQueue eInvoiceQueue, DocumentStatusResponse response, AsTransEntities db)
        {
            eInvoice.EinLegalStatus = InvoiceCarvajal.DictionaryLegalStatusAsTrans[response.legalStatus];
            if (!string.IsNullOrEmpty(response.businessStatus))
                eInvoice.EinBusinessStatus = InvoiceCarvajal.DictionaryBusinessStatusAsTrans[response.businessStatus];

            if (eInvoiceQueue != null)
            {
                var historico = new ElectronicInvoiceHisto();

                historico.EihIvoNumber = eInvoice.EinIvoNumber;
                historico.EihFEProcessName = response.processName;
                historico.EihFEProcessStatus = response.processStatus;
                historico.EihFEProcessDate = response.processDate;
                historico.EihFEErrorMessage = response.errorMessage;
                historico.EihCreationDate = DateTime.Now;
                historico.EihExternalId = eInvoice.EinExternalId;
                historico.EihLegalStatus = InvoiceCarvajal.DictionaryLegalStatusAsTransLib[response.legalStatus];
                historico.EihBusinessStatus = InvoiceCarvajal.DictionaryBusinessStatusAsTransLib[response.businessStatus];
                historico.EihMreLib = db.Database.SqlQuery<string>(string.Format(AsTransQuery.ObtenerMreLibFromInvoice, eInvoice.EinIvoNumber)).FirstOrDefault();

                using (var transaction = new TransactionScope())
                {
                    db.ElectronicInvoiceQueue.Remove(eInvoiceQueue);
                    db.ElectronicInvoiceHisto.Add(historico);
                    db.SaveChanges();
                    transaction.Complete();
                }
            }
        }
    }
}
