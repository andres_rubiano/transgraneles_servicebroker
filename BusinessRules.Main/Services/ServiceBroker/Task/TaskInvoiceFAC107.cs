﻿using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using BusinessRules.Main.Resources;
using Common;
using Infrastructure.Data.Main.EF;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    /// <summary>
    /// Tarea ReportaAceptacionTacita
    /// </summary>
    public class TaskInvoiceFAC107 : CarvajalBaseTask, ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            Constantes = CarvajalInvoice.Constants();

            try
            {
                var invoiceList = new List<GetInvoiceForAceptTacitlyToCarvajal_Result>();
                ObjectResult<GetInvoiceForAceptTacitlyToCarvajal_Result> invoiceForAceptTacitlyToCarvajal_Result;

                using (var db = new AsTransEntities())
                {
                    invoiceForAceptTacitlyToCarvajal_Result = db.GetInvoiceForAceptTacitlyToCarvajal(invoiceNumber: task.StkReference);
                    invoiceList = invoiceForAceptTacitlyToCarvajal_Result.ToList();
                }

                if (!invoiceList.Any())
                {
                    task.EstablecerEnError(string.Format(Messages.errorInvoiceNotFound, task.StkReference)); //todo: validar el mensaje si es correcto
                    return task;
                }

                var invoice = invoiceList.FirstOrDefault();

                if (string.IsNullOrEmpty(invoice.DocumentReferenceUUID))
                {
                    task.EstablecerEnError(string.Format("Factura [{0}] no tiene registrado CUFE.", task.StkReference));
                    //Utility.CrearTarea("FAC107", task.StkReference, task.StkObjectJson, DateTime.Now.AddMinutes(Constantes.Fac107MinutosReprogramar), task.StkCreation_UsrCode);
                    return task;
                }

                var data = GeneraInformacion(invoice);

                // Se debe consignar el contenido del archivo, codificado en base 64. 
                byte[] fileContents;
                fileContents = Encoding.UTF8.GetBytes(data.ToString());
                var fileEncoded = Convert.ToBase64String(fileContents);

                var requestId = 0;
                var response = MetodoUpload(fileEncoded, "AceptacionTacita_" + task.StkReference + CarvajalInvoice.Constants().ExtensionArchivos, CarvajalInvoice.Constants().AccountIdAceptacionTacita, out requestId);

                task.StkRequestID = requestId;
                var respuesta = Utility.SerializeObject(response);
                task.StkResponseID = Utility.RegistrarResponse(respuesta);

                if (string.IsNullOrEmpty(response.transactionId))
                {
                    task.EstablecerEnError("Carvajal (status): " + response.status);
                    return task;
                }

                using (var db = new AsTransEntities())
                {
                    var tacitaQueue = db.ElectronicInvoiceTaticlyQueue.FirstOrDefault(t => t.EtqIvoNumber.Equals(task.StkReference));

                    using (var transaction = new TransactionScope())
                    {
                        if (tacitaQueue == null)
                        {
                            tacitaQueue = new ElectronicInvoiceTaticlyQueue();
                            tacitaQueue.EtqIvoNumber = task.StkReference;
                            tacitaQueue.EtqExternalId = response.transactionId;
                            tacitaQueue.EtqCreationDate = DateTime.Now;
                            db.ElectronicInvoiceTaticlyQueue.Add(tacitaQueue);
                        }
                        else
                        {
                            tacitaQueue.EtqExternalId = response.transactionId;
                            tacitaQueue.EtqCreationDate = DateTime.Now;
                        }
                        db.SaveChanges();
                        task.EstablecerEnOk("AsTrans: Tarea finalizada correctamente.");
                        transaction.Complete();
                    }
                }

                // Crear tarea FAC108: ConsultarEstadoDocumento de aceptacion tacita:
                if (!string.IsNullOrEmpty(response.transactionId))
                {
                    string objectJson = JsonConvert.SerializeObject(new { TransactionId = response.transactionId });
                    Utility.CrearTarea("FAC108", task.StkReference, objectJson, DateTime.Now, task.StkCreation_UsrCode);
                }
            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }
            return task;
        }

        private string GeneraInformacion(GetInvoiceForAceptTacitlyToCarvajal_Result invoice)
        {
            var data = new StringBuilder();
            data.AppendLine(@"<ApplicationResponse xmlns=""urn:oasis:names:specification:ubl:schema:xsd:ApplicationResponse-2"" ");
            data.AppendLine(@"                     xmlns:cac=""urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"" ");
            data.AppendLine(@"                     xmlns:cbc=""urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"" ");
            data.AppendLine(@"                     xmlns:ext=""urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"">");
            data.AppendLine(@"  <cbc:UBLVersionID>UBL 2.1</cbc:UBLVersionID>");
            data.AppendLine(@"  <cbc:CustomizationID>1</cbc:CustomizationID>");
            data.AppendLine(@"  <cbc:ProfileID>DIAN 2.1: ApplicationResponse de la Factura Electrónica de Venta</cbc:ProfileID>");

            if (string.IsNullOrEmpty(Constantes.EmailPruebaEntregaFactura))
                data.AppendLine(@"  <cbc:ProfileExecutionID>1</cbc:ProfileExecutionID>"); // Ambiente de produccion
            else
                data.AppendLine(@"  <cbc:ProfileExecutionID>2</cbc:ProfileExecutionID>"); // Ambiente de calidad

            data.AppendLine(@"  <cbc:ID>" + Guid.NewGuid().ToString("N") + "</cbc:ID>"); // Duda carvajal
            data.AppendLine(@"  <cbc:UUID schemeID=""2"" schemeName=""CUDE - SHA384""/>");
            data.AppendLine(@"  <cbc:IssueDate>" + invoice.IssueDate + "</cbc:IssueDate>");
            data.AppendLine(@"  <cbc:IssueTime>" + invoice.IssueTime + "</cbc:IssueTime>");
            data.AppendLine(@"  <cbc:Note>" + string.Format(Constantes.Fac107Note, invoice.Note, invoice.DocumentReferenceUUID, invoice.ReceiverPartyRegistrationName, invoice.ReceiverPartyCompanyID) + "</cbc:Note>");
            data.AppendLine(@"   <cac:SenderParty>");
            data.AppendLine(@"    <cac:PartyTaxScheme>");
            data.AppendLine(@"      <cbc:RegistrationName>" + invoice.SenderPartyRegistrationName + "</cbc:RegistrationName>");
            data.AppendLine(@"      <cbc:CompanyID schemeAgencyID=""" + Constantes.Fac107SenderPartySchemeAgencyID + "\" ");
            data.AppendLine(@"                     schemeAgencyName=""" + Constantes.Fac107schemeAgencyName + "\" ");
            data.AppendLine(@"                     schemeID=""" + invoice.SenderPartySchemeID + "\" ");
            data.AppendLine(@"                     schemeName=""" + invoice.SenderPartySchemeName + "\" ");
            data.AppendLine(@"                     schemeVersionID=""" + invoice.SenderPartySchemeVersionID + "\">" + invoice.SenderPartyCompanyID + "</cbc:CompanyID>");
            data.AppendLine(@"      <cac:TaxScheme>");
            data.AppendLine(@"        <cbc:ID>" + Constantes.Gte_1 + "</cbc:ID>");
            data.AppendLine(@"        <cbc:Name>" + Constantes.Gte_2 + "</cbc:Name>");
            data.AppendLine(@"      </cac:TaxScheme>");
            data.AppendLine(@"    </cac:PartyTaxScheme>");
            data.AppendLine(@"    <cac:Contact>");
            data.AppendLine(@"      <cbc:ElectronicMail>" + Constantes.Cde_4 + "</cbc:ElectronicMail>");
            data.AppendLine(@"    </cac:Contact>");
            data.AppendLine(@"  </cac:SenderParty>");
            data.AppendLine(@"  <cac:ReceiverParty>");
            data.AppendLine(@"     <cac:PartyTaxScheme>");
            data.AppendLine(@"      <cbc:RegistrationName>" + Constantes.Fac107RegistrationName + "</cbc:RegistrationName>");
            data.AppendLine(@"      <cbc:CompanyID schemeID=""" + invoice.ReceiverPartySchemeID + "\" ");
            data.AppendLine(@"                     schemeName=""" + invoice.ReceiverPartySchemeName + "\" ");
            data.AppendLine(@"                     schemeAgencyID=""" + Constantes.Fac107ReceiverPartySchemeAgencyID + "\" ");
            data.AppendLine(@"                     schemeAgencyName=""" + Constantes.Fac107schemeAgencyName + "\" ");
            data.AppendLine(@"                     schemeVersionID=""" + invoice.ReceiverPartySchemeVersionID + "\">" + Constantes.Fac107ReceiverPartyCompanyID + "</cbc:CompanyID>");
            data.AppendLine(@"      <cac:TaxScheme>");
            data.AppendLine(@"        <cbc:ID>" + Constantes.Gta_1 + "</cbc:ID>");
            data.AppendLine(@"        <cbc:Name>" + Constantes.Gta_2 + "</cbc:Name>");
            data.AppendLine(@"      </cac:TaxScheme>");
            data.AppendLine(@"    </cac:PartyTaxScheme>");
            data.AppendLine(@"    <cac:Contact>");
            data.AppendLine(@"      <cbc:ElectronicMail>" + invoice.ReceiverPartyElectronicMail + "</cbc:ElectronicMail>");
            data.AppendLine(@"    </cac:Contact>");
            data.AppendLine(@"  </cac:ReceiverParty>");
            data.AppendLine(@"  <cac:DocumentResponse>");
            data.AppendLine(@"    <cac:Response>");
            data.AppendLine(@"      <cbc:ResponseCode>" + Constantes.Fac107ResponseCode + "</cbc:ResponseCode>");
            data.AppendLine(@"      <cbc:Description>" + Constantes.Fac107ResponseDescription + "</cbc:Description>");
            data.AppendLine(@"    </cac:Response>");
            data.AppendLine(@"    <cac:DocumentReference>");
            data.AppendLine(@"      <cbc:ID>" + invoice.Note + "</cbc:ID>");
            data.AppendLine(@"      <cbc:UUID schemeName=""CUFE-SHA384"">" + invoice.DocumentReferenceUUID + "</cbc:UUID>");
            data.AppendLine(@"      <cbc:DocumentTypeCode>" + Constantes.Fac107DocumentTypeCode + "</cbc:DocumentTypeCode>");
            data.AppendLine(@"    </cac:DocumentReference>");
            data.AppendLine(@"    <cac:LineResponse>");
            data.AppendLine(@"      <cac:LineReference>");
            //data.AppendLine(@"        <cbc:LineID>1</cbc:LineID>");
            data.AppendLine(@"        <cbc:LineID/>");
            data.AppendLine(@"      </cac:LineReference>");
            data.AppendLine(@"      <cac:Response>");
            data.AppendLine(@"        <cbc:ResponseCode>" + Constantes.Fac107ResponseCode + "</cbc:ResponseCode>");
            data.AppendLine(@"        <cbc:Description>" + Constantes.Fac107ResponseDescription + "</cbc:Description>");
            data.AppendLine(@"      </cac:Response>");
            data.AppendLine(@"    </cac:LineResponse>");
            data.AppendLine(@"  </cac:DocumentResponse>");
            data.AppendLine(@"</ApplicationResponse>");

            return data.ToString();
        }
    }
}
