﻿using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using Common;
using Infrastructure.Data.Main.EF;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    public class TaskQrGenerateQRC001 : ITaskProcessor
    {
        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            string queryAtransact = "Select Top 1 ATRNUM as Comprobante, " +
                                    "             ATDTIENOM As Beneficiario, " +
                                    "             ATDTIESIRET As Identificacion, " +
                                    "             ATDVALD1 As Valor, " +
                                    "             VOYNUM As Viaje, " +
                                    "             VOYTRACODE As Vehiculo, " +
                                    "             CONVERT(VARCHAR(10), GetDate(), 111) As FechaActual " +
                                    "From   ATRANSACT " +
                                    "       Join ATRANSDET On ATDATRID = ATRID " +
                                    "       Left Join DEUDA On ATDDEUID = DEUID " +
                                    "       Left Join VOYAGE On VOYNUM = DEUORINUM " +
                                    "Where  ATRID = {0} " +
                                    "Order By ATDID ";

            string queryAtranscash = "Select ATHTIENOM As Beneficiario, " +
                                    "       ATHTIESIRET As Identificacion, " +
                                    "       ATHNUMDOC As NumeroReferencia " +
                                    "From   ATRANSCASH " +
                                    "Where  ATHATRID = {0} " +
                                    "Order By ATHID ";

            try
            {

                using (var db = new AsTransEntities())
                {
                    var atransactDto = db.Database.SqlQuery<AtransactDto>(string.Format(queryAtransact, task.StkReference)).FirstOrDefault();
                    var atranscashDto = db.Database.SqlQuery<AtranscashtDto>(string.Format(queryAtranscash, task.StkReference)).FirstOrDefault();

                    string qrInformation = string.Format("Comprobante: {0}\r\n", atranscashDto.NumeroReferencia);
                    qrInformation += string.Format("Beneficiario: {0}\r\n", atranscashDto.Beneficiario);
                    qrInformation += string.Format("Identificacion: {0}\r\n", atranscashDto.Identificacion);
                    qrInformation += string.Format("Valor: {0}\r\n", atransactDto.Valor.ToString("#,##0"));
                    qrInformation += string.Format("Viaje: {0}\r\n", atransactDto.Viaje);
                    qrInformation += string.Format("Vehiculo: {0}\r\n", atransactDto.Vehiculo);

                    // Control de estructura de carpetas: YYYY\MM\DD:
                    var pathStructDate = atransactDto.FechaActual.Substring(0, 4) + @"\" + atransactDto.FechaActual.Substring(5, 2) + @"\" + atransactDto.FechaActual.Substring(8, 2) + @"\";
                    var pathQrCode = AsTransConfig.Constants().Qrc001_UbicacionArchivos + pathStructDate;

                    string fullFileName;
                    QrCodeGenerator.Create(atransactDto.Comprobante, qrInformation, pathQrCode, out fullFileName);

                    // EL QR se generó:
                    using (var transaction = new TransactionScope())
                    {
                        var registroQr = db.AtransactQrCode.Where(w => w.AqrNumber.Equals(atransactDto.Comprobante)).FirstOrDefault();
                        if (registroQr != null)
                        {
                            registroQr.AqrLocalPathQrCode = pathQrCode + fullFileName;
                            registroQr.AqrUrlQrCode = (AsTransConfig.Constants().Qrc001_DirectorioVirtual + pathStructDate + fullFileName).Replace(@"\", "/");
                        }
                        else
                        {
                            AtransactQrCode registroNuevo = new AtransactQrCode();
                            registroNuevo.AqrNumber = atransactDto.Comprobante;
                            registroNuevo.AqrLocalPathQrCode = pathQrCode + fullFileName;
                            registroNuevo.AqrUrlQrCode = (AsTransConfig.Constants().Qrc001_DirectorioVirtual + pathStructDate + fullFileName).Replace(@"\", "/");
                            db.AtransactQrCode.Add(registroNuevo);
                        }

                        db.SaveChanges();
                        transaction.Complete();
                    }
                    task.EstablecerEnOk("AsTrans: Tarea finalizada correctamente.");
                }
            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);

                task.EstablecerEnError(ex.Message);

            }
            return task;
        }

        private class AtransactDto
        {
            public string Comprobante { get; set; }
            public string Beneficiario { get; set; }
            public string Identificacion { get; set; }
            public double Valor { get; set; }
            public string Viaje { get; set; }
            public string Vehiculo { get; set; }
            public string FechaActual { get; set; }
        }

        private class AtranscashtDto
        {
            public string Beneficiario { get; set; }
            public string Identificacion { get; set; }
            public string NumeroReferencia { get; set; }
        }
    }
}
