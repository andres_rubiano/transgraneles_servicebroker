﻿using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using Common;
using Infrastructure.Data.Main.EF;
using Infrastructure.Interoperability.CenFinancieroService;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    /// <summary>
    /// Tarea ValidaAceptacionDocumento
    /// </summary>
    public class TaskInvoiceFAC106 : CarvajalBaseTask, ITaskProcessor
    {
        private string NumeroDocumento { get; set; }
        private string TransactionId { get; set; }
        private string TipoDocumentoFe { get; set; }

        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            Constantes = CarvajalInvoice.Constants();

            try
            {
                dynamic objeto = JsonConvert.DeserializeObject(task.StkObjectJson);
                NumeroDocumento = objeto.NumeroDocumento;
                TransactionId = objeto.TransactionId;
                TipoDocumentoFe = objeto.TipoDocumentoFe;

                #region Consumir servicio "DocumentStatus"

                var requestId = 0;
                var response = MetodoDocumentStatus(TransactionId, out requestId);
                task.StkRequestID = requestId;

                var respuesta = Utility.SerializeObject(response);
                task.StkResponseID = Utility.RegistrarResponse(respuesta);

                #endregion

                if (response.legalStatus.Equals("ACCEPTED"))
                {
                    DateTime fechaReceived = DateTime.MinValue;

                    using (var db = new AsTransEntities())
                    {
                        fechaReceived = (DateTime)db.ElectronicInvoicing.Where(e => e.EinIvoNumber.Equals(task.StkReference)).Select(e => e.EinBusinessReceivedDate).FirstOrDefault();
                    }

                    if (fechaReceived == DateTime.MinValue)
                    {
                        task.EstablecerEnError(string.Format("AsTrans: La fecha [{0}] del evento RECEIPT_GOODS_SERVICES no es valida.", fechaReceived));
                        return task;
                    }

                    switch (response.businessStatus)
                    {
                        case "RECEIPT_GOODS_SERVICES":
                            
                            var diferenciaHoras = (DateTime.Now - fechaReceived).TotalHours;
                            if (diferenciaHoras > Constantes.Fac106HorasValidaAceptacionTacita)
                            {
                                task.EstablecerEnOk("AsTrans: Tarea terminada, se crea tarea para reportar Aceptación Tácita.");
                                Utility.CrearTarea("FAC107", NumeroDocumento, task.StkObjectJson, DateTime.Now, task.StkCreation_UsrCode);
                            }
                            else
                            {
                                task.EstablecerEnOk(string.Format("AsTrans: No han pasado [{0}] horas desde la fecha [{1}] del evento RECEIPT_GOODS_SERVICES.", Constantes.Fac106HorasValidaAceptacionTacita, fechaReceived));
                                Utility.CrearTarea("FAC106", NumeroDocumento, task.StkObjectJson, DateTime.Now.AddMinutes(Constantes.Fac106MinutosReprogramar), task.StkCreation_UsrCode);
                            }

                            break;

                        case "PUBLISHED":
                        case "SENT":
                        case "CONSULTED":
                        case "RECEIVED":
                            task.EstablecerEnOk("AsTrans: No se ha recibido el evento RECEIPT_GOODS_SERVICES, se reprograma tarea.");
                            Utility.CrearTarea("FAC106", NumeroDocumento, task.StkObjectJson, DateTime.Now.AddMinutes(Constantes.Fac106MinutosReprogramar), task.StkCreation_UsrCode);
                            break;

                        default:
                            break;
                    }

                }
                else
                {
                    task.EstablecerEnError(string.Format("El documento [{0}] debe tener el estado legal [Exitoso en Muisca, aceptado por la Dian]"));
                }

            }
            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }
            return task;
        }
    }
}
