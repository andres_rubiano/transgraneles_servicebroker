﻿using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.Interfaces.ServiceBroker;
using Infrastructure.Interoperability.CenFinancieroService;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.ServiceModel.Description;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Xml;
using System.IO;
using Infrastructure.Data.Main.EF;
using System.Transactions;
using Newtonsoft.Json;
using Common.Constants;
using System.Data.Entity.Validation;

namespace BusinessRules.Main.Services.ServiceBroker.Task
{
    /// <summary>
    /// Tarea ConsultarEstadoDocumento
    /// </summary>
    public class TaskInvoiceFAC105 : CarvajalBaseTask, ITaskProcessor
    {
        private string NumeroDocumento { get; set; }
        private string TransactionId { get; set; }
        private string TipoDocumentoFe { get; set; }

        public brServiceTaskDTO ExecuteTask(brServiceTaskDTO task)
        {
            Constantes = CarvajalInvoice.Constants();

            try
            {
                // Decodificar ObjectJson:

                dynamic objeto = JsonConvert.DeserializeObject(task.StkObjectJson);
                NumeroDocumento = objeto.NumeroDocumento;
                TransactionId = objeto.TransactionId;
                TipoDocumentoFe = objeto.TipoDocumentoFe;

                #region Consumir servicio "DocumentStatus"

                var requestId = 0;
                var response = MetodoDocumentStatus(TransactionId, out requestId);
                task.StkRequestID = requestId;

                var respuesta = Utility.SerializeObject(response);
                task.StkResponseID = Utility.RegistrarResponse(respuesta);

                #endregion

                //Si sucede un error es capturado por la excepción de aqui en adelante se asume correcta respuesta del servcio.

                if (response.errorMessage.Equals("DIAN_RESULT"))
                {
                    var id = 0;
                    var responseDownload = MetodoDownload(NumeroDocumento, TipoDocumentoFe, "DIAN_RESULT", out id);

                    var encodedString = responseDownload.downloadData;

                    byte[] data = Convert.FromBase64String(encodedString);
                    string decodedString = Encoding.UTF8.GetString(data);

                    var demoId = Utility.RegistrarResponse(decodedString);

                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.DtdProcessing = DtdProcessing.Parse;
                    XmlReader reader = XmlReader.Create(new StringReader(decodedString), settings);

                    string mensaje = string.Empty;

                    mensaje = XMLTools.GetValue(decodedString, "c:string").ToString();

                    if (!string.IsNullOrEmpty(mensaje))
                        response.errorMessage = mensaje;
                    else
                        response.errorMessage = "AsTrans: No se encontró mensaje para DIAN_RESULT.";

                }

                #region Analisis de LegalStatus:

                using (var db = new AsTransEntities())
                {
                    var eInvoice = db.ElectronicInvoicing.Where(e => e.EinIvoNumber.Equals(task.StkReference)).FirstOrDefault();
                    var eInvoiceQueue = db.ElectronicInvoiceQueue.FirstOrDefault(e => e.EiqIvoNumber.Equals(task.StkReference));

                    //if (string.IsNullOrEmpty(response.legalStatus))
                    //    response.legalStatus = "FAIL";

                    switch (response.legalStatus)
                    {
                        case "RECEIVED":
                        case "FAIL":
                        case "RETRY":
                            ActualizaMonitor(eInvoice, eInvoiceQueue, response, db);
                            task.EstablecerEnOk("AsTrans: Información actualizada");
                            Utility.CrearTarea("FAC105", task.StkReference, task.StkObjectJson, DateTime.Now.AddMinutes(Constantes.Fac105MinutosReprogramar), task.StkCreation_UsrCode);

                            break;

                        case "REJECTED":
                            if (eInvoiceQueue != null)
                            {
                                ActualizaMonitor(eInvoice, eInvoiceQueue, response, db);
                                task.EstablecerEnOk("AsTrans: Información actualizada");
                            }
                            break;

                        case "ACCEPTED":
                            ProcesarAccepted(task, eInvoice, eInvoiceQueue, response, db);
                            break;

                        default:
                            var mensaje = string.Format("AsTrans: Estado legal [{0}] no reconocido, validar información recibida del servicio CEN.", response.legalStatus);
                            task.EstablecerEnOk(mensaje);
                            break;
                    }

                }

                #endregion

            }

            catch (Exception ex)
            {
                Exception customException;
                ExceptionPolicy.HandleException(ex, "LoggingReplaceException", out customException);

                string innerMessage = string.Empty;
                if (ex.InnerException != null)
                    innerMessage = ex.InnerException.Message;
                task.EstablecerEnError(string.Format("{0}. {1}", ex.Message, innerMessage));
            }

            return task;
        }

        private void ProcesarAccepted(brServiceTaskDTO task, ElectronicInvoicing eInvoice, ElectronicInvoiceQueue eInvoiceQueue, DocumentStatusResponse response, AsTransEntities db)
        {
            var metodoPago = db.Database.SqlQuery<string>(string.Format(AsTransQuery.ObtenerMetodoPagoInvoice, eInvoice.EinIvoNumber)).FirstOrDefault();

            if (metodoPago != null && metodoPago.Equals("Credito"))
            {

                switch (response.businessStatus)
                {
                    case "ACCEPTED":
                    case "REJECTED":
                    case "ACEPTED_TACITLY":
                        FinalizaMonitoreo(eInvoice, eInvoiceQueue, response, db);
                        task.EstablecerEnOk("AsTrans: Información actualizada");
                        break;

                    case "PUBLISHED":
                    case "SENT":
                    case "CONSULTED":
                    case "RECEIVED":
                        ActualizaMonitor(eInvoice, eInvoiceQueue, response, db);
                        task.EstablecerEnOk("AsTrans: Información actualizada");
                        Utility.CrearTarea("FAC105", NumeroDocumento, task.StkObjectJson, DateTime.Now.AddMinutes(Constantes.Fac105MinutosReprogramarCuandoLegalAceptado), task.StkCreation_UsrCode);
                        break;

                    case "RECEIPT_GOODS_SERVICES":

                        var id = 0;
                        var responseDownload = MetodoDownload(NumeroDocumento, TipoDocumentoFe, "XML_DIAN_BS", out id);

                        var encodedString = responseDownload.downloadData;

                        byte[] data = Convert.FromBase64String(encodedString);
                        string decodedString = Encoding.UTF8.GetString(data);

                        XmlReaderSettings settings = new XmlReaderSettings();
                        settings.DtdProcessing = DtdProcessing.Parse;
                        XmlReader reader = XmlReader.Create(new StringReader(decodedString), settings);

                        var fec = XMLTools.GetFirtsValue(decodedString, "cbc:IssueDate");
                        var tim = XMLTools.GetFirtsValue(decodedString, "cbc:IssueTime").Truncate(8); // se remueve zon, siempre llega -05:00

                        string fechaEvento = string.Format("{0} {1}", fec, tim);
                        DateTime fecha = Utility.CastStringToValidDatetimeFormats(fechaEvento);

                        if (fecha > DateTime.MinValue)
                        {
                            eInvoice.EinBusinessReceivedDate = fecha;
                            FinalizaMonitoreo(eInvoice, eInvoiceQueue, response, db);
                            task.EstablecerEnOk("AsTrans: Finaliza monitoreo por estado comercial RECEIPT_GOODS_SERVICES.");

                            #region Registra en cola de aceptacion tacita
                            var tacitaQueue = db.ElectronicInvoiceTaticlyQueue.FirstOrDefault(t => t.EtqIvoNumber.Equals(NumeroDocumento));

                            using (var transaction = new TransactionScope())
                            {
                                if (tacitaQueue == null)
                                {
                                    tacitaQueue = new ElectronicInvoiceTaticlyQueue();
                                    tacitaQueue.EtqIvoNumber = NumeroDocumento;
                                    tacitaQueue.EtqExternalId = "Pendiente de envio de Aceptacion tacita";
                                    tacitaQueue.EtqDateReceiptOfGoodOrService = fecha;
                                    tacitaQueue.EtqCreationDate = DateTime.Now;
                                    db.ElectronicInvoiceTaticlyQueue.Add(tacitaQueue);
                                }
                                else
                                {
                                    tacitaQueue.EtqExternalId = "Pendiente de envio de Aceptacion tacita";
                                    tacitaQueue.EtqDateReceiptOfGoodOrService = fecha;
                                    tacitaQueue.EtqCreationDate = DateTime.Now;
                                }
                                db.SaveChanges();
                                transaction.Complete();
                            }
                            #endregion

                            Utility.CrearTarea("FAC106", NumeroDocumento, task.StkObjectJson, DateTime.Now.AddMinutes(Constantes.Fac106MinutosReprogramar), task.StkCreation_UsrCode);
                        }
                        else
                        {
                            ActualizaMonitor(eInvoice, eInvoiceQueue, response, db);
                            task.EstablecerEnError(string.Format("AsTrans: La fecha obtenida [{0}] para el evento RECEIPT_GOODS_SERVICES, no es válida (Metodo Download de CENFinanciero).", fechaEvento));
                        }

                        break;

                    default:
                        var mensaje = string.Format("AsTrans: Estado comercial [{0}] no reconocido, validar información recibida del servicio CEN.", response.businessStatus);
                        task.EstablecerEnOk(mensaje);
                        break;
                }
            }
            else
            {
                // Finaliza el monitoreo:

                if (eInvoiceQueue != null)
                {
                    var mensaje = string.Format("AsTrans: El método o condición de pago NO es crédito, se finaliza monitoreo del documento.");
                    response.errorMessage = mensaje;
                    FinalizaMonitoreo(eInvoice, eInvoiceQueue, response, db);
                    task.EstablecerEnOk(mensaje);
                }
            }
        }
    }
}
