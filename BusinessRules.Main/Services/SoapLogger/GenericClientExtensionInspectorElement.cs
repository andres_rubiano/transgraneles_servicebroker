﻿using System;
using System.ServiceModel.Configuration;

namespace BusinessRules.Main.Services.SoapLogger
{
    /// <summary>
    /// Simplexity S.A
    /// Date: 01:58 a.m.
    /// User:SPX-CESAR
    /// Machine:SPX-CESAR-PC
    /// Class Name: GenericClientExtensionInspectorElement
    /// Author:Cesar Fernando Estupiñan
    /// Description: Se implementa BehaviorExtensionElement con el fin de extender el comportamiento del servicio al utilizar el behaivor 
    /// correspondiente y así registrar el log correspondiente, La implementación expone el ClientLog
    /// </summary>
    public class GenericClientExtensionInspectorElement : BehaviorExtensionElement
    {
        public override Type BehaviorType
        {
            get { return typeof (GenericClientInspectorBehavior); }
        }

        protected override object CreateBehavior()
        {
            return new GenericClientInspectorBehavior();
        }
    }
}