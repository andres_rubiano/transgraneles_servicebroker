﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace BusinessRules.Main.Services.SoapLogger
{
    /// <summary>
    /// Simplexity S.A
    /// Date: 01:45 a.m.
    /// User:SPX-CESAR
    /// Machine:SPX-CESAR-PC
    /// Class Name: ClientLog
    /// Author:Cesar Fernando Estupiñan
    /// Description: Se implementa la clase IClientMessageInspector con el fin de capturar las peticiones salientes para los endpoints configurados para realizar trazabilidad 
    /// </summary>
    public class ClientLog : IClientMessageInspector
    {
        private readonly ServiceEndpoint serviceEndpoint;
        private string soapRequest = string.Empty;
        private string soapUrl = string.Empty;

        /// <summary>
        /// Con el fin de tener información completa del endpoint y poderlo identificar en DB, se solicita el endpoint en el constructor
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        public ClientLog(ServiceEndpoint endpoint)
        {
            serviceEndpoint = endpoint;
        }

        #region IClientMessageInspector Members

        /// <summary>
        /// Simplexity S.A
        /// Method: AfterReceiveReply
        /// Author: Cesar Fernando Estupiñan 
        /// Date: 15/11/2013
        /// Objective: Finalizada la invocación al servicio se captura el soap de respuesta y se utiliza el método de persistencia existente
        /// </summary>
        /// <param name="reply">The reply.</param>
        /// <param name="correlationState">State of the correlation.</param>
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            if (!String.IsNullOrEmpty(soapRequest))
            {
                LogService.CreateSoapCallLog(serviceEndpoint.Contract.Name, soapRequest, reply.ToString());
            }
        }

        /// <summary>
        /// Simplexity S.A
        /// Method: BeforeSendRequest
        /// Author: Cesar Fernando Estupiñan 
        /// Date: 15/11/2013
        /// Objective:Se captura el mensaje soap que será enviado mediante el servicio 
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="channel">The channel.</param>
        /// <returns>Object</returns>
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            if (!string.IsNullOrEmpty(request.ToString()))
            {
                soapRequest = request.ToString();
            }
            return null;
        }

        #endregion
    }
}