﻿using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace BusinessRules.Main.Services.SoapLogger
{
    /// <summary>
    /// Simplexity S.A
    /// Date: 02:00 a.m.
    /// User:SPX-CESAR
    /// Machine:SPX-CESAR-PC
    /// Class Name: GenericClientInspectorBehavior
    /// Author:Cesar Fernando Estupiñan
    /// Description: Se implementa el IEndpointBehavior con el fin de extender la funcionalidad a nivel de EndPoint
    /// Se modifica el comportamiento solo para el comportamiento de ApplyClientBehavior del servicio
    /// </summary>
    public class GenericClientInspectorBehavior : IEndpointBehavior
    {
        #region IEndpointBehavior Members

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
            //throw new NotImplementedException();
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new ClientLog(endpoint));
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            //throw new NotImplementedException();
        }

        public void Validate(ServiceEndpoint endpoint)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}