﻿using System;
using System.Data.SqlClient;
using System.Linq;
using BusinessRules.Main.DTOs.BRProviderService;
using Infrastructure.Data.Main.EF;

namespace BusinessRules.Main.Services.BRProviderService
{
    public class BrProviderServiceRule // : BaseService
    {
        public string InsertBrProviderServiceRule(BrProviderServiceDTO itemNew)
        {
            try
            {
                using (var bd = new AsTransEntities())
                {
                    var brServiceProvider1 = new brServiceProvider()
                    {
                        SprCode = itemNew.SprCode,
                        SprName = itemNew.SprName,
                        SprAuthenticationRequired = itemNew.SprAuthenticationRequired,
                        SprUser = itemNew.SprUser,
                        SprDomain = itemNew.SprDomain
                    };
                    bd.brServiceProvider.Add(brServiceProvider1);
                    bd.SaveChanges();

                    SetPasswordToUserRule(itemNew.SprCode, itemNew.SprPassword);
                }
            }
            catch (Exception error)
            {
                return error.ToString();
            }
            return "0";
        }

        public int SetPasswordToUserRule(string sprCode, string sprPassword)
        {
            try
            {
                using (var bd = new AsTransEntities())
                {
                    var @params = new[] { new SqlParameter("@SprPassword", sprPassword), new SqlParameter("@SprCode", sprCode), };
                    return bd.Database.ExecuteSqlCommand(@"UPDATE brServiceProvider SET SprPassword = ENCRYPTBYPASSPHRASE('spxc1', @SprPassword) where (SprCode =  @SprCode)", @params);
                }
                return 0;
            }
            catch (Exception error)
            {
                return 0;
            }
        }
        public string GetPasswordOfUserRule(string sprCode)
        {
            try
            {
                using (var bd = new AsTransEntities())
                {
                    var @params = new[] { new SqlParameter("@SprCode", sprCode), };
                    var result = bd.Database.SqlQuery<BrProviderServicePsswDTO>(@"select  CONVERT(VARCHAR(300), DECRYPTBYPASSPHRASE('spxc1', SprPassword)) AS SprPassword from  brServiceProvider where (SprCode = @SprCode)", @params).ToList();
                    return result[0].SprPassword;
                }
            }
            catch (Exception error)
            {
                return string.Empty;
            }
        }

        public bool CheckingUserExistsRule(string us, string ps)
        {
            try
            {
                using (var bd = new AsTransEntities())
                {
                    var @params = new[] { new SqlParameter("@u", us),
                                   new SqlParameter("@p", ps)};
                    var result = bd.Database.SqlQuery<BrProviderServiceDTO>(@"select * from  brServiceProvider where SprUser = @u  and @p = CONVERT(VARCHAR(300), DECRYPTBYPASSPHRASE('spxc1', SprPassword))", @params).ToList();
                    if (result.Count >= 1) return true;
                }
                return false;
            }
            catch (Exception error)
            {
                return false;
            }
        }

        public BrProviderServiceDTO GetBrProviderServiceRule(string idSprCode)
        {
            using (var bd = new AsTransEntities())
            {
                var itemresult = bd.brServiceProvider.FirstOrDefault(x => x.SprCode == idSprCode);
                if (itemresult != null)
                {
                    return new BrProviderServiceDTO
                    {
                        SprId = itemresult.SprId != null ? itemresult.SprId : 0,
                        SprCode = itemresult.SprCode ?? string.Empty,
                        SprName = itemresult.SprName ?? string.Empty,
                        SprAuthenticationRequired = (bool)itemresult.SprAuthenticationRequired,
                        SprUser = itemresult.SprUser ?? string.Empty,
                        SprPassword = GetPasswordOfUserRule(itemresult.SprCode) ?? string.Empty,
                        SprDomain = itemresult.SprDomain ?? string.Empty,
                    };
                }
                return null;
            }
        }
    }
}
