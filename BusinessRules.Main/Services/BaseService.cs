﻿using Infrastructure.Data.Main.EF;

namespace BusinessRules.Main.Services
{
    public class BaseService
    {
        public BaseService()
        {
            DbContext = null;
        }

        /// <summary>
        /// Contexto para el manejo de la base de datos
        /// </summary>
        public AsTransEntities DbContext { get; set; }




    }
}
