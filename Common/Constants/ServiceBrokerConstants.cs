﻿namespace Common.Constants
{
    /// <summary>
    /// Simplexity S.A
    /// Date: 09:28 a.m.
    /// User:SPX
    /// Machine:CESAR-PC
    /// Class Name: ServiceBrokerConstants
    /// Author: Cesar Fernando Estupiñan 
    /// Description:
    /// </summary>
    public class ServiceBrokerConstants
    {
        /// <summary>
        /// Enumerador que identifica el tipo de tarea registrada
        /// </summary>
        public enum ServiceTaskTypes
        {
            New = 1,
            AutomaticRetry = 2,
            PreconditionRetry = 3,
            PosconditionRetry = 4
        }

        /// <summary>
        /// Enumerador que indica los diferentes estados de una tarea de servicio
        /// </summary>
        public enum ServiceTaskState
        {
            Scheduled = 1,
            Processing = 2,
            Processed = 3,
            Locked = 4
        }

        /// <summary>
        /// Enumerador que indica la respuesta generada al validar una precondición de ejecución
        /// </summary>
        public enum TaskPreconditionState
        {
            Valid = 1,
            Error = 2,
            Scheduled = 3,
            Processing = 4
        }

        public static class Datos
        {
            public const string user = "auto";
        }
        public static class Mensajes
        {
            public const string bloqueo = "Registro bloqueado para procesamiento por precondición pendiente de procesar.";
        }
        public static class BrockerServices
        {
            public const string ErrorHospitalForRetry = "INT-100";
            
        }
        public static class CallLogTypes
        {
            public const int Request = 1;
            public const int Response = 2;
        }
    }
}
