﻿using System.Collections.Generic;

namespace Common.Constants
{
    public static class InvoiceCarvajal
    {
        public static Dictionary<string, string> TypeInvoice = new Dictionary<string, string>
        {
            {"INVOIC", "FACTURA"},
            {"NC", "NOTA"},
            {"ND", "NOTA"}
        };

        public const string Factura = "INVOIC";
        public const string AnulacionFactura = "NC";
        public const string NotaCredito = "NC";
        public const string NotaDebito = "ND";

        public static Dictionary<string, string> HomologationTypeInvoice = new Dictionary<string, string>
        {
            {"DE-FVENT", "FV"},
            {"DE-FVMAN", "FV"},
            {"DE-FVTAD", "FV"},
            {"DE-NCC-000","NC"},
            {"DE-NDC-000", "ND"}
        };

        public static Dictionary<string, short> DictionaryLegalStatusAsTrans = new Dictionary<string, short>
        {
            { "RETRY", 20 },
            { "FAIL", 30 },
            { "REJECTED", 40 },
            { "ACCEPTED", 50 },
            { "RECEIVED", 10 }
        };

        public static Dictionary<string, short> DictionaryEventStatusAsTrans = new Dictionary<string, short>
        {
            { "DOCUMENT_ACCEPTED", 10 },
            { "DOCUMENT_REJECTED", 20 },
            { "AVAIBILITY_EVENT_DETECTED", 30 },
            { "RESPONSE_INTEGRITY_EVENT_DETECTED", 40 },
            { "FAIL", 50 },
            { "PROCESSING", 60 }
        };

        public static Dictionary<string, short> DictionaryBusinessStatusAsTrans = new Dictionary<string, short>
        {
            {"RECEIPT_GOODS_SERVICES", 80},
            {"ACEPTED", 70},
            {"REJECTED", 60},
            {"ACCEPTED", 50},
            {"CONSULTED", 40},
            {"RECEIVED", 30},
            {"SENT", 20},
            {"PUBLISHED", 10}
        };


        public static Dictionary<string, string> DictionaryLegalStatusAsTransLib = new Dictionary<string, string>
        {
            {"RETRY", "En proceso de reintentos para obtener la respuesta de la Verificación del estado DIAN"},
            {"FAIL", "No ha sido posible realizar el envio o consulta con el WS de la DIAN"},
            {"REJECTED", "Con errores en Muisca, rechazado por la Dian"},
            {"ACCEPTED", "Exitoso en Muisca, aceptado por la Dian"},
            {"RECEIVED", "Recibido por la Dian"}
        };

        public static Dictionary<string, string> DictionaryBusinessStatusAsTransLib = new Dictionary<string, string>
        {
            {"RECEIPT_GOODS_SERVICES", "Recibo del bien o servicio"},
            {"ACEPTED", "Aceptado Tácitamente"},
            {"REJECTED", "Rechazado por el adquiriente"},
            {"ACCEPTED", "Aceptado por el adquiriente"},
            {"CONSULTED", "Consultado"},
            {"RECEIVED", "Recibido por el adquiriente"},
            {"SENT", "Enviado al adquiriente"},
            {"PUBLISHED", "Publicado"}
        };
    }

    public static class AsTransQuery
    {
        public const string ObtenerMreLibFromInvoice = "select MRELIB from INVOICE join MODREG on MREID = IVOMREID where IVONUM = '{0}'";

        public const string ObtenerMetodoPagoInvoice = "Select 'Credito' " +
                                                       "From   INVOICE " +
                                                       "       Join MODREG On MREID = IVOMREID " +
                                                       "Where  IVONUM = '{0}' " +
                                                       "       And MREID Not In (1, 9, 22) ";

        public const string ObtenerDocumentosSinCufeCude = "Select Top 20 EinIvoNumber " +
                                                           "From   ElectronicInvoicing " +
                                                           "       Join INVOICE On IVONUM = EinIvoNumber " +
                                                           "Where  EinLegalStatus = 50 " +
                                                           "       And (IVOFEDOCNUMBER Is Null " +
                                                           "            Or IVOFEDOCNUMBER = '') ";
    }
}
