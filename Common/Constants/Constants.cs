﻿using System.Collections.Generic;

namespace Common.Constants
{
    public class MessagesConstants
    {
        #region Types enum

        public enum Types
        {
            None = 0,
            Information = 1,
            Warning = 2,
            OperationalWarnning = 3,
            Error = 4,
            OperationalError = 5 // Tipo de error para los errores que van a consumir el hospital de errores    
        }

        #endregion
    }


    public static class WorkFlowConstants
    {
        public static string Orders
        {
            get { return "ORDERS"; }
        }

        public static string Shipments
        {
            get { return "SHIPMENTS"; }
        }

        public static string Loads
        {
            get { return "LOADS"; }
        }
    }

    public static class OperationConstants
    {
        public static char True
        {
            get { return 'T'; }
        }

        public static char False
        {
            get { return 'F'; }
        }

        public enum EstadoTareaCumplido
        {
            Creada = 10,
            Procesando = 25,
            Procesada = 30,
            CumplidoAsTrans = 40,
            Erronea = 20
        }

        public enum EstadoTareaAnulado
        {
            Creada = 10,
            Procesando = 25,
            Procesada = 30,
            AnuladoAsTrans = 40,
            Erronea = 20
        }

    }

    public static class SpringContext
    {
        public const string AsContext = "AsContext";
        public const string ServiceBrokerContext = "ServiceBrokerContext";
    }

    public static class DocumentTyes
    {
        public const string Cedula = "13";
        public const string Nit = "31";
    }

    public static class RegimenFiscal
    {
        public const string ResponsableIva = "48";
        public const string NoResponsableIva = "49";
    }

    public static class AstransIntegrationsTypes
    {
        public const string Vehicles = "Vehiculos";
        public const string Drivers = "Conductores";
        public const string Movement = "Int. Movimientos";
        public const string Dispatch = "Int.Despacho";
    }

    public static class EventTypesCodeConstants
    {
        public const string Negocio = "B001";
        public const string Facturacion = "FACT";
    }

    public static class UserCredentialsTC
    {
        public const string Auto = "auto";
    }

    public static class ErrorHospitalObjectBusiness
    {
        public const string NameService = "AST";
        public const string ConfirmacionEntrega = "CONFIRMACION ENTREGA";
        public const string Despacho = "DESPACHO";
        public const string Terceros = "TERCEROS";

    }

    public static class ExceptionManagerConstants
    {
        public static string DefaultExceptionPolicie
        {
            get { return "LoggingReplaceException"; }
        }
    }

    public static class FunctionalErrors
    {
        public static IDictionary<string, string> DispatchErrors
        {
            get
            {
                return new Dictionary<string, string>
                           {
                               {"0001", "No existe un registro relacionado al vehículo con placa: {0} activo en el sistema."},
                               {"0002", "No existe un registro relacionado al remolque con placa: {0} activo en el sistema."},
                               {"0003", "No existe un registro relacionado al conductor con código: {0}, o no esta activo en el sistema."},
                               {
                                   "0004", "No existe una registro relacionado a la sociedad con código: {0} en el sistema."
                               },
                               {"0005", "No existe una homologación para la agencia con código: {0} en el sistema."},
                               {"0006", "No existe un registro relacionado a la ciudad con código: {0} en el sistema."},
                               {
                                   "0007",
                                   "No existe un registro relacionado al departamento con código: {0} en el sistema."
                               },
                               {"0008", "No existe un registro relacionado al país con código: {0} en el sistema."},
                               {
                                   "0009",
                                   "No existe un registro relacionado al tipo de pedido con código: {0} en el sistema."
                               },
                               {"0010", "No existe un registro relacionado al producto con código: {0} en el sistema."},

                               {"0011", "El flete por tonelada del producto {0} es superior a ${1}"},
                               {"0012", "El código de la unidad de peso {0} no esta homologado."},
                               {"0013", "El código de la unidad de empaque  {0} no esta homologado."},
                               {"0014", "No existe flete para el producto {0}  ó el flete unitario es cero."},
                               {"0015", "El peso neto del producto debe ser mayo que cero."},
                               {"0016", "El numero de remision  {0} no tiene productos."},
                               {"0017", "Homologación de tipo de identificación {0} del remitente no existe"},
                               {"0018", "Estado del viaje {0} no permite la operación."},
                               {"0019", "Remision sin la cantidad minima de terceros. Minima =3 Actual={0}"},
                               {"0020", "El codigo de Ruta SAP {0} no esta homologado en el sistema"},
							   {"0021", "El interlocultor Bill {0} no existe o no está activo en el sistema"}
                               
                           };
            }
        }

        public static IDictionary<string, string> ConfirmacionErrors
        {
            get
            {
                return new Dictionary<string, string>
                           {
                               {"0001", "No existe un registro relacionado a la ciudad con código: {0} en el sistema."},
                               {"0002", "No existe un registro relacionado al Tipo de documento: {0} en el sistema"},
                               {
                                   "0003",
                                   "No existe registro relacionado a un operador logístico con código: {0} activo en el sistema."
                               },
                               {
                                   "0004",
                                   "No existe la tarifa para el producto {0} con el tipo de novedad {1} en el sistema"
                               },
                               {"0005", "No existe el tipo de novedad {0} del viaje {1} "},
                               {"0006", "No existe la remision {0} del viaje {1} en el sistema "},
                               {"0007", "No existe el detalle de remision {0} en el sistema "},
                               {"0008", "La informacion del viaje remoto reportado es incorrecta, se debe validar el peso/UM del proveedor"},
                               {"0009", "El viaje {0} no existe ó no puede ser confirmado"},
                               {"0010", "El viaje {0} reporta la cantidad real entregada en 0."},

                           };
            }
        }

        public static IDictionary<string, string> MasterOLServices
        {
            get
            {
                return new Dictionary<string, string>
                           {
                               {"0001", "No existe un registro relacionado a la ciudad con código: {0} en el sistema."},
                               {"0002", "No existe un registro relacionado al Tipo de documento: {0} en el sistema"},
                               {"0003", "No existe registro relacionado al tipo de tercero con código: {0} activo en el sistema."},

                           };
            }
        }
    }

    public static class State
    {
        public enum Integration
        {
            SinIntegrar = 1,
            EnProcesoDeIntegracion = 2,
            ErrorEnIntegracion = 3,
            Integrado = 4
        }

    }
}