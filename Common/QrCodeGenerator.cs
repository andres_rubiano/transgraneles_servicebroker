﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class QrCodeGenerator
    {
        /// <summary>
        /// Genra imagen de codigo QR
        /// </summary>
        /// <param name="fileName">Nombre de archivo a crear</param>
        /// <param name="qrData">Informacion para la imagen de QR</param>
        /// <param name="localPathUrl">Directorio para guardar la imagen generada, si no existe se crea.</param>

        public static void Create(string fileName, string qrData, string localPathUrl, out string fullFileName)
        {
            if (!Directory.Exists(localPathUrl))
                Directory.CreateDirectory(localPathUrl);

            // Generar el QR:

            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = qrEncoder.Encode(qrData);

            GraphicsRenderer renderer = new GraphicsRenderer(new FixedModuleSize(5, QuietZoneModules.Two), Brushes.Black, Brushes.White);
            using (FileStream stream = new FileStream(localPathUrl + fileName + ".png", FileMode.Create))
            {
                renderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, stream);
            }
            fullFileName = fileName + ".png";
        }

        //private static string ConvertToQrInformation(ManifestQrInformationDto manifest)
        //{

        //    string qrInformation = string.Format("MEC:{0}\r\n", manifest.Mec);
        //    qrInformation += string.Format("Fecha:{0}\r\n", manifest.Fecha);
        //    qrInformation += string.Format("Placa:{0}\r\n", manifest.Placa);

        //    if (!string.IsNullOrEmpty(manifest.Remolque))
        //        qrInformation += string.Format("Remolque:{0}\r\n", manifest.Remolque);

        //    qrInformation += string.Format("Config:{0}\r\n", manifest.ConfiguracionConjunto);
        //    qrInformation += string.Format("Orig:{0}\r\n", manifest.CiudadOrigen);
        //    qrInformation += string.Format("Dest:{0}\r\n", manifest.CiudadDestino);
        //    qrInformation += string.Format("Mercancia:{0}\r\n", manifest.Mercancia);
        //    qrInformation += string.Format("Conductor:{0}\r\n", manifest.ConductorId);
        //    qrInformation += string.Format("Empresa:{0}\r\n", manifest.Empresa);

        //    if (!string.IsNullOrEmpty(manifest.ObservacionesRndc))
        //        qrInformation += string.Format("Obs:{0}\r\n", manifest.ObservacionesRndc);

        //    qrInformation += string.Format("Seguro:{0}", manifest.CodigoSeguridadQr);

        //    return qrInformation;
        //}
    }
}
