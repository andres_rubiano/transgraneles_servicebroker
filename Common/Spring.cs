﻿using Spring.Context;
using Spring.Context.Support;

namespace Common
{
    public static class Spring
    {
        public static object GetObject(string objectName, string context)
        {
            IApplicationContext ctx = ContextRegistry.GetContext(context);
            return ctx.GetObject(objectName);
        }
    }
}
