﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;
using Common.Resources;
using Infrastructure.Data.Main.EF;
using Simplexity.Email;
using System.Security.Cryptography;

namespace Common
{
    public static class Utility
    {
        /// <summary>
        /// Crea y escribe lineas en un archivo
        /// </summary>
        /// <param name="line"></param>
        /// <param name="path"></param>
        public static void WriteFile(string line, string path)
        {
            try
            {
                if (!File.Exists(path))
                {
                    var file = new StreamWriter(path);
                    file.Close();
                }
                using (var writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(line);
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }


        }


        /// escribe un evento en el log de eventos de aplicación de windows
        /// </summary>
        /// <param name="sTexto">Texto del mensaje a insertar en el Log de Eventos de Windows</param>
        /// <param name="tipo">Tipo de Evento del Log (Error, Advertencia, Informacion)</param>
        /// <param name="sSource">nombre de la fuente para el eventlog de windows</param>
        /// <param name="sLog">nombre del log para el eventlog de windows</param>
        public static void RegistrarLogEvento(string sTexto, EventLogEntryType tipo, string sSource, string sLog)
        {
            if (!EventLog.SourceExists(sSource))
            {
                EventLog.CreateEventSource(sSource, sLog);
                return;
            }
            EventLog.WriteEntry(sSource, sTexto, tipo);
        }

        /// <summary>
        /// Registra un evento controlado del negocio en la tabla Events
        /// </summary>
        /// <param name="evenCode">Codigo del tipo de Evento</param>
        /// <param name="eveDescription"></param>
        /// <param name="eveError"></param>
        /// <param name="eveProcess"></param>
        /// <param name="eveUserUcrCode"></param>
        /// <param name="eveLocationUloCode"></param>
        public static void RegistrarEventoNegocio(string evenCode, string eveDescription, string eveError, string eveUserUcrCode, string eveReference, string eveLocationUloCode = null, string eveProcess = null, string evesource = null)
        {
            using (var db = new AsTransLogEntities())
            {
                var _event = new Event
                {
                    EveCode = evenCode,
                    EveError = eveError,
                    EveDescription = eveDescription,
                    EveDateTime = DateTime.Now,
                    EveProcess = eveProcess,
                    EveUser_UsrCode = eveUserUcrCode,
                    EveLocation_UloCode = eveLocationUloCode,
                    EveReference = eveReference,
                    EveSource = evesource
                };
                db.Events.Add(_event);
                db.SaveChanges();

            }
        }

        public static int RegistrarRequest(string dato)
        {
            return RegistrarCallLog(dato, Constants.ServiceBrokerConstants.CallLogTypes.Request);
        }

        public static int RegistrarResponse(string dato)
        {
            return RegistrarCallLog(dato, Constants.ServiceBrokerConstants.CallLogTypes.Response);
        }

        private static int RegistrarCallLog(string datos, int tipo)
        {
            int id;
            using (var db = new AsTransEntities())
            {
                var callLog = new brCallLog()
                {
                    ClgType = (short)tipo,
                    ClgData = datos
                };
                db.brCallLog.Add(callLog);
                db.SaveChanges();
                id = callLog.ClgId;
            }
            return id;
        }

        public static void SendMail(string to, string subject, string body)
        {
            var email = new Email("EmailSettings");
            email.SendMail(to, subject, body);
        }

        public static void SendMailAttachment(string to, string subject, string body, string fileName)
        {
            var email = new Email("EmailSettings");
            email.SendMailAttachment(to, subject, body, fileName);
        }

        public static void SetUserDetails(string receivername, string receiverEmailAddress, string receiverQty, string subject)
        {
            Receiver myCarrier = new Receiver();
            myCarrier.ReceiverName = receivername;
            myCarrier.ReceiverEmailAddress = receiverEmailAddress;
            myCarrier.ReceiverQty = receiverQty;

            Hashtable objHash = new Hashtable();
            objHash["ext:Receiver"] = myCarrier;

            SendMail(receiverEmailAddress, subject, FormatEmail(objHash));

        }

        /// <summary>
        /// Sends an email by using the XSLT template file. The template file is transform over a IDictionary object
        /// to an XHTML document. The contents in the subject line are replaced by the contents of title tag
        /// and the html body becomes the email contents.
        /// </summary>
        /// <param name="emailto">To email address</param>
        /// <param name="xslttemplatename">XSLT template file name</param>
        /// <param name="objDictionary">Dictonary objects containing data to be inserted in the transformed doc.</param>
        static string FormatEmail(IDictionary objDictionary)
        {

            byte[] byteArray = Encoding.ASCII.GetBytes(Templates.emailtemplate.ToCharArray());
            Stream xmlFile = new MemoryStream(byteArray);
            XmlReader xmlt = new XmlTextReader(xmlFile);
            XslTransform objxslt = new XslTransform();
            objxslt.Load(xmlt);

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.AppendChild(xmldoc.CreateElement("DocumentRoot"));
            XPathNavigator xpathnav = xmldoc.CreateNavigator();


            XsltArgumentList xslarg = new XsltArgumentList();
            if (objDictionary != null)
                foreach (DictionaryEntry entry in objDictionary)
                {
                    xslarg.AddExtensionObject(entry.Key.ToString(), entry.Value);
                }

            StringBuilder emailbuilder = new StringBuilder();
            XmlTextWriter xmlwriter = new XmlTextWriter(new StringWriter(emailbuilder));

            objxslt.Transform(xpathnav, xslarg, xmlwriter, null);

            string subjecttext, bodytext;

            XmlDocument xemaildoc = new XmlDocument();
            xemaildoc.LoadXml(emailbuilder.ToString());
            XmlNode titlenode = xemaildoc.SelectSingleNode("//title");

            subjecttext = titlenode.InnerText;

            XmlNode bodynode = xemaildoc.SelectSingleNode("//body");

            bodytext = bodynode.InnerXml;
            if (bodytext.Length > 0)
            {
                bodytext = bodytext.Replace("&amp;", "&");
            }

            return bodytext;

        }

        public static DateTime CastStringToDatetime(string dateTimeCandidate, string datetimeFormat)
        {
            DateTime datetime;
            string formatString = datetimeFormat;
            CultureInfo provider = CultureInfo.InvariantCulture;

            try
            {
                datetime = DateTime.ParseExact(dateTimeCandidate, formatString, provider);
            }
            catch (FormatException e)
            {

                datetime = DateTime.MinValue;
            }
            return datetime;

        }

        /// <summary>
        /// Convierte una cadena texto a Datetime. La cadena texto debe estar en alguno de los siguientes formatos: yyyy/MM/dd HH:mm:ss ,
        /// yyyy.MM.dd HH:mm:ss , yyyy-MM-dd HH:mm:ss , yyyyMMdd HH:mm:ss
        /// siempre se envìan las horas en formato militar (HH)
        /// </summary>
        /// <param name="dateTimeCandidate"></param>
        /// <returns></returns>
        public static DateTime CastStringToValidDatetimeFormats(string dateTimeCandidate)
        {
            DateTime datetime = DateTime.MinValue;
            if (string.IsNullOrEmpty(dateTimeCandidate))
            {
                return DateTime.MinValue;
            }
            datetime = CastStringToDatetime(dateTimeCandidate, "yyyy/MM/dd HH:mm:ss");
            if (datetime != DateTime.MinValue)
            {
                return datetime;
            }
            datetime = CastStringToDatetime(dateTimeCandidate, "yyyy.MM.dd HH:mm:ss");
            if (datetime != DateTime.MinValue)
            {
                return datetime;
            }

            datetime = CastStringToDatetime(dateTimeCandidate, "yyyy-MM-dd HH:mm:ss");
            if (datetime != DateTime.MinValue)
            {
                return datetime;
            }

            datetime = CastStringToDatetime(dateTimeCandidate, "yyyyMMdd HH:mm:ss");
            if (datetime != DateTime.MinValue)
            {
                return datetime;
            }

            datetime = CastStringToDatetime(dateTimeCandidate, "yyyyMMdd");
            if (datetime != DateTime.MinValue)
            {
                return datetime;
            }

            datetime = CastStringToDatetime(dateTimeCandidate, "yyyy-MM-dd");
            if (datetime != DateTime.MinValue)
            {
                return datetime;
            }

            return datetime;
        }

        public static Dictionary<string, string> CompareTwoObjectsProperties(Dictionary<string, string> equivalences, object object1, object object2)
        {
            var result = new Dictionary<string, string>();

            var properties = object2.GetType().GetProperties();

            foreach (PropertyInfo property in object1.GetType().GetProperties())
            {
                //obtengo la propiedad correspondiente del otro lado
                var propertyValue = equivalences[property.Name];


                //busco en el otro lado la propiedad 
                var targetProp = properties.FirstOrDefault(a => a.Name == propertyValue);
                if (targetProp == null)
                {
                    continue;
                }

                //ahora comparo el valor 
                if (property.GetValue(object1, null).ToString().ToUpper() != targetProp.GetValue(object2, null).ToString().ToUpper())
                {
                    //ha cambiado
                    result.Add(property.Name, targetProp.GetValue(object2, null).ToString());
                }
            }
            return result;
        }

        public static T GetEnumFromShort<T>(short? shortValue)
        {
            if (!shortValue.HasValue)
            {
                return default(T);
            }

            foreach (var value in Enum.GetValues(typeof(T)).Cast<object>().Where(value => (short)value == shortValue))
            {
                return (T)value;
            }

            return default(T);
        }
        public static string RemoveXMLNamespace(string xmlData)
        {
            var xslTransform = new XslCompiledTransform();
            var stream = new MemoryStream();
            XmlReader xmlDocumentRead = XmlReader.Create(new StringReader(xmlData));
            XmlReader xslReader = XmlReader.Create(new StringReader(Configurations.XslRemoveNamespaceTags));
            string transformMessage = string.Empty;

            xslTransform.Load(xslReader);
            xslTransform.Transform(xmlDocumentRead, null, stream);
            stream.Position = 1;

            var sr = new StreamReader(stream);
            transformMessage = sr.ReadToEnd();

            xmlDocumentRead.Close();
            xslReader.Close();
            transformMessage = transformMessage.Substring(2);

            return transformMessage;
        }


        public static string SerializeObject<T>(T DTO)
        {
            var serializer = new XmlSerializer(typeof(T));
            var serializedMessage = string.Empty;

            using (var stirngWriter = new StringWriter())
            {
                var xmlWriter = new XmlTextWriter(stirngWriter);
                serializer.Serialize(xmlWriter, DTO);
                serializedMessage = stirngWriter.ToString();
            }
            return serializedMessage;
        }

        /// <summary>
        /// Simplexity S.A
        /// Method: DeserializeObject
        /// Author: Cesar Fernando Estupiñan 
        /// Date: 26/08/2013
        /// Objective: Método estático encargado de des serializar un xml y asignarlo a un objeto T 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlData">The XML data.</param>
        /// <returns>T</returns>
        public static T DeserializeObject<T>(string xmlData)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stringReader = new StringReader(xmlData))
            {
                var xmlReader = new XmlTextReader(stringReader);
                var respClass =
                    (T)serializer.Deserialize(xmlReader);

                return respClass;
            }
        }


        public static void MapperProperties(object destination, object source)
        {
            Type sourcetype = source.GetType();
            Type destinationtype = destination.GetType();

            PropertyInfo[] sourceProperties = sourcetype.GetProperties();
            PropertyInfo[] destionationProperties = destinationtype.GetProperties();

            //var spProps = from sp in sourceProperties select new { sp.Name, sp.PropertyType };
            //var deProps = from dp in destionationProperties select new { dp.Name, dp.PropertyType };

            //seleccionar  las propiedades comunes
            var commonproperties = from sp in sourceProperties
                                   join dp in destionationProperties on new { sp.Name, sp.PropertyType } equals
                                       new { dp.Name, dp.PropertyType }
                                   select new { sp, dp };

            foreach (var match in commonproperties)
            {
                try
                {
                    match.dp.SetValue(destination, match.sp.GetValue(source, null), null);
                }
                catch (Exception)
                {
                }
            }
        }

        /// <summary>
        /// Crea un archivo con la extension indicada.
        /// </summary>
        /// <param name="fileContent">Contenido del archivo.</param>
        /// <param name="filePath">Ubicacion fisisca del archivo.</param>
        /// <param name="reference">Numero de referencia.</param>
        /// <param name="extension">Estension del archivo.</param>
        /// <param name="localPath">Ubicacion completa del archivo (incluye nombre de archivo).</param>
        /// <param name="url">Ubicacion url para acceso via web.</param>
        public static void CreateFile(string fileContent, string filePath, string reference, string extension, out string localPath, out string url, out string nameFile)
        {
            // Estructura en carpetas Año, mes y dia:

            var pathStruct = filePath + GetFolderStructureByDate();
            nameFile = reference + "_" + ObtenerFechaActualSerializada() + extension;

            if (!Directory.Exists(pathStruct))
                Directory.CreateDirectory(pathStruct);

            var fileName = pathStruct + nameFile;
            using (StreamWriter file = new StreamWriter(fileName, true))
            {
                file.WriteLine(fileContent); //se agrega información al documento
                file.Close();
            }

            localPath = fileName;
            url = (GetFolderStructureByDate() + nameFile).Replace(@"\", @"/");

        }

        public static void CreateFile(string fileContent, string filePathName)
        {
            using (StreamWriter file = new StreamWriter(filePathName, true))
            {
                //file.WriteLine(fileContent); //se agrega información al documento
                file.Write(fileContent);
                file.Close();
            }
        }

        private static string GetFolderStructureByDate()
        {
            var dia = "0" + DateTime.Now.Day;
            var mes = "0" + DateTime.Now.Month;

            return DateTime.Now.Year + @"\" + mes.Substring(mes.Length - 2, 2) + @"\" + dia.Substring(dia.Length - 2, 2) +
                   @"\";
        }

        public static string ObtenerFechaActualSerializada()
        {
            var dia = "0" + DateTime.Now.Day;
            var mes = "0" + DateTime.Now.Month;
            var hora = "0" + DateTime.Now.Hour;
            var minuto = "0" + DateTime.Now.Minute;
            var segundo = "0" + DateTime.Now.Second;

            return DateTime.Now.Year + mes.Substring(mes.Length - 2, 2) +
                   dia.Substring(dia.Length - 2, 2) + "_" + hora.Substring(hora.Length - 2, 2) +
                   minuto.Substring(minuto.Length - 2, 2) + segundo.Substring(segundo.Length - 2, 2);
        }

        public static string ObtenerFechaActualSerializadaConMilisegundos()
        {
            var miliSegundo = "0000" + DateTime.Now.Millisecond;

            return ObtenerFechaActualSerializada().Replace("_", "") + miliSegundo.Substring(miliSegundo.Length - 4, 4);
        }

        public static string EncryptSha256(string phrase)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(phrase));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static string FormatInvoiceNumberToCarvajal(string value)
        {
            // Separar prefijo y numero:

            var arrayNumber = value.Split('-');

            // Al numero sin prefijo se le remueven ceros a la Izquierda:

            arrayNumber[1] = arrayNumber[1].TrimStart('0');

            return arrayNumber[0] + arrayNumber[1];

        }

        public static void CrearTarea(string tareaCodigo, string referencia, string objectJson, DateTime fechaParaProcesar, string usuario)
        {
            using (var db = new AsTransEntities())
            {
                var tarea = new brServiceTask()
                {
                    StkType = 1,
                    StkService_SrvCode = tareaCodigo,
                    StkReference = referencia,
                    StkState = 1,
                    StkProcessDate = fechaParaProcesar,
                    StkCreationDate = DateTime.Now,
                    StkCreation_UsrCode = usuario,
                    stkObjectJson = objectJson
                };
                db.brServiceTask.Add(tarea);
                db.SaveChanges();
            }
        }
    }



    public class Receiver
    {
        private string _receiverName;
        private string _receiverEmailAddress;
        private string _receiverQty;

        public string ReceiverName
        {
            get
            {
                return _receiverName;
            }
            set
            {
                _receiverName = value;
            }
        }

        public string ReceiverEmailAddress
        {
            get
            {
                return _receiverEmailAddress;
            }
            set
            {
                _receiverEmailAddress = value;
            }
        }

        public string ReceiverQty
        {
            get
            {
                return _receiverQty;
            }
            set
            {
                _receiverQty = value;
            }
        }
    }

}