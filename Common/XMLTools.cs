﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Common
{
    public static class XMLTools
    {
        /// <summary>
        /// Obtiene la informacion del taga buscar.
        /// </summary>
        /// <param name="xmlString">Contenido XML en donde buscar la información</param>
        /// <param name="pathPrincipal">Pth donde se encuentra el tag o tags</param>
        /// <param name="findTag">tag a buscar</param>
        /// <returns></returns>
        public static string GetValue(string xmlString, string pathPrincipal, string findTag)
        {
            var diccionario = new Dictionary<string, string>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            var demo = string.Format(@"/{0}", pathPrincipal);
            var nodes = xmlDoc.SelectNodes(demo);

            foreach (XmlNode data in nodes)
            {
                foreach (XmlNode tagXml in data)
                    diccionario.Add(tagXml.Name.ToLower(), tagXml.InnerText);

            }

            if (diccionario.Any())
                return diccionario[findTag.ToLower()];
            return string.Empty;
        }

        /// <summary>
        /// Obtiene un StringBuilder con la informacion encontrada por cada tag encontrado, que coincida con el tag a buscar.
        /// </summary>
        /// <param name="xmlString">Contenido XML en donde buscar la información</param>
        /// <param name="findTag">Tag a buscar</param>
        /// <returns></returns>
        public static StringBuilder GetValue(string xmlString, string findTag)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            XmlReader reader = XmlReader.Create(new StringReader(xmlString), settings);

            var datos = new StringBuilder();
            Boolean capturar = false;
            while (reader.Read())
            {

                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (reader.Name.Equals(findTag))
                            capturar = true;
                        else
                            capturar = false;
                        break;
                    case XmlNodeType.Text:
                        if (capturar)
                        {
                            datos.AppendLine(reader.Value);
                            capturar = false;
                        }
                        break;
                }
            }

            return datos;
        }


        /// <summary>
        /// Obtiene un string con la primera informacion encontrada que coincida con el tag a buscar.
        /// </summary>
        /// <param name="xmlString">Contenido XML en donde buscar la información</param>
        /// <param name="findTag">Tag a buscar</param>
        /// <returns></returns>
        public static string GetFirtsValue(string xmlString, string findTag)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            XmlReader reader = XmlReader.Create(new StringReader(xmlString), settings);

            string datos = string.Empty;
            Boolean capturar = false;
            while (reader.Read())
            {

                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        if (reader.Name.Equals(findTag))
                            capturar = true;
                        else
                            capturar = false;
                        break;
                    case XmlNodeType.Text:
                        if (capturar)
                        {
                            datos = reader.Value;
                            return datos;
                        }
                        break;
                }
            }
            return datos;
        }
    }
}

