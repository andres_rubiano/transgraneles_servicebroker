﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using Infrastructure.Data.Main.Repositories.Interfaces;

namespace Infrastructure.Data.Main.Repositories
{

    public abstract class GenericRepository<C, T> : IGenericRepository<T>, IDisposable
        where T : class
        where C : ObjectContext, new()
    {
        private C db = new C();

        public C Context
        {
            get { return db; }
            set { db = value; }
        }

        public virtual IEnumerable<T> SelectAll()
        {
            IEnumerable<T> query = db.CreateObjectSet<T>();
            return query;
        }

        public virtual T SelectOne(Expression<Func<T, bool>> predicate)
        {
            T entity = db.CreateObjectSet<T>().Where(predicate).FirstOrDefault<T>();
            return entity;
        }

        public IEnumerable<T> SelectWhere(Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = db.CreateObjectSet<T>().Where(predicate);
            return query;
        }

        public virtual void Add(T entity)
        {
            string entityType = entity.GetType().ToString();
            int start = entityType.LastIndexOf(".") + 1;
            string entityName = entityType.Substring(start, entityType.Length - start);

            db.AddObject(String.Format("{0}.{1}", db.DefaultContainerName, entityName), entity);
        }

        public virtual void Delete(T entity)
        {
            db.DeleteObject(entity);
        }

        public virtual void Edit(T entity)
        {
            db.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
        }

        public virtual void Save()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            if (db != null)
            {
                db.Dispose();
            }
            GC.SuppressFinalize(this);
        }


    }

}
