﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Infrastructure.Data.Main.Repositories.Interfaces
{
  public interface IGenericRepository<T> where T : class
  {
    IEnumerable<T> SelectAll();

    T SelectOne(Expression<Func<T, bool>> predicate);

    IEnumerable<T> SelectWhere(Expression<Func<T, bool>> predicate);

    void Add(T entity);

    void Delete(T entity);

    void Edit(T entity);

    void Save();
  }
}
