﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Infrastructure.Data.Main.Utils
{
	public static class DataTableExtensions
	{



		private static Dictionary<Type, IList<PropertyInfo>> typeDictionary = new Dictionary<Type, IList<PropertyInfo>>();
		public static IList<PropertyInfo> GetPropertiesForType<T>()
		{
			var type = typeof(T);
			if (!typeDictionary.ContainsKey(typeof(T)))
			{
				typeDictionary.Add(type, type.GetProperties().ToList());
			}
			return typeDictionary[type];
		}

		//public static IList<T> ToList<T>(this DataTable table) where T : new()
		//{
		//  IList<PropertyInfo> properties = GetPropertiesForType<T>();
		//  IList<T> result = new List<T>();

		//  foreach (var row in table.Rows)
		//  {
		//    var item = CreateItemFromRow<T>((DataRow)row, properties);
		//    result.Add(item);
		//  }

		//  return result;
		//}

		private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
		{
			T item = new T();
			foreach (var property in properties)
			{
				property.SetValue(item, row[property.Name], null);
			}
			return item;
		}


		public static IList<T> ToList<T>(this DataTable table) where T : new()
		{

			string propName = string.Empty;
			List<T> entityList = new List<T>();

			foreach (DataRow dr in table.Rows)
			{
				// Create Instance of the Type T
				T entity = Activator.CreateInstance<T>();

				// Get all properties of the Type T
				PropertyInfo[]
				entityProperties = typeof(T).GetProperties();

				// Loop through the properties defined in the 
				// entityList entity object and mapped the value
				foreach (PropertyInfo item in
						entityProperties)
				{
					propName = string.Empty;
					if (propName.Equals(string.Empty))
						propName = item.Name;

					if (table.Columns.Contains(propName))
					{
						// Assign value to the property
						try
						{
							item.SetValue
							(
								entity,
								dr[propName].GetType().
									Name.Equals(typeof(DBNull).Name)
									? null : dr[propName],
								null
							);
						}
						catch (Exception)
						{

							throw;
						}

					}
				}


				entityList.Add(entity);
			}
			return entityList;
		}

	}
}
