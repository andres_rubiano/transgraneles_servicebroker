﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;

namespace Infrastructure.Data.Main.Utils
{
	public static class SqlTools
	{

		public static object SQLGet(string sQuery)
		{
			EntityConnection myConnection = new EntityConnection(ConfigurationManager.ConnectionStrings["ArgosMasterDBEntities"].ConnectionString);
			object Result = null;
			SqlCommand dbCmd = null;
			using (var dbCxn = myConnection.StoreConnection as SqlConnection)
			{
				dbCxn.Open();
				dbCmd = new SqlCommand(sQuery, dbCxn);
				try
				{
					Result = dbCmd.ExecuteScalar();
				}
				catch (Exception ex)
				{
					//throw;

					//Exception customException = new Exception();
					//if (ExceptionPolicy.HandleException(ex, "Business Layer Policy", customException))
					//{
					//    throw customException;
					//}
				}
				finally
				{
					dbCxn.Close();
				}
			}






			return Result;
		}

		public static DataSet SQLGetDataset(string sQuery)
		{
			EntityConnection myConnection = new EntityConnection(ConfigurationManager.ConnectionStrings["ArgosMasterDBEntities"].ConnectionString);
			DataSet Result = new DataSet();


			using (var dbCxn = myConnection.StoreConnection as SqlConnection)
			{
				dbCxn.Open();
				try
				{
					SqlDataAdapter dbAdapter = new SqlDataAdapter(sQuery, dbCxn);
					dbAdapter.Fill(Result, "Table");
				}
				catch (Exception ex)
				{
					//throw;
					//Exception customException = new Exception();
					//if (ExceptionPolicy.HandleException(ex, "Business Layer Policy", customException))
					//{
					//    throw customException;
					//}
				}
				finally
				{
					dbCxn.Close();
				}
			}
			return Result;
		}

		public static DataTable SQLGetDataTable(string sQuery)
		{
			EntityConnection myConnection = new EntityConnection(ConfigurationManager.ConnectionStrings["ArgosMasterDBEntities"].ConnectionString);
			DataSet DsResult = new DataSet();
			DataTable Result = new DataTable();
			sQuery = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;" + sQuery;
			using (var dbCxn = myConnection.StoreConnection as SqlConnection)
			{

				dbCxn.Open();
				try
				{
					SqlDataAdapter dbAdapter = new SqlDataAdapter(sQuery, dbCxn);
					dbAdapter.Fill(DsResult, "Table");
					if (DsResult.Tables.Count > 0)
					{
						Result = DsResult.Tables[0];
					}
				}
				catch (Exception ex)
				{
					//Exception customException = new Exception();
					//if (ExceptionPolicy.HandleException(ex, "Business Layer Policy", customException))
					//{
					//    throw customException;
					//}
				}
				finally
				{
					DsResult.Dispose();
					dbCxn.Close();
				}
			}
			return Result;
		}

		public static string SQLExecute(string sQuery)
		{
			EntityConnection myConnection = new EntityConnection(ConfigurationManager.ConnectionStrings["ArgosMasterDBEntities"].ConnectionString);
			//var a = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ArgosMasterDBAdo"].ToString());
			//sQuery = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;" + sQuery;
			//System.Data.SqlClient.SqlCommand dbCmd = null;
			//a.Open();
			//try
			//{
			//  using (SqlCommand command = new SqlCommand(sQuery, a))
			//  {
			//    command.ExecuteNonQuery();
			//  }



			//}
			//catch (Exception ex)
			//{


			//}
			//finally
			//{
			//  a.Close();
			//}

			#region old_code
			using (var dbCxn = myConnection.StoreConnection as SqlConnection)
			{

				dbCxn.Open();
				var dbCmd = new SqlCommand(sQuery, dbCxn);
				try
				{
					dbCmd.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					Exception customException = new Exception();
					//if (ExceptionPolicy.HandleException(ex, "Business Layer Policy", customException))
					//{
					//    throw customException;
					//}
				}
				finally
				{
					dbCxn.Close();
				}
			}
			#endregion

			return "";
		}

		public static DataTable AdoSqlGetDataTable(string sQuery, string alternativeCnx = "")
		{

			//ConnectionString = "user id=TC_Tester;password=Rqm7938$;server=COIBMTCPVW6K01;Trusted_Connection=no;database=SPX_TCARG_PROD; connection timeout=30"
			//EntityConnection a = new EntityConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SPX_TCARG_PRODEntities"].ToString());
			//act desde Col
			{
				SqlConnection sqlConnection;
				if (string.IsNullOrEmpty(alternativeCnx))
				{
					sqlConnection =
					  new SqlConnection(
						ConfigurationManager.ConnectionStrings["ArgosMasterDBEntities"].ToString());
				}
				else
				{
					sqlConnection =
					  new SqlConnection(
						ConfigurationManager.ConnectionStrings[alternativeCnx].ToString());
				}
				sQuery = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;" + sQuery;
				var DsResult = new DataSet();
				var Result = new DataTable();



				sqlConnection.Open();

				try
				{
					using (SqlDataAdapter dbAdapter = new SqlDataAdapter(sQuery, sqlConnection))
					{
						dbAdapter.Fill(DsResult, "Table");
						if (DsResult.Tables.Count > 0)
						{
							Result = DsResult.Tables[0];
						}
					}
				}
				catch (Exception ex)
				{
					//Exception customException = new Exception();
					//if (ExceptionPolicy.HandleException(ex, "Business Layer Policy", customException))
					//{
					//    throw customException;
					//}
					//MessageBox.Show(ex.Message);
				}
				finally
				{
					DsResult.Dispose();
					sqlConnection.Close();
					//dbCxn.Close();
				}

				return Result;
			}
		}

		/// <summary>
		/// Ejecuta el sql y obtiene una lista de elementos 
		/// del tipo seleccionado. Utiiliza Dapper muy bueno en velocidad
		/// </summary>
		/// <typeparam name="T">Tipo de datos a devolver</typeparam>
		/// <param name="query">Consulta de sql</param>
		/// <returns>Ienumerable con el resultado. Si no existen elementos 
		/// retorna una lista vacia</returns>
		public static IEnumerable<T> ExecSqlGetList<T>(string query, string alternativeCnx = "") where T : new()
		{
			//var cadCnx = System.Configuration.ConfigurationManager.ConnectionStrings["ArgosMasterDBAdo"].ToString();
			//var empList = new List<T>();
			//query = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;" + query;
			//using (var _db = new SqlConnection(cadCnx))
			//{
			//    empList = _db.Query<T>(query).ToList();
			//}

			//return empList;

			var obj = AdoSqlGetDataTable(query, alternativeCnx);
			var res = obj.ToList<T>();
			return res;


		}




		/// <summary>
		/// realiza un insert ADO 
		/// </summary>
		/// <param name="table">Tabla destino del insert</param>
		/// <param name="listParam">Listado de parametros </param>
		/// <returns></returns>
		public static string InsertAdoCommand(string table, Dictionary<string, string> listParam)
		{

			var cadCnx = ConfigurationManager.ConnectionStrings["ArgosMasterDBAdo"].ToString();
			var query = "INSERT INTO " + table + " ";
			var queryValues = listParam.Aggregate("", (current, keyValuePair) => current + (keyValuePair.Key + ","));
			queryValues = "(" + queryValues.Substring(0, queryValues.Length - 1) + ") VALUES (";
			queryValues += listParam.Aggregate("", (current, keyValuePair) => current + ("@" + keyValuePair.Key + ","));
			queryValues = "" + queryValues.Substring(0, queryValues.Length - 1) + ")";
			query += queryValues;
			using (var sqlConnection = new SqlConnection(cadCnx))
			{
				query = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;" + query;
				using (var cmd = new SqlCommand(query, sqlConnection))
				{



				}
			}

			return "";
		}

		/// <summary>
		/// Limpia las cadenas de texto de posible injeccion
		/// </summary>
		/// <param name="stringValue"></param>
		/// <returns></returns>
		public static string Sanitize(this string stringValue)
		{
			if (string.IsNullOrEmpty(stringValue))
				return stringValue;
			return stringValue
						.RegexReplace("-{2,}", "-")                 // transforms multiple --- in - use to comment in sql scripts
						.RegexReplace(@"[*/]+", string.Empty)      // removes / and * used also to comment in sql scripts
						.RegexReplace(@"(;|\s)(exec|execute|select|and|or|1=1|alert|insert|update|delete|create|alter|drop|rename|truncate|backup|restore)\s", string.Empty, RegexOptions.IgnoreCase);
		}


		private static string RegexReplace(this string stringValue, string matchPattern, string toReplaceWith)
		{
			return Regex.Replace(stringValue, matchPattern, toReplaceWith);
		}

		private static string RegexReplace(this string stringValue, string matchPattern, string toReplaceWith, RegexOptions regexOptions)
		{
			return Regex.Replace(stringValue, matchPattern, toReplaceWith, regexOptions);
		}
	}
}
