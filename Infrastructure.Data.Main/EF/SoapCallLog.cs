//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Infrastructure.Data.Main.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class SoapCallLog
    {
        public int Id { get; set; }
        public string OperationName { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public Nullable<System.DateTime> CallDateTime { get; set; }
        public string Reference { get; set; }
        public string OperationCode { get; set; }
    }
}
