//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Infrastructure.Data.Main.EF
{
    using System;
    
    public partial class GetInvoiceHeaderToCarvajal_Result
    {
        public int InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public string Enc_1_TipoDocumento { get; set; }
        public string Enc_2_IdentificacionEmisor { get; set; }
        public string Enc_3_IdentificacionAdquiriente { get; set; }
        public string Enc_6_NumeroDocumento { get; set; }
        public string Enc_7_FechaFactura { get; set; }
        public string Enc_8_HoraFactura { get; set; }
        public string Enc_9_TipoFactura { get; set; }
        public string Enc_16_FechaVencimiento { get; set; }
        public string Enc_17_UrlArchivosAnexos { get; set; }
        public string Emi_1_TipoPersona { get; set; }
        public string Emi_2_Identificacion { get; set; }
        public string Emi_3_TipoIdentificacion { get; set; }
        public string Emi_6_RazonSocial { get; set; }
        public string Emi_7_NombreComercial { get; set; }
        public string Emi_10_Direccion { get; set; }
        public string Emi_11_CodigoDepartamento { get; set; }
        public string Emi_13_Ciudad { get; set; }
        public string Emi_14_CodigoPostal { get; set; }
        public string Emi_19_NombreDepartamento { get; set; }
        public string Emi_22_DigitoVerificacion { get; set; }
        public string Emi_23_CodigoMunicipio { get; set; }
        public string Emi_Cde_1_TipoContacto { get; set; }
        public string Emi_Cde_2_NombreContacto { get; set; }
        public string Emi_Cde_3_TelefonoContacto { get; set; }
        public string Adq_1_TipoPersona { get; set; }
        public string Adq_2_Identificacion { get; set; }
        public string Adq_3_TipoIdentificacion { get; set; }
        public string Adq_4_RegimenFiscal { get; set; }
        public string Adq_6_RazonSocial { get; set; }
        public string Adq_7_NombreComercial { get; set; }
        public string Adq_8_NombreCompleto { get; set; }
        public string Adq_10_Direccion { get; set; }
        public string Adq_11_CodigoDepartamento { get; set; }
        public string Adq_13_Ciudad { get; set; }
        public string Adq_14_CodigoPostal { get; set; }
        public string Adq_19_NombreDepartamento { get; set; }
        public string Adq_22_DigitoVerificacion { get; set; }
        public string Adq_23_CodigoMunicipio { get; set; }
        public string Adq_24_Identificacion { get; set; }
        public string Tcr_1_ResponsabilidadesFiscales { get; set; }
        public string Icr_1_MatriculaMercantil { get; set; }
        public string Cda_1_TipoContacto { get; set; }
        public string Cda_2_NombreContacto { get; set; }
        public string Cda_3_TelefonoContacto { get; set; }
        public string Cda_4_EmailFactura { get; set; }
        public Nullable<double> Tot_1_ImporteBruto { get; set; }
        public Nullable<double> Tot_3_BaseImponible { get; set; }
        public Nullable<double> Tot_5_TotalFactura { get; set; }
        public Nullable<double> Tot_7_TotalFactura { get; set; }
        public Nullable<double> Tot_9_TotalDescuentos { get; set; }
        public string Tim_1_reteFuente { get; set; }
        public Nullable<double> Tim_2_reteFuente { get; set; }
        public Nullable<double> Imp_2_reteFuente_BaseImponible { get; set; }
        public Nullable<double> Imp_4_reteFuente_ImporteImpuesto { get; set; }
        public Nullable<double> Imp_6_reteFuente_PorcentajeImpuesto { get; set; }
        public string Tim_1_iva { get; set; }
        public Nullable<double> Tim_2_iva { get; set; }
        public Nullable<double> Imp_2_iva_BaseImponible { get; set; }
        public Nullable<double> Imp_4_iva_ImporteImpuesto { get; set; }
        public Nullable<double> Imp_6_iva_PorcentajeImpuesto { get; set; }
        public string Tim_1_reteIca { get; set; }
        public Nullable<double> Tim_2_reteIca { get; set; }
        public Nullable<double> Imp_2_reteIca_BaseImponible { get; set; }
        public Nullable<double> Imp_4_reteIca_ImporteImpuesto { get; set; }
        public Nullable<double> Imp_6_reteIca_PorcentajeImpuesto { get; set; }
        public Nullable<double> Dsc_3_ValorDescuento { get; set; }
        public string Drf_4_Prefijo { get; set; }
        public Nullable<int> Drf_5_NumeracionRangoMinimo { get; set; }
        public Nullable<int> Drf_6_NumeracionRangoMaximo { get; set; }
        public string Drf_Resolucion { get; set; }
        public string Nota_1_Pos_2 { get; set; }
        public string Nota_2_Pos_2 { get; set; }
        public string Nota_3_Pos_1 { get; set; }
        public string Nota_6_Pos_1 { get; set; }
        public string Nota_6_Pos_2 { get; set; }
        public Nullable<double> Nota_11_Pos_5 { get; set; }
        public string Ref_2_FacturaReferencia { get; set; }
        public string Ref_3_FacturaReferenciaFecha { get; set; }
        public string Ref_4_FacturaReferenciaCufe { get; set; }
        public string Nrf_1 { get; set; }
        public string Mep_2_MetodoDePago { get; set; }
        public string Mep_3_FechaDePago { get; set; }
        public string Enc_21_TipoOperacion { get; set; }
        public string Idi_1 { get; set; }
    }
}
