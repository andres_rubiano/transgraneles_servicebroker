﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Resources;
using System.Configuration;
using BusinessRules.Main.Services.ServiceBroker;
using BusinessRules.Main.DTOs.ServiceBroker;
using BusinessRules.Main.DTOs.BRProviderService;
using BusinessRules.Main.Services.BRProviderService;
using Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Services.Win.ServiceBroker.Task
{
    public class BrokerTaskMasterClass
    {
        private Timer _timerTksPr1;
        private Timer _timerTksPr2;
        private Timer _timerTksPr3;
        public bool IsDownservice = false;

        public BrokerTaskMasterClass()
        {
        }

        public void Stop()
        {
            IsDownservice = true;
            _timerTksPr1.Enabled = false;
            _timerTksPr2.Enabled = false;
            _timerTksPr3.Enabled = false;
        }

        public void Start()
        {
            #region [ Timer Procesar tareas de prioridad 1]
            _timerTksPr1 = new Timer();
            _timerTksPr1.Interval = double.Parse(ConfigurationManager.AppSettings["TskPriority1Interval"]); 
            _timerTksPr1.Elapsed += _timerTksPr1_Elapsed;
            _timerTksPr1.Enabled = true;
            #endregion

            #region [ Timer Procesar tareas de prioridad 2]
            _timerTksPr2 = new Timer();
            _timerTksPr2.Interval = double.Parse(ConfigurationManager.AppSettings["TskPriority2Interval"]);
            _timerTksPr2.Elapsed += _timerTksPr2_Elapsed;
            _timerTksPr2.Enabled = true;
            #endregion

            #region [ Timer Procesar tareas de prioridad 3]
            _timerTksPr3 = new Timer();
            _timerTksPr3.Interval = double.Parse(ConfigurationManager.AppSettings["TskPriority3Interval"]);
            _timerTksPr3.Elapsed += _timerTksPr3_Elapsed;
            _timerTksPr3.Enabled = true;
            #endregion
        }

        private void _timerTksPr1_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timerTksPr1.Enabled = false;
            ProcessTimerTksPr1().Wait();
            if (!IsDownservice)
                _timerTksPr1.Enabled = true;
        }

        private void _timerTksPr2_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timerTksPr2.Enabled = false;
            ProcessTimerTksPr2().Wait();
            if (!IsDownservice)
                _timerTksPr2.Enabled = true;
        }

        private void _timerTksPr3_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timerTksPr3.Enabled = false;
            ProcessTimerTksPr3().Wait();
            if (!IsDownservice)
                _timerTksPr3.Enabled = true;
        }

        public async Task<int> ProcessTimerTksPr1()
        {
            var j = -1;
            
            try
            {

                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("[ " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " ]  --->  Tareas de Prioridad: [ 1 ]");

                var listDataService = ServicesBroker.brServiceData().ToList();
                var configTasks1 = ServicesBroker.GetConfigurationTasksByPriority("1");
                var top = 0;
                int.TryParse(ConfigurationManager.AppSettings["ServiceBrokerTopTask"], out top);

                var task = ServicesBroker.GetServiceTaskByCodes(top, configTasks1);

                ServicesBroker.StateTask(task);
                if (task == null) return j;

                var typetask = string.Join(",", configTasks1.ToArray());
                Console.WriteLine(@"Tipos Tareas a procesar: " + typetask);
                Console.WriteLine(@"Tareas a procesar: " + task.Count());

                foreach (brServiceTaskDTO brServiceTask in task)
                {

                    Console.WriteLine(string.Format("{0} Tarea {1}", DateTime.Now, brServiceTask.StkService_SrvCode));
                    
                    #region getCredential
                    BrProviderServiceDTO BrProviderServiceDTO = null;
                    var itemlistDataService = listDataService.FirstOrDefault(x => x.SrvCode == brServiceTask.StkService_SrvCode);
                    if (itemlistDataService != null)
                    {
                        var clientproxi = new BrProviderServiceRule();
                        BrProviderServiceDTO = clientproxi.GetBrProviderServiceRule(itemlistDataService.SrvServiceProvider_SprCode);
                    }
                    #endregion

                    var statePreCondition = ServicesBroker.ValidateServiceTaskPrecondition(brServiceTask, listDataService);


                    switch (statePreCondition)
                    {
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Error:
                            ServicesBroker.ErrorTask(brServiceTask);
                            break;
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Scheduled:
                            ServicesBroker.TaskLocked(brServiceTask);
                            break;
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Processing:
                            ServicesBroker.TaskLocked(brServiceTask);
                            break;
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Valid:
                            ServicesBroker.ExecuteTask(brServiceTask, BrProviderServiceDTO);
                            ServicesBroker.ProcessResponses(brServiceTask, listDataService);
                            break;
                    }
                    Console.WriteLine(@"[ {0} ] Ejecuté la tarea: {1} Tipo: {2}", DateTime.Now, brServiceTask.StkReference, brServiceTask.StkService_SrvCode);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(@"[ {0} ] Error: {1} ", DateTime.Now, ex.Message);
                ExceptionPolicy.HandleException(ex, "Exception Shielding");
            }

            return j;
        }

        public async Task<int> ProcessTimerTksPr2()
        {
            var j = -1;
            try
            {

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("[ " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " ]  --->  Tareas de Prioridad: [ 2 ]");

                var listDataService = ServicesBroker.brServiceData().ToList();
                var configTasks2 = ServicesBroker.GetConfigurationTasksByPriority("2");
                var top = 0;
                int.TryParse(ConfigurationManager.AppSettings["ServiceBrokerTopTask"], out top);

                var task = ServicesBroker.GetServiceTaskByCodes(top, configTasks2);

                ServicesBroker.StateTask(task);
                if (task == null) return j;

                var typetask = string.Join(",", configTasks2.ToArray());
                Console.WriteLine(@"Tipos Tareas a procesar: " + typetask);
                Console.WriteLine(@"Tareas a procesar: " + task.Count());

                foreach (brServiceTaskDTO brServiceTask in task)
                {

                    Console.WriteLine(string.Format("{0} Tarea {1}", DateTime.Now, brServiceTask.StkService_SrvCode));

                    #region getCredential
                    BrProviderServiceDTO BrProviderServiceDTO = null;
                    var itemlistDataService = listDataService.FirstOrDefault(x => x.SrvCode == brServiceTask.StkService_SrvCode);
                    if (itemlistDataService != null)
                    {
                        var clientproxi = new BrProviderServiceRule();
                        BrProviderServiceDTO = clientproxi.GetBrProviderServiceRule(itemlistDataService.SrvServiceProvider_SprCode);
                    }
                    #endregion

                    var statePreCondition = ServicesBroker.ValidateServiceTaskPrecondition(brServiceTask, listDataService);


                    switch (statePreCondition)
                    {
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Error:
                            ServicesBroker.ErrorTask(brServiceTask);
                            break;
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Scheduled:
                            ServicesBroker.TaskLocked(brServiceTask);
                            break;
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Processing:
                            ServicesBroker.TaskLocked(brServiceTask);
                            break;
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Valid:
                            ServicesBroker.ExecuteTask(brServiceTask, BrProviderServiceDTO);
                            ServicesBroker.ProcessResponses(brServiceTask, listDataService);
                            break;
                    }
                    Console.WriteLine(@"[ {0} ] Ejecuté la tarea: {1} Tipo: {2}", DateTime.Now, brServiceTask.StkReference, brServiceTask.StkService_SrvCode);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(@"[ {0} ] Error: {1} ", DateTime.Now, ex.Message);
                ExceptionPolicy.HandleException(ex, "Exception Shielding");
            }

            return j;
        }

        public async Task<int> ProcessTimerTksPr3()
        {            
            var j = -1;
            try
            {

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("[ " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " ]  --->  Tareas de Prioridad: [ 3 ]");

                var listDataService = ServicesBroker.brServiceData().ToList();
                var configTasks3 = ServicesBroker.GetConfigurationTasksByPriority("3");
                var top = 0;
                int.TryParse(ConfigurationManager.AppSettings["ServiceBrokerTopTask"], out top);

                var task = ServicesBroker.GetServiceTaskByCodes(top, configTasks3);

                ServicesBroker.StateTask(task);
                if (task == null) return j;

                var typetask = string.Join(",", configTasks3.ToArray());
                Console.WriteLine(@"Tipos Tareas a procesar: " + typetask);
                Console.WriteLine(@"Tareas a procesar: " + task.Count());

                foreach (brServiceTaskDTO brServiceTask in task)
                {

                    Console.WriteLine(string.Format("{0} Tarea {1}", DateTime.Now, brServiceTask.StkService_SrvCode));

                    #region getCredential
                    BrProviderServiceDTO BrProviderServiceDTO = null;
                    var itemlistDataService = listDataService.FirstOrDefault(x => x.SrvCode == brServiceTask.StkService_SrvCode);
                    if (itemlistDataService != null)
                    {
                        var clientproxi = new BrProviderServiceRule();
                        BrProviderServiceDTO = clientproxi.GetBrProviderServiceRule(itemlistDataService.SrvServiceProvider_SprCode);
                    }
                    #endregion

                    var statePreCondition = ServicesBroker.ValidateServiceTaskPrecondition(brServiceTask, listDataService);


                    switch (statePreCondition)
                    {
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Error:
                            ServicesBroker.ErrorTask(brServiceTask);
                            break;
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Scheduled:
                            ServicesBroker.TaskLocked(brServiceTask);
                            break;
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Processing:
                            ServicesBroker.TaskLocked(brServiceTask);
                            break;
                        case (int)ServiceBrokerConstants.TaskPreconditionState.Valid:
                            ServicesBroker.ExecuteTask(brServiceTask, BrProviderServiceDTO);
                            ServicesBroker.ProcessResponses(brServiceTask, listDataService);
                            break;
                    }
                    Console.WriteLine(@"[ {0} ] Ejecuté la tarea: {1} Tipo: {2}", DateTime.Now, brServiceTask.StkReference, brServiceTask.StkService_SrvCode);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(@"[ {0} ] Error: {1} ", DateTime.Now, ex.Message);
                ExceptionPolicy.HandleException(ex, "Exception Shielding");
            }

            return j;

        }
    }
}
