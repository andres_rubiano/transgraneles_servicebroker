﻿using System;
using System.ServiceProcess;
using Services.Win.ServiceBroker.Task;

namespace Services.Win.ServiceBroker
{
    public partial class ServiceBroker : ServiceBase
    {
        #region [Definiciones]
        public bool IsDownservice = false;
        private BrokerTaskMasterClass _task;
        #endregion

        public ServiceBroker()
        {
        }

        public void Start()
        {          

            Console.WriteLine(@"[ {0} ] Inicia Servicio", DateTime.Now);
            _task = new BrokerTaskMasterClass
            {
                IsDownservice = IsDownservice
            };
            _task.Start();
        }
        public void Stop()
        {
            IsDownservice = true;
            _task.Stop();
        }

   
    }
}
