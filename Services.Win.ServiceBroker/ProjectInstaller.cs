﻿using System;
using System.ComponentModel;
using System.Configuration.Install;
using Microsoft.Win32;


namespace Services.Win.ServiceBroker
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private void serviceProcessInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            string keyPath = @"SYSTEM\CurrentControlSet\Services\" + serviceInstaller.ServiceName;
            RegistryKey ckey = Registry.LocalMachine.OpenSubKey(keyPath, true);
            // Pass the service name as a parameter to the service executable
            if (ckey != null && ckey.GetValue("ImagePath") != null)
                ckey.SetValue("ImagePath", (string)ckey.GetValue("ImagePath") + " " + serviceInstaller.ServiceName);
        }

        private void serviceProcessInstaller_BeforeInstall(object sender, InstallEventArgs e)
        {
            // Configura ServiceName e DisplayName
            if (!String.IsNullOrEmpty(Context.Parameters["ServiceName"]))
            {
                serviceInstaller.ServiceName = Context.Parameters["ServiceName"];
                serviceInstaller.DisplayName = Context.Parameters["ServiceName"];
            }
        }

        private void serviceProcessInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            if (!String.IsNullOrEmpty(Context.Parameters["ServiceName"]))
                serviceInstaller.ServiceName = Context.Parameters["ServiceName"];
        }

        private void serviceInstaller_AfterInstall(object sender, InstallEventArgs e)
        {

        }
    }
}
