﻿using System;
using System.Diagnostics;
using System.Net;
using System.ServiceProcess;
using System.Threading;
using Topshelf;

namespace Services.Win.ServiceBroker
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072;

            try
            {
                HostFactory.Run(x =>
                {
                    x.Service<ServiceBroker>(s =>
                    {
                        s.ConstructUsing(name => new ServiceBroker());
                        s.WhenStarted(tc => tc.Start());
                        s.WhenStopped(tc => tc.Stop());
                    });
                    x.RunAsLocalSystem();

                    x.SetDescription("Servicio procesamiento tareas Astrans");
                    x.SetDisplayName("Simplexity.ServiceBroker.Astrans");
                    x.SetServiceName("Simplexity.ServiceBroker.Astrans");
                });

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (ex.InnerException != null)
                {
                    Console.WriteLine("InnerException:");
                    Console.WriteLine(ex.InnerException.Message);
                }

            }


        }
    }
}
