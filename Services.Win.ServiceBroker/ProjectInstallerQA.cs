﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace Services.Win.ServiceBroker
{
    [RunInstaller(true)]
    public partial class ProjectInstallerQA : System.Configuration.Install.Installer
    {
        public ProjectInstallerQA()
        {
            InitializeComponent();
        }

        private void serviceInstaller_AfterInstall(object sender, InstallEventArgs e)
        {

        }

        private void serviceProcessInstaller_AfterInstall(object sender, InstallEventArgs e)
        {

        }
    }
}
